package reconcile

import (
	"fmt"

	log "github.com/sirupsen/logrus"
)

var (
	levelToStatusHandlePut = map[TaskMessage_Type]TaskStatus_StatusType{
		TaskMessage_Undefined: TaskStatus_Undefined,
		TaskMessage_Error:     TaskStatus_Error,
		TaskMessage_Warning:   TaskStatus_Success,
		TaskMessage_Info:      TaskStatus_Success,
		TaskMessage_Debug:     TaskStatus_Success,
		TaskMessage_Trace:     TaskStatus_Success,
	}

	levelToStatusHandleDelete = map[TaskMessage_Type]TaskStatus_StatusType{
		TaskMessage_Undefined: TaskStatus_Undefined,
		TaskMessage_Error:     TaskStatus_Error,
		TaskMessage_Warning:   TaskStatus_Deleted,
		TaskMessage_Info:      TaskStatus_Deleted,
		TaskMessage_Debug:     TaskStatus_Deleted,
		TaskMessage_Trace:     TaskStatus_Deleted,
	}
)

func CheckErrorToMessage(err error) *TaskMessage {
	if err == nil {
		return nil
	}

	return TaskMessageError(err)
}

func TaskMessageUndefined() *TaskMessage {
	return &TaskMessage{
		Level: TaskMessage_Undefined,
	}
}

func TaskMessageError(err error) *TaskMessage {
	return &TaskMessage{
		Level:   TaskMessage_Error,
		Message: err.Error(),
	}
}

func TaskMessageErrorf(format string, a ...interface{}) *TaskMessage {
	return &TaskMessage{
		Level:   TaskMessage_Error,
		Message: fmt.Sprintf(format, a...),
	}
}

func TaskMessageWarning(info string) *TaskMessage {
	return &TaskMessage{
		Level:   TaskMessage_Warning,
		Message: info,
	}
}

func TaskMessageWarningf(format string, a ...interface{}) *TaskMessage {
	return &TaskMessage{
		Level:   TaskMessage_Warning,
		Message: fmt.Sprintf(format, a...),
	}
}

func TaskMessageInfo(info string) *TaskMessage {
	return &TaskMessage{
		Level:   TaskMessage_Info,
		Message: info,
	}
}

func TaskMessageInfof(format string, a ...interface{}) *TaskMessage {
	return &TaskMessage{
		Level:   TaskMessage_Info,
		Message: fmt.Sprintf(format, a...),
	}
}

func TaskMessageDebug(info string) *TaskMessage {
	return &TaskMessage{
		Level:   TaskMessage_Debug,
		Message: info,
	}
}

func TaskMessageDebugf(format string, a ...interface{}) *TaskMessage {
	return &TaskMessage{
		Level:   TaskMessage_Debug,
		Message: fmt.Sprintf(format, a...),
	}
}

func TaskMessageTrace(info string) *TaskMessage {
	return &TaskMessage{
		Level:   TaskMessage_Trace,
		Message: info,
	}
}

func TaskMessageTracef(format string, a ...interface{}) *TaskMessage {
	return &TaskMessage{
		Level:   TaskMessage_Trace,
		Message: fmt.Sprintf(format, a...),
	}
}

// The first argument is the highest level to log (where a higher level means logging more)
// Note that Messages with level undefined are never logged
func (rm *TaskMessage) logByReconciler(highestLevel TaskMessage_Type, disabled bool, manager, name, op, key string) {
	if rm == nil || rm.GetMessage() == "" || disabled {
		return
	}

	if highestLevel == TaskMessage_Undefined {
		highestLevel = defaultLogLevel
	}

	if rm.GetLevel() > highestLevel {
		return
	}

	message := fmt.Sprintf("[%s/%s/%s] %s : %s", manager, name, op, key, rm.GetMessage())

	switch rm.GetLevel() {
	case TaskMessage_Error:
		log.Error(message)
	case TaskMessage_Warning:
		log.Warning(message)
	case TaskMessage_Info:
		log.Info(message)
	case TaskMessage_Debug:
		log.Debug(message)
	case TaskMessage_Trace:
		log.Trace(message)
	}
}
