package reconcile

func NewTaskSummaryError(err error) *TaskSummary {
	return &TaskSummary{
		HighestStatus: TaskStatus_Error,
		Messages:      []*TaskMessage{TaskMessageError(err)},
	}
}

func (ts *TaskSummary) Merge(other ...*TaskSummary) {
	if ts == nil {
		switch len(other) {
		case 0:
			// do nothing
		case 1:
			ts = other[0]
		default:
			ts = other[0]
			ts.Merge(other[1:]...)
		}

		return
	}

	for _, o := range other {
		if o == nil {
			continue
		}

		if o.HighestStatus > ts.HighestStatus {
			ts.HighestStatus = o.HighestStatus
		}

		if o.LastUpdated != nil && o.LastUpdated.AsTime().After(ts.LastUpdated.AsTime()) {
			ts.LastUpdated = o.LastUpdated
		}

		if (ts.FirstUpdated == nil) ||
			(o.FirstUpdated != nil && ts.FirstUpdated.AsTime().After(o.FirstUpdated.AsTime())) {
			ts.FirstUpdated = o.FirstUpdated
		}

		ts.Messages = append(ts.Messages, o.Messages...)
	}

}
