package reconcile

import (
	"context"
	"fmt"
	"strings"
	// "sync"

	sets "github.com/deckarep/golang-set/v2"
	log "github.com/sirupsen/logrus"
	//"go.etcd.io/etcd/api/v3/mvccpb"
	"go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/durationpb"
	"google.golang.org/protobuf/types/known/timestamppb" // for .Now()

	storage "gitlab.com/mergetb/tech/shared/storage/etcd"
)

func NewReconcilerHealth(rm *ReconcilerManager, r *Reconciler) *ReconcilerHealth {

	lastkey := ""
	x := r.lastKeyHandled.Load()
	if x != nil {
		lastkey = x.(string)
	}

	var whenLastUpdate *timestamppb.Timestamp
	x = r.whenLastKey.Load()
	if x != nil {
		whenLastUpdate = x.(*timestamppb.Timestamp)
	}

	return &ReconcilerHealth{
		Manager:     rm.Manager,
		Name:        r.Name,
		Duration:    durationpb.New(rm.HealthFrequency),
		SelfVersion: -1,
		StartTime:   r.startTime,
		LastKey:     lastkey,
		KeysHandled: r.keysHandled.Load(),
		WhenLastKey: whenLastUpdate,
	}
}

func NewReconcilerHealths(tms ...*ReconcilerManager) []*ReconcilerHealth {
	var ans []*ReconcilerHealth

	for _, tm := range tms {
		for _, t := range tm.Reconcilers {
			ans = append(ans, NewReconcilerHealth(tm, t))
		}
	}

	return ans
}

func NewReconcilerHealthFromStatus(ts *TaskStatus) *ReconcilerHealth {

	return &ReconcilerHealth{
		Manager:     ts.ReconcilerManager,
		Name:        ts.ReconcilerName,
		SelfVersion: -1,
	}
}

func (rh *ReconcilerHealth) CreateLeaseID(etcdCli *clientv3.Client) (clientv3.LeaseID, error) {

	if etcdCli == nil {
		e := fmt.Errorf("etcd is nil!")
		//log.Error(e)
		return 0, e
	}

	ctx, cancel := context.WithTimeout(context.Background(), contextTimeout)
	defer cancel()

	resp, err := etcdCli.Grant(ctx, int64(rh.Duration.AsDuration().Seconds()*healthFrequencyBuffer))
	if err != nil {
		return 0, err
	}

	return resp.ID, err
}

// Storage ObjectIO interface implementation ==================================

func (rh *ReconcilerHealth) Key() string {
	return rh.GetKey()
}

func (rh *ReconcilerHealth) Value() interface{} {
	return rh
}

func (rh *ReconcilerHealth) GetVersion() int64 {
	return -1
}

func (rh *ReconcilerHealth) SetVersion(v int64) {
	rh.SelfVersion = v
	return
}

// Storage BufferedObject interface implementation ============================

func (rh *ReconcilerHealth) AppendCreateOps(etcdCli *clientv3.Client, ops *storage.EtcdTransaction) error {

	if rh.Manager == "" || rh.Name == "" {
		return fmt.Errorf("reconciler health is empty")
	}

	rh.StartTime = timestamppb.Now()
	rh.Unresponsive = false
	ops.Write(rh)

	if rh.Duration.AsDuration().Seconds() >= 1 {
		lease, err := rh.CreateLeaseID(etcdCli)

		if err != nil {
			return err
		}

		ops.WriteWithOpt(rh.KeepAliveObject(), clientv3.WithLease(lease))
	} else {
		ops.Write(rh.KeepAliveObject())
	}

	return nil

}

func (rh *ReconcilerHealth) AppendUpdateOps(etcdCli *clientv3.Client, ops *storage.EtcdTransaction) error {
	return rh.AppendCreateOps(etcdCli, ops)
}

func (rh *ReconcilerHealth) AppendDeleteOps(etcdCli *clientv3.Client, ops *storage.EtcdTransaction) error {

	if rh.Manager == "" || rh.Name == "" {
		return fmt.Errorf("reconciler health is empty")
	}

	ops.Delete(rh)
	ops.Delete(rh.KeepAliveObject())

	return nil

}

func (rh *ReconcilerHealth) AppendReadOps(etcdCli *clientv3.Client, ops *storage.EtcdTransaction) error {

	if rh.Manager == "" || rh.Name == "" {
		return fmt.Errorf("reconciler health is empty")
	}

	ops.Read(rh)
	ops.Read(rh.KeepAliveObject())

	return nil
}

func (_ *ReconcilerHealth) ReadAllFromResponse(txn *clientv3.TxnResponse) (map[string]storage.EtcdObjectIO, error) {

	// the answer map
	m := make(map[string]storage.EtcdObjectIO)
	keepalives := sets.NewSet[string]()

	// read keepalives
	for _, r := range txn.Responses {
		rr := r.GetResponseRange()

		if rr == nil {
			continue
		}

		for _, kv := range rr.Kvs {

			if strings.HasPrefix(string(kv.Key), aliveRoot) {
				keepalives.Add(string(kv.Key))
			}
		}

	}

	// read the actual reconciler health objects
	for _, r := range txn.Responses {
		rr := r.GetResponseRange()

		if rr == nil {
			continue
		}

		for _, kv := range rr.Kvs {

			if strings.HasPrefix(string(kv.Key), healthRoot) {
				rh := new(ReconcilerHealth)

				err := proto.Unmarshal(kv.Value, rh)
				if err != nil {
					log.Warnf("failed to unmarshal task goal, ignoring %s: %+v", string(kv.Key), err)
					continue
				}

				rh.SetVersion(kv.Version)
				rh.Unresponsive = !keepalives.Contains(rh.KeepAliveObject().Key())

				m[rh.Key()] = rh
			}
		}

	}

	return m, nil
}

// Singleton storage functions ================================================

func (rh *ReconcilerHealth) Read(etcdCli *clientv3.Client) error {
	return storage.UnbufferedRead(etcdCli, rh)
}

func (rh *ReconcilerHealth) Create(etcdCli *clientv3.Client) (*storage.EtcdRollback, error) {
	return storage.UnbufferedCreate(etcdCli, rh)
}

func (rh *ReconcilerHealth) Update(etcdCli *clientv3.Client) (*storage.EtcdRollback, error) {
	return storage.UnbufferedUpdate(etcdCli, rh)
}

func (rh *ReconcilerHealth) Delete(etcdCli *clientv3.Client) (*storage.EtcdRollback, error) {
	return storage.UnbufferedDelete(etcdCli, rh)
}

// EtcdTxTimestamped interface implementation =================================

func (rh *ReconcilerHealth) WriteTimestamp() {
	rh.CheckInTime = timestamppb.Now()
}

// Clone helper function ======================================================

func (rh *ReconcilerHealth) Clone() *ReconcilerHealth {
	return proto.Clone(rh).(*ReconcilerHealth)
}

// Key helper functions =======================================================

func (rh *ReconcilerHealth) GetKey() string {
	return storage.SmartJoin(
		healthRoot,
		rh.Manager,
		rh.Name,
	)
}

func (rh *ReconcilerHealth) KeepAliveObject() *storage.GenericEtcdKey {
	return storage.NewGenericEtcdKey(
		storage.SmartJoin(
			aliveRoot,
			rh.Manager,
			rh.Name,
		),
		nil,
	)
}
