package reconcile

import (
	"fmt"
	"strings"
	"time"

	mapset "github.com/deckarep/golang-set/v2"
	sets "github.com/deckarep/golang-set/v2"
	log "github.com/sirupsen/logrus"
	"go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/timestamppb" // for .Now()

	storage "gitlab.com/mergetb/tech/shared/storage/etcd"
)

var (
	goaltypeToPrefix = map[TaskGoal_GoalType]string{
		TaskGoal_Undefined: "",
		TaskGoal_Supergoal: goalsRoot,
		TaskGoal_Supertask: statusRoot,
	}
)

func NewGenericGoal(key string) *TaskGoal {
	return &TaskGoal{
		SelfKey: key,
		When:    timestamppb.Now(),
	}
}

func NewGoal(key, name, desc string) *TaskGoal {
	return &TaskGoal{
		SelfKey: key,
		Name:    name,
		Desc:    desc,
		When:    timestamppb.Now(),
	}
}

// Key helper functions =======================================================

func (tg *TaskGoal) GetPrefixedKey() string {
	return storage.SmartJoin(goalsRoot, tg.SelfKey)
}

// EtcdTxTimestamped interface implementation =================================

func (tg *TaskGoal) WriteTimestamp() {
	tg.When = timestamppb.Now()
	if tg.Creation == nil {
		tg.Creation = tg.When
	}
}

// Storage ObjectIO interface implementation ==================================

func (tg *TaskGoal) Key() string {
	return tg.GetPrefixedKey()
}

func (tg *TaskGoal) Value() interface{} {
	return tg
}

// ignore the version of the key, should always be able to write
func (tg *TaskGoal) GetVersion() int64 {
	return -1
}

func (tg *TaskGoal) SetVersion(v int64) {
	tg.SelfVersion = v
}

// Storage BufferedObject interface implementation ============================

func (tg *TaskGoal) AppendCreateOps(etcdCli *clientv3.Client, ops *storage.EtcdTransaction) error {
	tg.cleanupDeprecatedSubkeys()

	if etcdCli == nil {
		e := fmt.Errorf("etcd is nil!")
		//log.Error(e)
		return e
	}

	if tg.SelfKey == "" {
		return fmt.Errorf("selfkey is empty")
	}

	tg.When = timestamppb.Now()
	if tg.Creation == nil {
		tg.Creation = timestamppb.Now()
	}

	ops.Write(tg)

	return nil

}

func (tg *TaskGoal) AppendUpdateOps(etcdCli *clientv3.Client, ops *storage.EtcdTransaction) error {
	tg.cleanupDeprecatedSubkeys()

	return tg.AppendCreateOps(etcdCli, ops)
}

func (tg *TaskGoal) AppendDeleteOps(etcdCli *clientv3.Client, ops *storage.EtcdTransaction) error {
	tg.cleanupDeprecatedSubkeys()

	if tg.SelfKey == "" {
		return fmt.Errorf("selfkey is empty")
	}

	ops.Delete(tg)

	return nil

}

func (tg *TaskGoal) AppendReadOps(etcdCli *clientv3.Client, ops *storage.EtcdTransaction) error {
	tg.cleanupDeprecatedSubkeys()

	if tg.SelfKey == "" {
		return fmt.Errorf("selfkey is empty")
	}

	ops.Read(tg)

	return nil
}

func (_ *TaskGoal) ReadAllFromResponse(txn *clientv3.TxnResponse) (map[string]storage.EtcdObjectIO, error) {

	// the answer map
	m := make(map[string]storage.EtcdObjectIO)

	for _, r := range txn.Responses {
		rr := r.GetResponseRange()

		if rr == nil {
			continue
		}

		for _, kv := range rr.Kvs {

			if strings.HasPrefix(string(kv.Key), goalsRoot) {
				t := new(TaskGoal)

				err := proto.Unmarshal(kv.Value, t)
				if err != nil {
					log.Warnf("failed to unmarshal task goal, ignoring %s: %+v", string(kv.Key), err)
					continue
				}

				t.SetVersion(kv.Version)
				t.cleanupDeprecatedSubkeys()

				m[t.Key()] = t
			}
		}

	}

	return m, nil

}

// Singleton storage functions ================================================

func (tg *TaskGoal) Read(etcdCli *clientv3.Client) error {
	return storage.UnbufferedRead(etcdCli, tg)
}

func (tg *TaskGoal) Create(etcdCli *clientv3.Client) (*storage.EtcdRollback, error) {
	return storage.UnbufferedCreate(etcdCli, tg)
}

func (tg *TaskGoal) Update(etcdCli *clientv3.Client) (*storage.EtcdRollback, error) {
	return storage.UnbufferedUpdate(etcdCli, tg)
}

func (tg *TaskGoal) Delete(etcdCli *clientv3.Client) (*storage.EtcdRollback, error) {
	return storage.UnbufferedDelete(etcdCli, tg)
}

// Helper functions around adding subkeys ====================================

func (tg *TaskGoal) cleanupDeprecatedSubkeys() {
	if len(tg.Subkeys) > 0 {
		switch tg.GetType() {
		case TaskGoal_Supergoal:
			tg.Subgoals = append(tg.Subkeys, tg.Subgoals...)

			tg.Subkeys = nil
		case TaskGoal_Supertask:
			ans := make([]*TaskRecord, 0, len(tg.Subkeys)+len(tg.Subtasks))
			for _, k := range tg.Subkeys {
				ans = append(ans, &TaskRecord{
					Task: k,
				})

				tg.Subtasks = append(ans, tg.Subtasks...)
			}

			tg.Subkeys = nil
		default:
			log.Errorf("somehow not supergoal or supertask, but has subkeys, ignoring")
		}
	}
}

func (tg *TaskGoal) AddSubgoal(other ...*TaskGoal) {
	added := false
	for _, o := range other {
		if o.GetSelfKey() != "" {
			tg.Subgoals = append(tg.Subgoals, o.GetSelfKey())
			added = true
		}
	}

	if added {
		tg.Type = TaskGoal_Supergoal
	}
}

func (tg *TaskGoal) AddTaskRecord(trs ...*TaskRecord) {
	tg.Subtasks = append(tg.Subtasks, trs...)

	if tg.Type != TaskGoal_Supergoal {
		tg.Type = TaskGoal_Supertask
	}

	return
}

func (tg *TaskGoal) RemoveSubtask(rem *TaskRecord) bool {
	found := false
	subtasks := make([]*TaskRecord, 0, len(tg.Subtasks)-1)

	for _, tr := range tg.Subtasks {
		if tr.Equals(rem) {
			found = true
		} else {
			subtasks = append(subtasks, tr)
		}
	}

	tg.Subtasks = subtasks

	return found
}

func (tg *TaskGoal) RemoveSubgoal(rem *TaskGoal) bool {
	found := false

	if len(tg.Subgoals) > 0 {
		subgoals := make([]string, 0, len(tg.Subgoals)-1)
		for _, k := range tg.Subgoals {
			if k == rem.SelfKey {
				found = true
			} else {
				subgoals = append(subgoals, k)
			}
		}

		tg.Subgoals = subgoals
	}

	return found
}

func (tg *TaskGoal) JoinTaskForests(tfs ...*TaskForest) *TaskForest {
	var higheststatus TaskStatus_StatusType = 0
	var lastupdated time.Time
	num_childtasks := int64(0)

	for _, tf := range tfs {
		if tf.HighestStatus > higheststatus {
			higheststatus = tf.HighestStatus
		}

		if lastupdated.Before(tf.LastUpdated.AsTime()) {
			lastupdated = tf.LastUpdated.AsTime()
		}

		// if tg.Creation is nil, or if it's a goal not written in etcd and the time is lower,
		// copy over the time
		if (tg.GetCreation() == nil) ||
			(tg.Creation.AsTime().After(tf.Goal.Creation.AsTime())) {
			tg.Creation = tf.Goal.Creation
		}

		num_childtasks += tf.NumChildTasks
	}

	// if lastupdated hasn't been updated
	// then use the goal time
	if lastupdated == (time.Time{}) {
		lastupdated = tg.When.AsTime()
	}

	// if we didn't write any subgoals/statuses,
	// it is considered a success
	if higheststatus == TaskStatus_Undefined {
		higheststatus = TaskStatus_Success
	}

	root := &TaskForest{
		Goal:          tg,
		Subgoals:      tfs,
		HighestStatus: higheststatus,
		LastUpdated:   timestamppb.New(lastupdated),
		NumChildTasks: num_childtasks,
	}

	root.Organize()

	tg.Type = TaskGoal_Supergoal

	return root
}

func (tg *TaskGoal) AppendTaskRecordOps(etcdCli *clientv3.Client, ops *storage.EtcdTransaction) {
	subtasks := tg.GetSubtasks()

	added_tasks := sets.NewSet[string]()

	// process prefixed keys first
	for _, tr := range subtasks {
		if tr.KeyType == TaskRecord_PrefixSpace {
			if !added_tasks.Contains(tr.Task) {
				ts := NewTaskStatusUnknown(tr.Task)
				ts.AppendReadOpsAsPrefix(etcdCli, ops)
				added_tasks.Add(tr.Task)
			}
		}
	}

	// process everything else
	for _, tr := range subtasks {
		if !added_tasks.Contains(tr.Task) {

			switch tr.KeyType {
			case TaskRecord_SingleKey:
				// we add it as unknown so we can read everything in the space anyways
				ts := NewTaskStatusUnknown(tr.Task)

				ts.AppendReadOps(etcdCli, ops)
				added_tasks.Add(tr.Task)
			case TaskRecord_PrefixSpace:
				// handled already

			default:
				panic(fmt.Errorf("unhandled key type: %s", tr.KeyType))
			}
		}
	}
}

func (tg *TaskGoal) ReadAllSubkeys(etcdCli *clientv3.Client, cache *storage.EtcdCache) ([]*TaskGoal, []*TaskStatus, error) {
	// read everything at once
	ops := new(storage.EtcdTransaction)

	subgoals := tg.GetSubgoals()
	subtasks := tg.GetSubtasks()

	// get all keys
	for _, c := range subgoals {
		// read the goals keys
		NewGenericGoal(c).AppendReadOps(etcdCli, ops)
	}
	tg.AppendTaskRecordOps(etcdCli, ops)

	// read txr
	txr, _, err := ops.EtcdTxWithOptions(etcdCli, storage.EtcdTransactionOptions{Cache: cache})
	if err != nil {
		return nil, nil, err
	}

	// read goals
	read_goals, err := tg.ReadAllFromResponse(txr)
	if err != nil {
		return nil, nil, err
	}

	// read statuses
	read_statuses, _, _, err := ReadAllStatusInfoFromResponse(txr)
	if err != nil {
		return nil, nil, err
	}

	// create a list of everything else that we read
	read_keys_m := mapset.NewSet[string]()
	for _, r := range txr.Responses {
		rr := r.GetResponseRange()
		if rr == nil {
			continue
		}

		for _, kv := range rr.Kvs {
			k := string(kv.Key)

			// ignore anything with the tasksroot
			if !strings.HasPrefix(k, tasksRoot+"/") {
				read_keys_m.Add(k)
			}

		}
	}
	read_keys := read_keys_m.ToSlice()
	read_keys_m = nil

	// process goals
	var g_ans []*TaskGoal

	for _, k := range subgoals {

		goal := NewGenericGoal(k)
		k := goal.Key()

		v, ok := read_goals[k]
		// goal is missing, make a note of that and continue
		if !ok {
			log.Errorf("error: could not find its goal: %s", k)
			g_ans = append(g_ans, NewGoal(k, k, fmt.Sprintf("error: could not find its goal: %s", k)))

			continue
		}

		goal, ok = v.(*TaskGoal)
		if !ok {
			log.Warnf("%s is not a task goal, skipping", k)
			continue
		}

		g_ans = append(g_ans, goal)
	}

	// convert prefix space task records by converting them into single key records
	new_subtasks := make([]*TaskRecord, 0, len(subtasks))
	for _, tr := range subtasks {
		switch tr.KeyType {
		case TaskRecord_SingleKey:
			new_subtasks = append(new_subtasks, tr)
		case TaskRecord_PrefixSpace:
			for _, k := range read_keys {
				if strings.HasPrefix(k, tr.Task) {
					new_subtasks = append(new_subtasks, &TaskRecord{
						Task:              k,
						ReconcilerManager: tr.ReconcilerManager,
						ReconcilerName:    tr.ReconcilerName,
						Existence:         tr.Existence,
					})
				}
			}
		}
	}
	subtasks = new_subtasks

	// process statuses
	ts_map := make(map[string][]*TaskStatus)

	handled_recs := make([]bool, len(subtasks))
	for i, tr := range subtasks {
		tses := read_statuses[tr.Task]

		if len(tses) == 0 {

			// is valid reconciler/name combo to look for,
			// so mark that it's missing
			if tr.ReconcilerManager != "" && tr.ReconcilerName != "" {

				// only mark it as missing if it doesn't exist
				if tr.Existence == TaskRecord_Exists {
					ts := NewTaskMissingStatus(tr)

					ts_map[tr.Task] = append(ts_map[tr.Task], ts)
				}

				handled_recs[i] = true

			} else if tr.ReconcilerManager != "" || tr.ReconcilerName != "" {
				// if both are nil, it's ok
				log.Errorf("invalid manager/reconciler name, both need to be nonnil: %s/%s", tr.ReconcilerManager, tr.ReconcilerName)
			}

			continue
		}

		// just write everything that we found,
		// if it's the first time we've seen the key
		if ts_map[tr.Task] == nil {
			ts_map[tr.Task] = tses[:]
		}

		// generally, the number of reconcilers operating a key is less than 10,
		// so just walking through multiple times is faster than using a map

		// remove ignored statuses by copying only unignored statuses
		filtered_tses := make([]*TaskStatus, 0, len(ts_map[tr.Task]))
		for _, ts := range ts_map[tr.Task] {

			// it's considered ignored if it fulfills the task record
			// and the option is to ignore
			if !(ts.FullfillsTaskRecord(tr) && tr.Existence == TaskRecord_Ignore) {
				filtered_tses = append(filtered_tses, ts.RewriteCurrentMessage(tr))
			}
		}
		ts_map[tr.Task] = filtered_tses

		// find the current rec in the tses and mark if the task record was fulfilled
		for _, ts := range ts_map[tr.Task] {
			if ts.FullfillsTaskRecord(tr) {
				handled_recs[i] = true
				break
			}
		}

		// if we didn't find it, mark that it's missing
		if !handled_recs[i] {

			if tr.Existence == TaskRecord_Exists {
				ts := NewTaskMissingStatus(tr)
				ts_map[tr.Task] = append(ts_map[tr.Task], ts)
			}

			handled_recs[i] = true
		}
	}

	ts_ans := make([]*TaskStatus, 0, len(subtasks))

	// the only things that should be unhandled are
	// generic reconciler searches on missing keys
	added_tasks := sets.NewSet[string]()
	for i, tr := range subtasks {
		if !added_tasks.Contains(tr.Task) && len(ts_map[tr.Task]) > 0 {
			ts_ans = append(ts_ans, ts_map[tr.Task]...)
		}
		added_tasks.Add(tr.Task)

		// add a missing message if we made no specific reconciler searches and it needs to exist
		if !handled_recs[i] && len(ts_map[tr.Task]) == 0 && tr.Existence == TaskRecord_Exists {
			ts_ans = append(ts_ans, NewTaskMissingStatus(tr))
		}
	}

	return g_ans, ts_ans, nil
}
