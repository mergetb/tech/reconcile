package main

import (
	"encoding/base64"
	"fmt"

	"gitlab.com/mergetb/tech/reconcile"

	"github.com/spf13/cobra"
	"google.golang.org/protobuf/encoding/protojson"
	"google.golang.org/protobuf/proto"
)

// This is a small program to help debug statuses in etcd before you have full API implementations.
// Maybe there should be a mega-binary that can read all MergeTB objects from base64, so you can debug directly from etcd.

func main() {

	var root = &cobra.Command{
		Use:   "rectool",
		Short: "Manually read base64 encodings of statuses and goals (use `etcdctl get -w json`)",
	}

	goalcmd := &cobra.Command{
		Use:   "goal [base64]",
		Short: "Read a goal (encoded as base64) as JSON",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			readgoal(args[0])
		},
	}
	root.AddCommand(goalcmd)

	statuscmd := &cobra.Command{
		Use:   "status [base64]",
		Short: "Read a status (encoded as base64) as JSON",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			readstatus(args[0])
		},
	}
	root.AddCommand(statuscmd)

	root.Execute()

}

func readgoal(s string) {
	dec, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		panic(err)
	}

	goal := new(reconcile.TaskGoal)

	err = proto.Unmarshal(dec, goal)
	if err != nil {
		panic(err)
	}

	fmt.Println(protojson.Format(goal))

}

func readstatus(s string) {
	fmt.Println(s)

	dec, err := base64.StdEncoding.DecodeString(s)
	if err != nil {
		panic(err)
	}

	status := new(reconcile.TaskStatus)

	err = proto.Unmarshal(dec, status)
	if err != nil {
		panic(err)
	}

	fmt.Println(protojson.Format(status))

}
