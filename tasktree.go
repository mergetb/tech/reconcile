package reconcile

import (
	"fmt"
	"sort"

	sets "github.com/deckarep/golang-set/v2"
	"github.com/maruel/natural"
	log "github.com/sirupsen/logrus"
	"go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/types/known/timestamppb" // for .Now()

	storage "gitlab.com/mergetb/tech/shared/storage/etcd"
)

func (tt *TaskTree) getStatuses(statuses []*TaskStatus) []*TaskStatus {
	statuses = append(statuses, tt.GetTask())

	for _, o := range tt.GetSubtasks() {
		statuses = o.getStatuses(statuses)
	}

	return statuses
}

func (ts *TaskStatus) GetTaskTree(etcdCli *clientv3.Client, cache *storage.EtcdCache) (*TaskTree, error) {
	return ts.getTaskTreeHelper(etcdCli, sets.NewSet[string](), cache)
}

func (ts *TaskStatus) getTaskTreeHelper(etcdCli *clientv3.Client, visited sets.Set[string], cache *storage.EtcdCache) (*TaskTree, error) {
	if etcdCli == nil {
		e := fmt.Errorf("etcd is nil!")
		log.Error(e)
		return nil, e
	}

	m, err := ReadAllTaskStatusSubkeys(etcdCli, ts.SubtaskKeys, cache)
	if err != nil {
		return nil, err
	}

	visited.Add(ts.TaskKey)

	var subtasks []*TaskTree
	higheststatus := ts.CurrentStatus
	lastupdated := ts.When.AsTime()
	num_childtasks := int64(0)

	// Strip off the actual value that it was reconciled on,
	// I don't think that users should be able to read this
	// Also, it saves data
	ts.PrevValue = nil

	sorted_keys := make([]string, 0, len(m))
	for k := range m {
		sorted_keys = append(sorted_keys, k)
	}
	sort.Sort(natural.StringSlice(sorted_keys))

	for _, k := range sorted_keys {
		ts_array := m[k]

		if visited.Contains(k) {
			log.Warnf("status %s already visited, skipping", k)
			continue
		}

		visited.Add(k)

		sort.Slice(ts_array,
			func(i, j int) bool {
				return natural.Less(
					ts_array[i].GetFullName(),
					ts_array[j].GetFullName(),
				)
			},
		)

		for _, ts := range ts_array {

			c_tt, err := ts.getTaskTreeHelper(etcdCli, visited, cache)
			if err != nil {
				return nil, err
			}

			subtasks = append(subtasks, c_tt)

			if c_tt.HighestStatus > higheststatus {
				higheststatus = c_tt.HighestStatus
			}

			if lastupdated.Before(c_tt.LastUpdated.AsTime()) {
				lastupdated = c_tt.LastUpdated.AsTime()
			}

			num_childtasks += c_tt.NumChildTasks
		}

	}

	tt := &TaskTree{
		Task:          ts,
		Subtasks:      subtasks,
		HighestStatus: higheststatus,
		LastUpdated:   timestamppb.New(lastupdated),
		NumChildTasks: num_childtasks + int64(len(subtasks)),
	}

	return tt, nil
}
