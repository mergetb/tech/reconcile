package reconcile

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"math"
	"runtime"
	"sync"
	"sync/atomic"
	"time"

	sets "github.com/deckarep/golang-set/v2"
	"github.com/puzpuzpuz/xsync/v3"
	log "github.com/sirupsen/logrus"
	"go.etcd.io/etcd/api/v3/mvccpb"
	"go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/timestamppb"

	"gitlab.com/mergetb/tech/reconcile/pkg/altsync"
	workerpool "gitlab.com/mergetb/tech/reconcile/pkg/pworkerpool"
	"gitlab.com/mergetb/tech/reconcile/pkg/qosticker"
	storage "gitlab.com/mergetb/tech/shared/storage/etcd"
)

type ReconcilerManager struct {

	// The name of the reconciler manager
	Manager     string
	EtcdCli     *clientv3.Client
	Reconcilers []*Reconciler

	// How often to have the reconciler manager check in
	// (Reconcilers will always check-in after it handles a task.)
	// A negative value means that it will only check-in after a task is reconciled.
	// A value of 0 gets defaulted to the package default
	HealthFrequency time.Duration

	// How long until a heartbeat is missed until we decide to give up and assume the server is dead
	// Value of 0 indicate to use the package default (`defaultHeartbeatGracePeriod`)
	// Value < 0 disables heartbeat checking
	ServerHeartbeatGracePeriod time.Duration

	// The highest level to log, i.e. when set to Warning, only logs Warnings and Errors
	// Messages with level undefined are never logged.
	// If not set, defaults to level: Warning
	LogLevel TaskMessage_Type

	// If this is set, completely disables automatic message logging if you want to do it yourself
	DisableMessageLogging bool

	// How many workers to run updates simultaneously
	// Only 1 worker per key is allowed to run at a time,
	// and the revisions are handled in order.
	// In other words, consider a sequence of key operations on keyA:
	//   1. PUT keyA
	//   2. DELETE keyA
	//   3. PUT keyA
	//   4. PUT keyB
	// 1-4 will be run in order and 2-4 will wait for the previous revision to be executed before starting.
	// A value of 0 will default to 1 worker
	// A negative value will default to NUM_CPU workers
	Workers int

	// When .Run() is done, by default/set to false, it Fatals on error
	// Mostly just used for testing so we can test the errors produced
	JustLogErrors bool

	// Is running mutex
	running sync.Mutex

	// Heartbeat event handling
	heartbeat *heartbeatMonitor

	// channel to ping if you want to stop
	stopchannel chan struct{}
	// channel to indicate that we are stopped
	stopped chan struct{}

	// pool for handling events
	pool *workerpool.WorkerPool

	// log2(the number of reconciles) to calculate how many bits we need
	rec_bits int

	// variables for batched status writes
	io_pool          *workerpool.WorkerPool
	io_buffered_txn  storage.EtcdTransaction
	io_buffered_keys sets.Set[string]
	io_ticker        *qosticker.QOSTicker
	io_index         atomic.Uint64
	io_remaining     atomic.Int32
	io_queued        chan struct{}
	io_flushed       chan struct{}
	io_flusher       sync.Mutex
	io_wg            altsync.WaitGroup

	merged         chan reconcilerHandlerEvent
	watch_chans    []watchTuple
	periodic_chans []*reconcilerTicker

	err       error
	err_mutex sync.Mutex
}

type watchTuple struct {
	watch      clientv3.WatchChan
	reconciler int
}

// Runs multiple ReconcilerManagers in parallel
// If any of them stops, the rest are stopped, and all errors are returned
func RunReconcilerManagers(rms ...*ReconcilerManager) error {
	if len(rms) == 0 {
		return nil
	}

	var once sync.Once
	var wg sync.WaitGroup

	errs := make([]error, len(rms))
	end := make(chan struct{})

	wg.Add(1)
	go func() {
		<-end

		for _, rm := range rms {
			rm.Stop()
		}

		wg.Done()
	}()

	for i, rm := range rms {
		i := i
		rm := rm

		wg.Add(1)
		go func() {
			defer wg.Done()
			defer once.Do(func() { close(end) })

			errs[i] = rm.Run()
		}()
	}

	wg.Wait()
	return errors.Join(errs...)
}

func (rm *ReconcilerManager) setDefaults() {
	if rm == nil {
		return
	}

	if rm.JustLogErrors {
		log.Infof("[%s] when encountering errors, will not fatally abort", rm.Manager)
	}

	for _, r := range rm.Reconcilers {

		if r.PollCheckFrequency == 0 {
			r.PollCheckFrequency = defaultPollCheckFrequency
		}

		if r.PeriodicMessageFrequency == 0 {
			r.PeriodicMessageFrequency = defaultPeriodicMessageFrequency
		}

		if r.ensuredTaskStatuses == nil {
			r.ensuredTaskStatuses = xsync.NewMapOf[string, *TaskStatus]()
		}

		if r.ensuredTaskValues == nil {
			r.ensuredTaskValues = xsync.NewMapOf[string, *storage.GenericEtcdKey]()
		}

		if r.handledRevisions == nil {
			r.handledRevisions = xsync.NewMapOf[string, revisionHistory]()
		}

		if r.taskQueues == nil {
			r.taskQueues = xsync.NewMapOf[string, *taskQueue]()
		}

		r.startTime = timestamppb.Now()
	}

	cpus := runtime.NumCPU()

	switch w := rm.Workers; {
	case w < 0:
		rm.Workers = cpus
	case w == 0 || w == 1:
		rm.Workers = 1
	}
	log.Infof("[%s] using %d workers", rm.Manager, rm.Workers)

	if rm.HealthFrequency == 0 {
		rm.HealthFrequency = defaultHealthFrequency
	}

	if rm.ServerHeartbeatGracePeriod == 0 {
		rm.ServerHeartbeatGracePeriod = defaultHeartbeatGracePeriod
	}

	// disallow grace periods between 0 and the minimum threshold
	if rm.ServerHeartbeatGracePeriod > 0 && rm.ServerHeartbeatGracePeriod < minimumHeartbeatGracePeriod {
		rm.ServerHeartbeatGracePeriod = minimumHeartbeatGracePeriod
	}

	if rm.LogLevel == TaskMessage_Undefined {
		rm.LogLevel = defaultLogLevel
	}
	if rm.DisableMessageLogging {
		log.Infof("[%s] disabled auto logging of Actions results", rm.Manager)
	} else {
		log.Infof("[%s] auto logging of Actions results of level %s or higher", rm.Manager, rm.LogLevel.String())
	}

	rm.stopchannel = make(chan struct{})

	rm.stopped = make(chan struct{})

	if rm.pool == nil {
		rm.pool = workerpool.New(rm.Workers)
	}

	if rm.io_pool == nil {
		rm.io_pool = workerpool.New(1)
	}

	if rm.io_ticker == nil {
		rm.io_ticker = qosticker.NewQOSTicker(ioTickerInterval, ioTickerMultipler)
	}

	if rm.io_buffered_keys == nil {
		rm.io_buffered_keys = sets.NewSet[string]()
	}

	if rm.io_flushed == nil {
		rm.io_flushed = make(chan struct{})
	}

	if rm.io_queued == nil {
		rm.io_queued = make(chan struct{})
	}

	for i := len(rm.Reconcilers); i >= 2; i = (i + 1) / 2 {
		rm.rec_bits++
	}
}

// Stop the tasks in this manager.
// If you run it multiple times, all calls will block until it's stopped
func (rm *ReconcilerManager) Stop() bool {
	select {
	// if someone else is actually watching the stopchannel (aka Run() is happening somewhere),
	// wait for it to return
	case rm.stopchannel <- struct{}{}:
		rm.running.Lock()
		rm.running.Unlock()
		return true
	default:
		log.Infof("[%s] already stopped", rm.Manager)
		rm.running.Lock()
		rm.running.Unlock()
		return false
	}
}

func (rm *ReconcilerManager) stopWithError(err error) {
	if err == nil {
		return
	}

	log.Error(err)

	rm.joinError(err)

	go rm.Stop()
}

func (rm *ReconcilerManager) WaitAllUntil(targets ...ReconcilerOp) error {
	if rm == nil {
		return fmt.Errorf("tm is nil")
	}

	var wg sync.WaitGroup

	errs := make([]error, len(rm.Reconcilers))

	for i := range rm.Reconcilers {
		t := rm.Reconcilers[i]

		wg.Add(1)

		go func(i int) {
			defer wg.Done()
			errs[i] = t.WaitUntil(targets...)
		}(i)
	}

	wg.Wait()

	var err error = nil
	for _, e := range errs {
		if e != nil {
			if err == nil {
				err = e
			} else {
				err = fmt.Errorf("%+v; %+v", err, e)
			}
		}
	}

	return err
}

// Run the tasks.
// You can do tm.Run() again if you tm.Stop() later on
func (rm *ReconcilerManager) Run() error {
	if !rm.running.TryLock() {
		return fmt.Errorf("already running")
	}

	defer rm.running.Unlock()

	rm.setDefaults()

	ops := new(storage.EtcdTransaction)

	var err error

	revision, err := storage.GetCurrentRevision(rm.EtcdCli)

	if err != nil {
		err := fmt.Errorf("failed to get current revision: %+v", err)
		log.Error(err)
		return err
	}

	// init health objects
	for _, rh := range NewReconcilerHealths(rm) {
		err := rh.AppendCreateOps(rm.EtcdCli, ops)

		if err != nil {
			err := fmt.Errorf("error creating rec health object: %+v", err)
			log.Error(err)
			return err
		}
	}

	_, _, err = ops.EtcdTx(rm.EtcdCli, "")
	if err != nil {
		err := fmt.Errorf("error writing rec health object: %+v", err)
		log.Error(err)
		return err
	}

	// establish a heartbeat monitor
	if rm.ServerHeartbeatGracePeriod < 0 {
		log.Infof("[%s/heartbeat-monitor] heartbeats disabled", rm.Manager)
	} else {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		rm.heartbeat = newHeartbeatMonitor(ctx, rm.ServerHeartbeatGracePeriod)
		go func() {
			err := rm.heartbeat.run(rm.EtcdCli)
			rm.stopWithError(err)
		}()
	}

	rm.periodic_chans = nil
	rm.watch_chans = nil

	for i, t := range rm.Reconcilers {
		log.Debugf("[%s/%s] starting task", rm.Manager, t.Name)

		ctx, cancel := context.WithCancel(context.Background())
		t.cancel = cancel

		rm.watch_chans = append(
			rm.watch_chans,
			watchTuple{
				rm.EtcdCli.Watch(
					ctx,
					t.Prefix,
					clientv3.WithPrefix(),
					clientv3.WithRev(revision+1),
				),
				i,
			},
		)

		if t.EnsureFrequency <= 0 {
			log.Infof("[%s/%s] periodic ensure disabled", rm.Manager, t.Name)
		} else {
			log.Infof("[%s/%s] periodic ensure frequency of: %s", rm.Manager, t.Name, t.EnsureFrequency.String())

			rm.periodic_chans = append(
				rm.periodic_chans,
				&reconcilerTicker{
					reconcilerToTick: i,
					eventType:        ReconcilerManagerEventEnsure,
					ticker:           qosticker.NewQOSTicker(t.EnsureFrequency, maintenanceTickerMultipler),
				},
			)
		}

		if t.PeriodicMessageFrequency <= 0 {
			log.Infof("[%s/%s] periodic timeout warning disabled", rm.Manager, t.Name)
		} else {
			log.Infof("[%s/%s] periodic timeout warning frequency of: %s", rm.Manager, t.Name, t.PeriodicMessageFrequency.String())
		}

		if t.PollCheckFrequency <= 0 {
			log.Infof("[%s/%s] periodic pollcheck disabled", rm.Manager, t.Name)
		} else {
			log.Infof("[%s/%s] periodic pollcheck frequency of: %s", rm.Manager, t.Name, t.PollCheckFrequency.String())

			rm.periodic_chans = append(
				rm.periodic_chans,
				&reconcilerTicker{
					reconcilerToTick: i,
					eventType:        ReconcilerManagerEventPollCheck,
					ticker:           qosticker.NewQOSTicker(t.PollCheckFrequency, maintenanceTickerMultipler),
				},
			)
		}

	}

	// add periodic task for reconciler health checks
	if rm.HealthFrequency <= 0 {
		log.Infof("[%s] periodic reconciler health check disabled, expect unresponsive statuses", rm.Manager)
	} else {
		log.Infof("[%s] periodic reconciler health check frequency of: %s", rm.Manager, rm.HealthFrequency.String())

		rm.periodic_chans = append(
			rm.periodic_chans,
			&reconcilerTicker{
				eventType: ReconcilerManagerEventHealth,
				ticker:    qosticker.NewQOSTicker(rm.HealthFrequency, maintenanceTickerMultipler),
			},
		)
	}

	rm.mergeChans()

	for i := range rm.Reconcilers {
		err := rm.initFromState(i, revision)
		if err != nil {
			rm.joinError(err)
		}
	}

	if rm.err != nil {
		log.Error(rm.err)
		return err
	}

	// Start all tickers after initializing everything
	for _, p := range rm.periodic_chans {
		p.ticker.Start()
	}

	// main loop
	for output := range rm.merged {
		if !rm.handleTaskEvent(output) {
			break
		}
	}

	// failsafe in case things hang
	stopped := make(chan struct{})
	defer close(stopped)
	go func() {
		select {
		case <-stopped:
		case <-time.After(stopTimeout):
			log.Fatalf("[%s] took too long to stop, err: %v", rm.Manager, rm.err)
		}
	}()

	log.Infof("[%s] stopping...", rm.Manager)

	// main loop ended, so clean up
	for _, p := range rm.periodic_chans {
		p.ticker.Stop()
	}

	rm.pool.StopWait()
	rm.pool = nil

	rm.io_wg.Wait()

	rm.io_pool.StopWait()
	rm.io_pool = nil

	rm.io_ticker.Stop()
	rm.io_ticker = nil

	close(rm.io_flushed)
	rm.io_flushed = nil

	for _, r := range rm.Reconcilers {
		log.Infof("[%s/%s] stopping task...", rm.Manager, r.Name)
		r.Stop()
	}

	// wait for rm.merged to be closed
	for range rm.merged {
	}

	log.Infof("[%s] all stopped", rm.Manager)

	rm.merged = nil

	rm.err_mutex.Lock()
	defer rm.err_mutex.Unlock()
	if rm.err != nil {
		if rm.JustLogErrors {
			log.Error(rm.err)
		} else {
			log.Fatal(rm.err)
		}

	}

	return rm.err
}

// There are three parts to this:
// From most significant to least significant
// [64:60]/04: General Priority
// [60:XX]/44-RB: Revision
// [XX:16]/RB: Task Index, depends on how many bits we need to index every task
// [16:00]/16: Offset, usually alphabetical order index
// RB stands for rec bits, the number of bits we need to index all reconcilers
//
//	Offset is so that keys in the same revision are in order when we process them,
//	other the heap data structure puts them in mostly reverse order
func (rm *ReconcilerManager) composePriority(priority uint64, revision int64, rec_index int, offset int) uint64 {
	// already shifted by 60 bits
	x := uint64(priority)

	// we use the lower 44-rec.bits bits of the revision
	// we probably don't need more than the amount of milliseconds in a century
	x |= (uint64(revision) & (1<<(45-rm.rec_bits) - 1)) << (16 + rm.rec_bits)

	// rec index, we only use as many bits as needed to index every task
	x |= (uint64(rec_index) & (1<<(rm.rec_bits+1) - 1)) << uint64(rm.rec_bits)

	// lower 16 bits of the offset
	x |= uint64(offset) & (1<<16 - 1)
	return uint64(x)
}

func (rm *ReconcilerManager) joinError(err error) {
	rm.err_mutex.Lock()
	defer rm.err_mutex.Unlock()

	rm.err = errors.Join(rm.err, err)
}

func (rm *ReconcilerManager) handleTaskEvent(output reconcilerHandlerEvent) bool {
	switch output.eventType {
	case ReconcilerManagerEventStop:
		close(rm.stopchannel)
		close(rm.stopped)
		rm.stopchannel = nil

		return false

	case ReconcilerManagerEventEtcdWatch:
		rec_index := output.reconcilerOrigin

		if output.etcdWatchResponse.Err() != nil {
			rm.stopWithError(fmt.Errorf("[%s] EtcdWatchResponse: %+v", rm.Manager, output.etcdWatchResponse.Err()))
			break
		}

		if output.etcdWatchResponse.Canceled {
			rm.stopWithError(fmt.Errorf("[%s] EtcdWatchResponse cancelled", rm.Manager))
			break
		}

		for i, e := range output.etcdWatchResponse.Events {
			i := i
			k := string(e.Kv.Key)
			e := e

			rm.io_remaining.Add(1)
			rm.submitTaskWithKey(k, NormalPriority, e.Kv.ModRevision, rec_index, i, func() {
				rm.handleEvent(k, e, rm.Reconcilers[rec_index])
			})
		}

	case ReconcilerManagerEventEnsure:
		// if we put it in tm.pool, it would take a worker,
		// blocking itself when single threaded
		go func() {
			err := rm.handleEnsureEvent(output)
			if err != nil {
				rm.stopWithError(err)
			}
		}()

	case ReconcilerManagerEventPollCheck:
		go func() {
			err := rm.handlePollCheckWrapper(output)
			if err != nil {
				rm.stopWithError(err)
			}
		}()

	case ReconcilerManagerEventHealth:
		// maintainance task, so do this in the background
		for _, r := range rm.Reconcilers {
			r.incrementRunningTasks()
		}

		rm.pool.Submit(math.MaxUint64, func() {
			for _, r := range rm.Reconcilers {
				defer r.decrementRunningTasks()
			}

			err := rm.handleHealthEvent()

			if err != nil {
				rm.stopWithError(err)
			}
		})
	}

	return true

}

// returns if this actually handled the event
// if it's not ready, then it cannot handle the event and return false
func (rm *ReconcilerManager) handleEvent(k string, e *clientv3.Event, r *Reconciler) {
	skipped := true

	defer func() {
		if skipped {
			rm.io_remaining.Add(-1)
		}
	}()

	actions, err := r.cloneActions()
	if err != nil {
		rm.stopWithError(fmt.Errorf("[%s/%s]: %+v", rm.Manager, r.Name, err))
		return
	}

	if actions.Parse(k) {
		skipped = false
		ts, _ := r.ensuredTaskStatuses.Load(k)

		log.Debugf("[%s/%s] accepted event %s @ %d", rm.Manager, r.Name, e.Kv.Key, e.Kv.ModRevision)

		if ts.GetTaskRevision() > e.Kv.ModRevision {
			rm.stopWithError(
				fmt.Errorf("[%s/%s] somehow, the latest handled revision is higher than an incoming task: %d vs. %d",
					rm.Manager,
					r.Name,
					ts.GetTaskRevision(),
					e.Kv.ModRevision,
				))
		}

		var err error
		switch e.Type {
		case mvccpb.PUT:
			err = rm.handlePut(storage.NewGenericEtcdKeyFromKV(e.Kv), r, ts, actions)
		case mvccpb.DELETE:
			_, err = rm.handleDelete(k, e.Kv.ModRevision, r, ts, actions)
		default:
			log.Errorf("unknown e.Type: %+v", e.Type)
		}

		if err != nil {
			rm.stopWithError(err)
		}
	}
}

func (rm *ReconcilerManager) submitTaskWithKey(k string, priority uint64, revision int64, rec_index int, offset int, f func()) {
	p := rm.composePriority(priority, revision, rec_index, offset)
	//log.Infof("composePriority: %s: %x", k, p)

	r := rm.Reconcilers[rec_index]

	// get current or initialize
	q, _ := r.taskQueues.LoadOrStore(k, &taskQueue{})

	// add the current revision to the priority queue

	q.Push(p, p)

	r.incrementRunningTasks()

	times := 0
	last_warning := time.Now()

	var resubmit func()
	var g func()

	resubmit = func() {
		rewarn_time := time.Duration(times) * taskQueueNotReadySleepInterval
		if rewarn_time >= taskQueueNotReadyWarnDuration && time.Since(last_warning) >= taskQueueNotReadyWarnDuration {
			last_warning = time.Now()

			// if workers is 1, then as nothing else is running,
			// the front of the queue IS the lowest priority item,
			// yet we cannot run it for some reason
			if rm.Workers == 1 {
				err := fmt.Errorf("[%s/%s] %s : @ %d this task has been blocked for %s, something went wrong as workers==1, nothing should be blocked, queue: %s",
					rm.Manager,
					r.Name,
					k,
					revision,
					rewarn_time,
					q.String(),
				)
				rm.stopWithError(err)

				return
			}

			last_warning = time.Now()

			log.Warnf("[%s/%s] %s : @ %d this task has been blocked for %s, something might've gone wrong, queue: %s",
				rm.Manager,
				r.Name,
				k,
				revision,
				rewarn_time,
				q.String(),
			)
		}

		times++
		time.Sleep(taskQueueNotReadySleepInterval)

		select {
		// stopped, so don't resubmit
		case <-rm.stopped:
			return
		default:
			rm.pool.Submit(p, g)
		}
	}

	g = func() {
		// not ready, so come back later
		if q.Front() != p {
			resubmit()
			return
		}

		q.lock.Lock()
		defer q.lock.Unlock()

		// check again, it could've changed after we've waited to get the lock
		if q.Front() != p {
			resubmit()
			return
		}

		defer r.decrementRunningTasks()

		// actually do the task
		f()

		// remove itself from the queue
		// we may not actually be at the beginning of the queue
		// so don't use pop and manually go through the queue
		// usually though, we are at the front or near it
		removed := q.Remove(func(x uint64) bool {
			return x == p
		})

		if !removed {
			rm.stopWithError(
				fmt.Errorf("[%s/%s] %s : somehow, we couldn't remove itself from the queue", rm.Manager, r.Name, k),
			)
		}

		if q.Len() == 0 {
			// delete from the map if the value in the map has a length of 0
			r.taskQueues.Compute(k, func(oldValue *taskQueue, loaded bool) (newValue *taskQueue, delete bool) {
				if !loaded {
					return
				}

				if oldValue.Len() == 0 {
					delete = true
					return
				}

				newValue = oldValue
				return
			})
		}
	}

	rm.pool.Submit(p, g)
}

func (rm *ReconcilerManager) bufferOps(ops storage.EtcdTransaction) {
	defer rm.io_wg.Done()

	// queue flusher if not already present
	if rm.io_flusher.TryLock() {
		rm.io_wg.Add(1)

		var final_flusher func()
		final_flusher = func() {
			remaining := rm.io_remaining.Load()

			// don't bother waiting, just flush as there's no keys to wait for
			if remaining <= 0 {
				if remaining < 0 {
					log.Errorf("[%s] somehow, remaining is < 0: %d", rm.Manager, remaining)
				}

				defer rm.io_flusher.Unlock()
				defer rm.io_wg.Done()
				rm.flushBufferedOps()

				return
			}

			select {
			// resubmit, there's something new
			case <-rm.io_queued:
				rm.io_pool.Submit(math.MaxUint64, final_flusher)

			// actually flush
			case <-rm.io_ticker.TickChannel():
				defer rm.io_flusher.Unlock()
				defer rm.io_wg.Done()
				rm.flushBufferedOps()
			}
		}

		rm.io_pool.Submit(math.MaxUint64, final_flusher)
	}

	for _, s_op := range ops.Ops {
		k := s_op.Object.Key()

		// flush if we have a duplicate key
		if rm.io_buffered_keys.Contains(k) {
			rm.flushBufferedOps()
		}

		rm.io_buffered_keys.Add(k)
	}

	rm.io_buffered_txn.Merge(&ops)

	// flush if we exceed the txn op limit (including reconciler healths)
	if rm.io_buffered_keys.Cardinality()+len(rm.Reconcilers) >= storage.DEFAULT_TXN_LIMIT {
		rm.flushBufferedOps()
	}
}

func (rm *ReconcilerManager) flushBufferedOps() {
	log.Tracef("[%s] flushing buffered operations: %+v", rm.Manager, rm.io_buffered_keys)

	// only write if we actually have ops
	if rm.io_buffered_keys.Cardinality() > 0 {
		start := time.Now()

		rhs := NewReconcilerHealths(rm)

		for _, rh := range rhs {
			err := rh.AppendUpdateOps(rm.EtcdCli, &rm.io_buffered_txn)
			if err != nil {
				rm.stopWithError(err)
			}
		}

		_, _, err := rm.io_buffered_txn.EtcdTx(rm.EtcdCli, "")
		if err != nil {
			rm.stopWithError(err)
		}

		latency := time.Since(start)

		rm.io_ticker.UpdateQOS(latency)
	}

	rm.io_buffered_keys = sets.NewSet[string]()
	rm.io_buffered_txn = storage.EtcdTransaction{}

	for cont := true; cont; {
		select {
		// broadcast to all that we flushed
		case rm.io_flushed <- struct{}{}:
		default:
			cont = false
		}
	}

}

func (rm *ReconcilerManager) bufferedWriteStatusOps(ops storage.EtcdTransaction) *sync.WaitGroup {
	defer rm.io_remaining.Add(-1)

	rm.io_wg.Add(1)

	// submit adding the ops to the global list
	x := rm.io_index.Add(1)

	var queued sync.WaitGroup

	queued.Add(1)

	rm.io_pool.Submit(x, func() {
		defer queued.Done()

		rm.io_ticker.ResetTimer()
		rm.io_ticker.Start()
		rm.bufferOps(ops)
	})

	// only send message that we queued something
	// if there's something watching it
	select {
	case rm.io_queued <- struct{}{}:
	default:
	}

	return &queued
}

func (rm *ReconcilerManager) handleHealthEvent() error {
	log.Debugf("[%s] handling health event", rm.Manager)
	defer log.Debugf("[%s] finished health event", rm.Manager)

	for _, t := range rm.Reconcilers {
		t.updateTaskChannel(ReconcilerOp_Health)
	}

	rhs := NewReconcilerHealths(rm)
	ops := new(storage.EtcdTransaction)

	for _, rh := range rhs {
		err := rh.AppendUpdateOps(rm.EtcdCli, ops)
		if err != nil {
			return err
		}
	}
	_, _, err := ops.EtcdTx(rm.EtcdCli, "")
	if err != nil {
		return err
	}

	// resume health events
	for _, p := range rm.periodic_chans {
		if p.eventType == ReconcilerManagerEventHealth {
			p.ticker.Start()
		}
	}

	return nil
}

func (rm *ReconcilerManager) handleEnsureHelper(k string, r *Reconciler, rev int64) *storage.EtcdTransaction {
	actions, err := r.cloneActions()
	if err != nil {
		rm.stopWithError(fmt.Errorf("[%s/%s]: %+v", rm.Manager, r.Name, err))
		return nil
	}

	if actions.Parse(k) {
		rh, _ := r.handledRevisions.Load(k)

		if rh.revision > rev {
			log.Infof("[%s/%s/ensure] %s : skipping, already handled a higher revision (%d > %d)", rm.Manager, r.Name, k, rh.revision, rev)
			return nil
		}

		ts, ok1 := r.ensuredTaskStatuses.Load(k)
		tk, ok2 := r.ensuredTaskValues.Load(k)

		if !ok1 || !ok2 {
			return nil
		}

		n_ops, err := rm.handleEnsure(tk, ts, r, actions)
		if err != nil {
			rm.stopWithError(err)
		}

		return n_ops
	}

	return nil
}

func (rm *ReconcilerManager) handleEnsureEvent(output reconcilerHandlerEvent) error {
	r := rm.Reconcilers[output.reconcilerOrigin]

	// to write all of the completed statuses at once
	var wg sync.WaitGroup

	// pause everything in the same task
	// (pointer comparison is good enough)
	for _, p := range rm.periodic_chans {
		if p.reconcilerToTick == output.reconcilerOrigin {
			p.ticker.Pause()
		}
	}

	r.poolWg.Wait()

	keys := r.getEnsuredTasksKeys()

	// measure the amount of time ensuring the keyspace takes,
	// so we can adjust the time between ensure events, for QOS reasons

	start := time.Now()

	// actually handle the event
	for i, k := range keys {
		i := i
		k := k

		tk, ok := r.ensuredTaskValues.Load(k)

		if !ok || tk == nil {
			// somehow, missing tk -- probably deleted between getting all keys
			continue
		}

		rm.io_remaining.Add(1)
		wg.Add(1)

		rm.submitTaskWithKey(k, LowPriority, tk.ModRevision, output.reconcilerOrigin, i, func() {
			defer wg.Done()

			ops := rm.handleEnsureHelper(k, r, tk.ModRevision)

			if ops != nil && len(ops.Ops) > 0 {
				queued := rm.bufferedWriteStatusOps(*ops)

				queued.Wait()
				<-rm.io_flushed
			}
		})
	}

	wg.Wait()
	diff := time.Since(start)

	output.reconcilerTicker.ticker.UpdateQOS(diff)

	for _, p := range rm.periodic_chans {
		if p.reconcilerToTick == output.reconcilerOrigin {
			p.ticker.Start()
		}
	}

	return nil

}

func (rm *ReconcilerManager) handlePollCheckWrapper(output reconcilerHandlerEvent) error {
	t := rm.Reconcilers[output.reconcilerOrigin]

	// pause everything in the same task
	// (pointer comparison is good enough)
	for _, p := range rm.periodic_chans {
		if p.reconcilerToTick == output.reconcilerOrigin {
			p.ticker.Pause()
		}
	}

	revision, err := storage.GetCurrentRevision(rm.EtcdCli)

	if err != nil {
		return fmt.Errorf("pollcheck: failed to get current revision: %+v", err)
	}

	log.Debugf("[%s/%s] handlePollCheck @ %d", rm.Manager, t.Name, revision)

	// request a progress notify to make sure we're up to date
	ctx, cancel := context.WithTimeout(context.Background(), contextTimeout)
	defer cancel()
	err = rm.EtcdCli.RequestProgress(ctx)

	// sleep to make sure that we
	// receive any ongoing watches
	time.Sleep(pollcheckWaitForProgressNotify)

	// measure the amount of time poll checking takes
	// so we can adjust the time between ensure events, for QOS reasons
	start := time.Now()
	err = rm.handlePollCheckEvent(revision, t)

	if err != nil {
		return fmt.Errorf("[%s/%s] handlePollCheck: %+v", rm.Manager, t.Name, err)
	}

	diff := time.Since(start)

	output.reconcilerTicker.ticker.UpdateQOS(diff)

	for _, p := range rm.periodic_chans {
		if p.reconcilerToTick == output.reconcilerOrigin {
			p.ticker.Start()
		}
	}

	return nil
}

func (rm *ReconcilerManager) handlePollCheckEvent(revision int64, r *Reconciler) error {

	// wait for everything else to finish
	r.poolWg.Wait()

	// increment tasks now because if we do it before .Wait(),
	// we will deadlock
	r.incrementRunningTasks()
	defer r.decrementRunningTasks()

	r.updateTaskChannel(ReconcilerOp_Pollcheck)

	ctx, cancel := context.WithTimeout(context.Background(), contextTimeout)
	defer cancel()

	task_resp, err := rm.EtcdCli.Get(
		ctx,
		r.Prefix,
		clientv3.WithPrefix(),
		clientv3.WithRev(revision),
	)

	if err != nil {
		return fmt.Errorf("failed to read etcd prefix key space: %s: %+v", r.Prefix, err)
	}

	actions, err := r.cloneActions()
	if err != nil {
		return fmt.Errorf("[%s/%s]: %+v", rm.Manager, r.Name, err)
	}

	// maps keys to revisions
	expected := make(map[string]revisionHistory)

	r.handledRevisions.Range(func(key string, value revisionHistory) bool {
		expected[key] = value
		return true
	})

	for _, kvs := range task_resp.Kvs {
		k := string(kvs.Key)
		actual_rev := kvs.ModRevision

		if actions.Parse(k) {
			handled, found := expected[k]

			if !found || handled.revision <= 0 {
				return fmt.Errorf("%s seems to have been missed by the engine", k)
			} else if actual_rev > handled.revision {
				return fmt.Errorf("%s latest revision is %d, but the reconciler only sees revision %d", k, actual_rev, handled.revision)
			}
		}

		delete(expected, k)
	}

	// we have extra keys that wasn't present at the revision number
	var missing []string
	for k, handled := range expected {

		// if the key was last deleted,
		// then we should expect that it's still in the history
		if handled.deleted {
			// we can delete this from the history if we haven't handled anything in the meantime
			r.handledRevisions.Compute(k, func(oldValue revisionHistory, loaded bool) (newValue revisionHistory, delete bool) {
				if !loaded {
					return
				}

				if oldValue == handled {
					delete = true
					return
				}

				newValue = oldValue
				return
			})
			continue
		}

		// the key wasn't last deleted, but we have a higher revision
		// this probably means we handled a put between when we started this function
		// and what was last handled
		if handled.revision > revision {
			continue
		}

		log.Errorf("[%s/%s] %s @ %d : missing delete, last handled: %d",
			rm.Manager, r.Name,
			k, revision,
			handled.revision,
		)

		missing = append(missing, k)
	}

	if len(missing) > 0 {
		return fmt.Errorf(
			"[%s/%s] @ %d : keys seems to be deleted, but the engine has missed them",
			rm.Manager, r.Name, revision)
	}

	return nil

}

// handleDelete handles when a key is Delete and writes a status accordingly.
// If the return message is:
//   - nil: assumes that it was completed successfully
//   - of level Error: assumes that it had an error while deleting
//   - of level Warning/Info/Debug/Trace: assumes that it was deleted successfully
//   - of level Undefined: assume that the status in the reconciler has been manually set, so write that instead
//     this is so the tasks can manually set the status if need be
func (rm *ReconcilerManager) handleDelete(key string, revision int64, r *Reconciler, ts *TaskStatus, actions Actions) (*TaskStatus, error) {
	var err error

	// we will set the task value after we read from etcd
	td := NewTaskData(storage.NewGenericEtcdKey(key, nil), rm, r)

	if ts == nil {
		td.getCachedTaskStatusOrDefault()
	} else {
		td.Status = ts.Clone()
	}

	version := td.Status.GetTaskVersion()
	prev := td.Status.GetPrevValue()

	if version == 0 &&
		prev == nil &&
		td.Status.GetMessages() == nil &&
		td.Status.GetSelfVersion() == 0 &&
		td.Status.GetTaskRevision() == 0 &&
		td.Status.GetLastStatus() == TaskStatus_Undefined {

		log.Infof("[%s/%s/delete] %s @ %d : skipping, was never handled", rm.Manager, r.Name, key, revision)
		r.unregisterTaskKey(key, revision)
		return nil, nil
	}

	td.TaskValue.EtcdValue = prev
	td.TaskValue.SetVersion(0)
	td.TaskValue.ModRevision = revision

	log.Infof("[%s/%s/delete] %s @ %d : handling", rm.Manager, r.Name, key, revision)
	defer log.Infof("[%s/%s/delete] %s @ %d : finished", rm.Manager, r.Name, key, revision)

	r.updateTaskChannel(ReconcilerOp_Delete)

	var m *TaskMessage
	periodicMessageWarnfDuration(
		func() { m = actions.Delete(prev, version, td) },
		r.PeriodicMessageFrequency,
		"[%s/%s/delete] %s @ d : has been processing for %s...", rm.Manager, r.Name, key, revision,
	)

	m.logByReconciler(rm.LogLevel, rm.DisableMessageLogging, rm.Manager, r.Name, "delete", key)

	canDeleteStatus := true

	if m != nil {
		result := levelToStatusHandleDelete[m.Level]

		// If the result is undefined, assume that the current status in the reconciler is what we want,
		// so we don't have to do anything
		if result != TaskStatus_Undefined {
			td.SetStatusClearMessages(result, m)
		}

		if result == TaskStatus_Error {
			canDeleteStatus = false
		}
	}

	if canDeleteStatus {
		r.unregisterTaskKey(key, revision)
	} else {
		r.registerTaskKey(key, true, ts, td.TaskValue)
	}

	var ops storage.EtcdTransaction

	r.bumpKeysHandled(key)

	// we clone the status so that when it gets around to being written,
	// we can avoid data races between the status being written to etcd
	// and whatever we have in the cache
	write_ts := td.Status.Clone()
	if canDeleteStatus {
		err = write_ts.AppendDeleteOps(rm.EtcdCli, &ops)
	} else {
		err = write_ts.AppendUpdateOps(rm.EtcdCli, &ops)
	}

	if err != nil {
		err := fmt.Errorf("[%s/%s/delete] %s @ %d : error updating status ops: %+v", rm.Manager, r.Name, key, revision, err)
		return td.Status, err
	}

	queued := rm.bufferedWriteStatusOps(ops)

	r.incrementRunningTasks()
	go func() {
		defer r.decrementRunningTasks()

		queued.Wait()
		<-rm.io_flushed

		log.Debugf("[%s/%s/delete] %s @ %d : updated status", rm.Manager, r.Name, key, revision)
	}()

	return nil, nil
}

// handlePut handles when a key is Put and writes a status accordingly.
// If the return message is:
//   - nil: assumes that it was completed successfully
//   - of level Error: assumes that it had an error while deleting
//   - of level Warning/Info/Debug/Trace: assumes that it was completed successfully
//   - of level Undefined: assume that the status in the reconciler has been manually set, so write that instead
//     this is so the tasks can manually set the status if need be
func (rm *ReconcilerManager) handlePut(etcdkey *storage.GenericEtcdKey, r *Reconciler, ts *TaskStatus, actions Actions) error {
	var err error
	td := NewTaskData(etcdkey, rm, r)

	if ts == nil {
		td.getCachedTaskStatusOrDefault()
	} else {
		td.Status = ts.Clone()
	}

	prev := td.Status.GetPrevValue()

	// check to see if we have an unhandled delete:
	// the create revision is higher than the last handled revision (and we have a last handled revision)
	// if revision is <=0 (because previous versions did not track modrevision), then we have to infer, so delete the key iff:
	//  - the last reconciled version is higher than the current version
	//  - the versions are identical, but the values are different

	doDelete := false
	if ts.GetTaskRevision() > 0 {
		if ts.GetTaskRevision() < etcdkey.CreateRevision {
			doDelete = true
		}
	} else if (ts.GetTaskVersion() > etcdkey.Version) ||
		(etcdkey.Version == ts.GetTaskVersion() && !bytes.Equal(etcdkey.EtcdValue, prev)) {
		doDelete = true
	}

	if doDelete {
		log.Infof("[%s/%s/put] %s @ %d : key has been recreated, deleting first", rm.Manager, r.Name, etcdkey.EtcdKey, etcdkey.CreateRevision)
		if ts.GetTaskRevision() > 0 {
			log.Debugf("ts.GetTaskRevision(): %d, kv.CreateRevision: %d", ts.GetTaskRevision(), etcdkey.CreateRevision)
		} else {
			log.Debugf("ts.GetTaskVersion(): %d, kv.Version: %d", ts.GetTaskVersion(), etcdkey.Version)
			log.Debugf("bytes.Equal(kv.Value, prev): %t", bytes.Equal(etcdkey.EtcdValue, prev))
		}

		rm.io_remaining.Add(1)
		ts, err = rm.handleDelete(etcdkey.EtcdKey, etcdkey.CreateRevision, r, ts, actions)

		// don't crash on error,
		// as if it doesn't actually exist, it might error out
		if err != nil {
			log.WithFields(log.Fields{"err": err}).Error("init delete into put key")

			rm.joinError(err)
		}

		// remake ts
		if ts == nil {
			td.getCachedTaskStatusOrDefault()
		} else {
			td.Status = ts.Clone()
		}

		td.TaskValue = etcdkey
		prev = td.Status.GetPrevValue()
	}

	// run handleEnsure instead if it's already reconciled
	if td.Status.IsReconciled(td.TaskValue) {
		log.Infof("[%s/%s/put] %s @ %d : already reconciled, ensuring instead", rm.Manager, r.Name, etcdkey.EtcdKey, etcdkey.ModRevision)
		log.Tracef("[%s/%s] %s : version %d/%d vs %d/%d", rm.Manager, r.Name, etcdkey.EtcdKey, etcdkey.Version, etcdkey.ModRevision, ts.GetTaskVersion(), ts.GetTaskRevision())

		ops, err := rm.handleEnsure(etcdkey, td.Status.Clone(), r, actions)

		if err != nil {
			return err
		}

		if len(ops.Ops) != 0 {
			_, _, err = ops.EtcdTx(rm.EtcdCli, "")
			if err != nil {
				log.Errorf("final status update: %+v", err)
			}
		}

		return err

	}

	_, havePrev := r.ensuredTaskStatuses.Load(etcdkey.Key())

	var m *TaskMessage
	op := "update"
	if etcdkey.Version == 1 || !havePrev {
		op = "create"
		td.Status.Creation = timestamppb.Now()
	}

	callback_done := make(chan struct{})

	log.Infof("[%s/%s/%s] %s @ %d : handling", rm.Manager, r.Name, op, etcdkey.EtcdKey, etcdkey.ModRevision)
	defer log.Infof("[%s/%s/%s] %s @ %d : finished", rm.Manager, r.Name, op, etcdkey.EtcdKey, etcdkey.ModRevision)

	// Run the callback in a separate goroutine, so we can observe how long it takes
	go func() {
		if etcdkey.Version == 1 || !havePrev {

			r.updateTaskChannel(ReconcilerOp_Create)

			m = actions.Create(etcdkey.EtcdValue, etcdkey.Version, td)
		} else {
			r.updateTaskChannel(ReconcilerOp_Update)

			m = actions.Update(prev, etcdkey.EtcdValue, etcdkey.Version, td)
		}
		close(callback_done)
	}()

	// If it's not finished in 1 second, update the status key as processing
	select {
	case <-callback_done:
	case <-time.After(1 * time.Second):
		// we clone the status as to not perturb the status in the reconcile variable,
		// as that's passed to the reconciler
		new_ts := td.Status.Clone()

		// if the task revision has been updated,
		// then it's likely already been written,
		// so don't write processing
		if new_ts.GetTaskRevision() != etcdkey.ModRevision {
			new_ts.LastStatus = TaskStatus_Processing
			new_ts.Messages = []*TaskMessage{TaskMessageInfo("processing key...")}
			// Update the version and value,
			// so that the status isn't interpreted as "Pending" instead of "Processing"
			new_ts.TaskVersion = etcdkey.Version
			new_ts.PrevValue = etcdkey.EtcdValue
			new_ts.TaskRevision = etcdkey.ModRevision

			// we do not bother with status write buffering, as that's for performance reasons,
			// so, if the callback is taking longer than 1 second, then the performance gained though buffering is negligible
			_, err = new_ts.Update(rm.EtcdCli)
			if err != nil {
				return fmt.Errorf("[%s/%s/%s] %s @ %d : put status error: %+v", rm.Manager, r.Name, op, etcdkey.EtcdKey, etcdkey.ModRevision, err)
			}

			td.Status.SelfVersion++
		}

		periodicMessageWarnfDuration(
			func() { <-callback_done },
			r.PeriodicMessageFrequency,
			"[%s/%s/%s] %s @ %d : has been processing for %s...", rm.Manager, r.Name, op, etcdkey.EtcdKey, etcdkey.ModRevision,
		)
	}

	// all cases of the select wait for <-callback_done,
	// so we do not proceed until its finished

	m.logByReconciler(rm.LogLevel, rm.DisableMessageLogging, rm.Manager, r.Name, op, etcdkey.EtcdKey)

	if m == nil {
		td.Status.PrevValue = etcdkey.EtcdValue
		td.SetComplete()
	} else {
		result := levelToStatusHandlePut[m.Level]
		// If the result is undefined,
		// assume that the current status in the reconciler is what we want
		// and don't do anything
		if result != TaskStatus_Undefined {
			td.Status.PrevValue = etcdkey.EtcdValue
			td.SetStatusClearMessages(result, m)
		}
	}

	r.registerTaskKey(etcdkey.EtcdKey, false, td.Status, etcdkey)
	r.incrementRunningTasks()

	r.bumpKeysHandled(etcdkey.EtcdKey)

	var ops storage.EtcdTransaction

	// we clone the status so that when it gets around to being written,
	// we can avoid data races between the status being written to etcd
	// and whatever we have in the cache
	write_ts := td.Status.Clone()

	err = write_ts.AppendUpdateOps(rm.EtcdCli, &ops)
	if err != nil {
		err := fmt.Errorf("[%s/%s/%s] %s @ %d : error appending status update ops: %+v", rm.Manager, r.Name, op, etcdkey.EtcdKey, etcdkey.ModRevision, err)
		return err
	}

	queued := rm.bufferedWriteStatusOps(ops)

	go func() {
		defer r.decrementRunningTasks()

		queued.Wait()
		<-rm.io_flushed

		log.Debugf("[%s/%s/%s] %s @ %d : updated status", rm.Manager, r.Name, op, etcdkey.EtcdKey, etcdkey.ModRevision)
	}()

	return nil
}

// handleEnsure handles when an Ensure callback is done and writes a status accordingly.
// If the return message is:
//   - nil: assumes that it was completed successfully
//   - of level Error: assumes that it had an error while deleting
//   - of level Warning/Info/Debug/Trace: assumes that it was completed successfully
//   - of level Undefined: assume that the status in the reconciler has been manually set, so write that instead
//     this is so the tasks can manually set the status if need be
//
// Statuses are only written if:
//   - the previous status is not a success
//   - the current status is not a success
//   - the current status has a message that is a warning or error
//   - the return message was undefined and the new status is different than the old status
//
// (this is to avoid flooding etcd with perodic writes)
func (rm *ReconcilerManager) handleEnsure(etcdkey *storage.GenericEtcdKey, ts *TaskStatus, r *Reconciler, actions Actions) (*storage.EtcdTransaction, error) {

	td := NewTaskData(etcdkey, rm, r)

	// we pass ts around from a cached version to avoid etcd reads
	// (as we should be the only one with the key/manager/name combo)
	if ts != nil {
		td.Status = ts
	}

	orig_status := ts.Clone()

	// If the key was deleted,
	// we should treat this as a delete that was somehow missed
	if etcdkey.Version == 0 {
		if ts != nil {
			log.Infof("[%s/%s/ensure] %s @ %d : treating as delete instead", rm.Manager, r.Name, etcdkey.EtcdKey, etcdkey.ModRevision)
			_, err := rm.handleDelete(etcdkey.EtcdKey, etcdkey.ModRevision, r, td.Status, actions)
			return nil, err
		}

		// both key and ts are missing
		// we should skip, but note that somehow we are in this state
		log.Warnf("%s is somehow registered, yet both the actual key and its reconcile status is deleted", etcdkey.EtcdKey)
		r.unregisterTaskKey(etcdkey.EtcdKey, etcdkey.ModRevision)
		return nil, nil
	}

	prev := ts.GetPrevValue()

	log.Debugf("[%s/%s/ensure] %s @ %d : handling", rm.Manager, r.Name, etcdkey.EtcdKey, etcdkey.ModRevision)
	defer log.Debugf("[%s/%s/ensure] %s @ %d : finished", rm.Manager, r.Name, etcdkey.EtcdKey, etcdkey.ModRevision)
	r.updateTaskChannel(ReconcilerOp_Ensure)

	var m *TaskMessage
	periodicMessageWarnfDuration(
		func() { m = actions.Ensure(prev, etcdkey.EtcdValue, etcdkey.Version, td) },
		r.PeriodicMessageFrequency,
		"[%s/%s/ensure] %s @ %d : has been processing for %s...", rm.Manager, r.Name, etcdkey.EtcdKey, etcdkey.ModRevision,
	)

	m.logByReconciler(rm.LogLevel, rm.DisableMessageLogging, rm.Manager, r.Name, "ensure", etcdkey.EtcdKey)

	if m == nil {
		ts.LastStatus = TaskStatus_Success
		ts.Messages = []*TaskMessage{TaskMessageInfo("task ensured")}
	} else {

		result := levelToStatusHandlePut[m.Level]

		// If the result is undefined, assume that the current status in the reconciler what we want
		if result == TaskStatus_Undefined {
			ts = td.Status
		} else {
			ts.LastStatus = result
			ts.Messages = []*TaskMessage{m}
		}
	}

	ts.PrevValue = etcdkey.EtcdValue

	r.registerTaskKey(etcdkey.EtcdKey, false, ts, etcdkey)

	ops := new(storage.EtcdTransaction)

	if ts.updateStatusOkOnEnsure(m, orig_status) {

		// we clone the status so that when it gets around to being written,
		// we can avoid data races between the status being written to etcd
		// and whatever we have in the cache
		write_ts := ts.Clone()
		err := write_ts.AppendCreateOps(rm.EtcdCli, ops)
		if err != nil {
			log.Errorf("CreateOpsWithLeaseID error: %+v", err)
			return nil, err
		}
	}

	return ops, nil

}

func (rm *ReconcilerManager) mergeChans() {

	rm.merged = make(chan reconcilerHandlerEvent, len(rm.watch_chans)+len(rm.periodic_chans))

	// watch the stop channel
	go func() {
		<-rm.stopchannel
		rm.merged <- reconcilerHandlerEvent{
			eventType: ReconcilerManagerEventStop,
		}
	}()

	var wg sync.WaitGroup
	wg.Add(len(rm.watch_chans) + len(rm.periodic_chans))

	for i, t := range rm.watch_chans {
		go func(i int, t watchTuple) {
			defer wg.Done()

			for v := range t.watch {
				v := v
				rm.merged <- reconcilerHandlerEvent{
					eventType:         ReconcilerManagerEventEtcdWatch,
					reconcilerOrigin:  t.reconciler,
					etcdWatchResponse: &v,
				}
			}
		}(i, t)
	}

	for i, te := range rm.periodic_chans {
		go func(i int, te *reconcilerTicker) {
			defer wg.Done()

			for cont := true; cont; {
				_, cont = <-te.ticker.TickChannel()
				if cont {
					rm.merged <- reconcilerHandlerEvent{
						eventType:        te.eventType,
						reconcilerOrigin: te.reconcilerToTick,
						reconcilerTicker: te,
					}
				}
			}
		}(i, te)
	}

	go func() {
		wg.Wait()
		close(rm.merged)
	}()
}

func (rm *ReconcilerManager) initFindKey(r *Reconciler, ts *TaskStatus, kv *mvccpb.KeyValue) {

	skipped := true

	defer func() {
		if skipped {
			rm.io_remaining.Add(-1)
		}
	}()

	k := string(kv.Key)

	var err error
	actions, err := r.cloneActions()
	if err != nil {
		rm.stopWithError(fmt.Errorf("[%s/%s]: %+v", rm.Manager, r.Name, err))
		return
	}

	if actions.Parse(k) {
		tk, _ := r.handledRevisions.Load(k)

		if tk.revision > kv.ModRevision {
			log.Infof("[%s/%s/init] %s : skipping put, already handled a higher revision (%d > %d)", rm.Manager, r.Name, k, tk.revision, kv.ModRevision)
			return
		}

		skipped = false

		log.Infof("[%s/%s/init] %s @ %d : handling", rm.Manager, r.Name, k, kv.ModRevision)
		defer log.Infof("[%s/%s/init] %s @ %d : finished", rm.Manager, r.Name, k, kv.ModRevision)
		log.Debugf("[%s/%s/init] %s : last status rev: %d vs. task created/mod rev %d/%d", rm.Manager, r.Name, k, kv.ModRevision, kv.CreateRevision, ts.GetTaskRevision())

		err := rm.handlePut(storage.NewGenericEtcdKeyFromKV(kv), r, ts, actions)
		if err != nil {
			log.WithFields(log.Fields{"err": err}).Error("init put key")

			rm.joinError(err)
		}

	}

}

func (rm *ReconcilerManager) initDelete(r *Reconciler, ts *TaskStatus, revision int64) {
	skipped := true

	defer func() {
		if skipped {
			rm.io_remaining.Add(-1)
		}
	}()

	actions, err := r.cloneActions()
	if err != nil {
		rm.stopWithError(fmt.Errorf("[%s/%s]: %+v", rm.Manager, r.Name, err))
		return
	}

	if ts.LastStatus != TaskStatus_Deleted && actions.Parse(ts.TaskKey) {

		// check if we already handled it
		tk, _ := r.handledRevisions.Load(ts.TaskKey)

		if tk.revision > revision {
			log.Infof("[%s/%s/init] %s : skipping delete, already handled a higher revision (%d > %d)", rm.Manager, r.Name, ts.TaskKey, tk.revision, revision)
			return
		}

		skipped = false

		log.Infof("[%s/%s/init] %s : handling delete", rm.Manager, r.Name, ts.TaskKey)
		defer log.Infof("[%s/%s/init] %s : finished delete", rm.Manager, r.Name, ts.TaskKey)
		// since we don't have the key, we also don't have a mod revision for it,
		// so just put down the current header revision
		_, err := rm.handleDelete(ts.TaskKey, revision, r, ts, actions)
		if err != nil {
			log.WithFields(log.Fields{"err": err}).Error("init delete key")

			rm.joinError(err)
		}
	}
}

func (rm *ReconcilerManager) initFromState(rec_index int, revision int64) error {
	r := rm.Reconcilers[rec_index]

	var once sync.Once

	r.incrementRunningTasks()
	defer once.Do(func() {
		r.decrementRunningTasks()
	})

	upgradeOldStatuses(rm, r)

	actions, err := r.cloneActions()
	if err != nil {
		return fmt.Errorf("[%s/%s]: %+v", rm.Manager, r.Name, err)
	}

	log.Infof("[%s/%s] @ %d : initializing task from existing state", rm.Manager, r.Name, revision)

	// Go through the /tasks/status/:prefix/:manager/:name prefix space
	// and find all of the currently existing keys
	pfx := storage.SmartJoin(
		statusRoot,
		r.Prefix,
	) + "/"
	ctx, cancel := context.WithTimeout(context.Background(), contextTimeout)
	defer cancel()

	status_resp, err := rm.EtcdCli.Get(
		ctx,
		pfx,
		clientv3.WithPrefix(),
		clientv3.WithRev(revision),
	)

	if err != nil {
		err := fmt.Errorf("initFromState: %v", err)
		log.Error(err)
		return err
	}

	statuses := make(map[string]*TaskStatus)

	for _, kv := range status_resp.Kvs {
		k := string(kv.Key)

		ts := new(TaskStatus)

		err := proto.Unmarshal(kv.Value, ts)
		if err != nil {
			log.WithFields(log.Fields{"err": err}).Errorf("%s failed to parse as a task status", k)
			continue
		}
		// check that the key we're reading:
		//  - has the same task manager and name
		//  - is actually a key that we currently handle (it's possible that a reconciler changes the keys that it reconciles)
		if ts.CheckManagerAndName(rm.Manager, r.Name) && actions.Parse(ts.TaskKey) {
			statuses[ts.TaskKey] = ts
			r.registerTaskKey(ts.TaskKey, false, ts, ts.GetPrevAsGenericEtcdKey())
		}
	}

	ctx2, cancel2 := context.WithTimeout(context.Background(), contextTimeout)
	defer cancel2()

	task_resp, err := rm.EtcdCli.Get(
		ctx2,
		r.Prefix,
		clientv3.WithPrefix(),
		clientv3.WithRev(revision),
	)

	if err != nil {
		err := fmt.Errorf("initFromState: %v", err)
		log.Error(err)
		return err
	}

	r.updateTaskChannel(ReconcilerOp_Init)

	// process the currently existing keys and record them into existing
	existing := sets.NewSet[string]()

	for i := range task_resp.Kvs {
		i := i
		kv := task_resp.Kvs[i]
		k := string(kv.Key)

		existing.Add(k)
		ts := statuses[k]

		rm.io_remaining.Add(1)
		rm.submitTaskWithKey(k, LowPriority, ts.GetTaskRevision(), rec_index, i, func() {
			rm.initFindKey(r, ts, kv)
		})

	}

	// Check for deletes that occurred while the task manager was offline.
	// This is the set of status keys minus the set of existing keys

	for i, kv := range status_resp.Kvs {
		i := i
		ts := new(TaskStatus)

		err := proto.Unmarshal(kv.Value, ts)
		if err != nil {
			log.WithFields(log.Fields{"err": err}).Error("parse status")
			continue
		}

		// for deleted keys:
		// check that the status we're reading:
		//  - has the same task manager and name
		//  - their task key doesn't exist
		if ts.CheckManagerAndName(rm.Manager, r.Name) && !existing.Contains(ts.TaskKey) {
			existing.Add(ts.TaskKey)

			rm.io_remaining.Add(1)
			rm.submitTaskWithKey(ts.TaskKey, LowPriority, ts.GetTaskRevision(), rec_index, i, func() {
				rm.initDelete(r, ts, status_resp.Header.GetRevision())
			})
		}
	}

	log.Infof("[%s/%s/init] @ %d : queued all tasks", rm.Manager, r.Name, revision)
	go func() {
		r.poolWg.Wait()

		log.Infof("[%s/%s/init] @ %d : complete", rm.Manager, r.Name, revision)
	}()

	return nil
}
