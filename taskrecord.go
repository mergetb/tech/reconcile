package reconcile

func (tr *TaskRecord) Equals(o *TaskRecord) bool {
	// both are nil, they're equal
	if tr == nil && o == nil {
		return true
	}

	// one is nil, they're not equal
	if tr == nil || o == nil {
		return false
	}

	return tr.Task == o.Task &&
		tr.ReconcilerManager == o.ReconcilerManager &&
		tr.ReconcilerName == o.ReconcilerName &&
		tr.Existence == o.Existence &&
		tr.KeyType == o.KeyType
}
