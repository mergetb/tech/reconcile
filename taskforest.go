package reconcile

import (
	"context"
	"fmt"
	"sort"
	"strings"
	"time"

	sets "github.com/deckarep/golang-set/v2"
	"github.com/maruel/natural"
	log "github.com/sirupsen/logrus"
	"go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/types/known/timestamppb"

	storage "gitlab.com/mergetb/tech/shared/storage/etcd"
)

func (tf *TaskForest) GetParentName(parentpath ...string) string {
	pp := append(parentpath, tf.Goal.GetName())

	return JoinParentPath(pp...)
}

func (tf *TaskForest) ToTaskSummary() *TaskSummary {
	if tf == nil {
		return nil
	}

	tses := tf.GetStatuses()

	var messages []*TaskMessage

	// just returns messages as is if there's only 1 possible message
	if tf.GetNumChildTasks() <= 1 && len(tses) > 0 {
		messages = tses[0].GetMessages()
	} else {

		for _, ts := range tses {
			// ignore non errored statuses
			if ts.GetCurrentStatus() < TaskStatus_Pending {
				continue
			}

			// append the reconciler name to the message
			for _, m := range ts.GetMessages() {
				message := &TaskMessage{
					Level: m.GetLevel(),
					Message: fmt.Sprintf(
						"%s/%s: %s",
						ts.GetReconcilerManager(),
						ts.GetReconcilerName(),
						m.GetMessage()),
				}

				messages = append(messages, message)
			}

		}
	}

	return &TaskSummary{
		HighestStatus: tf.GetHighestStatus(),
		FirstUpdated:  tf.GetGoal().GetCreation(),
		LastUpdated:   tf.GetLastUpdated(),
		Messages:      messages,
	}
}

func (tf *TaskForest) GetStatuses() []*TaskStatus {
	return tf.getStatusesHelper(nil)
}

func (tf *TaskForest) getStatusesHelper(statuses []*TaskStatus) []*TaskStatus {
	for _, tt := range tf.GetSubtasks() {
		statuses = tt.getStatuses(statuses)
	}

	for _, o := range tf.GetSubgoals() {
		statuses = o.getStatusesHelper(statuses)
	}

	return statuses
}

//
// Given a top-level task, Find the task instances for the task.
// The Key given is not a task key, but the key itself.
// e.g. /users/glawler and not /tasks/entries/user-glawler. The
// task instances for this key are in /tasks/instances/<key>.
//

// List all existing tasks as a list of trees.
// Read all tasks under /tasks.
func GetGoals(etcdCli *clientv3.Client, cache *storage.EtcdCache) ([]*TaskForest, error) {

	if etcdCli == nil {
		e := fmt.Errorf("GetTasks: etcd is nil!")
		log.Error(e)
		return nil, e
	}

	ctx, cancel := context.WithTimeout(context.Background(), contextTimeout)
	defer cancel()

	// dump all task instances.
	resp, err := etcdCli.Get(ctx, goalsRoot, clientv3.WithPrefix())
	if err != nil {
		return nil, err
	}

	tts := []*TaskForest{}

	for _, k := range resp.Kvs {
		g := NewGenericGoal(string(k.Key))

		err := g.Read(etcdCli)
		if err != nil {
			return nil, err
		}

		tt, err := g.GetTaskForest(etcdCli, cache)
		if err != nil {
			return nil, err
		}

		tts = append(tts, tt)
	}

	return tts, nil
}

func (tg *TaskGoal) WatchTaskForest(etcdCli *clientv3.Client, timeout time.Duration) (*TaskForest, error) {
	if timeout <= 0 {
		return tg.GetTaskForest(etcdCli, nil)
	}

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	cache := storage.NewReadCache(0, nil)
	defer cache.Close()

	return tg.WatchTaskForestWithOptions(ctx, etcdCli, cache)
}

func (tg *TaskGoal) WatchTaskForestWithOptions(ctx context.Context, etcdCli *clientv3.Client, cache *storage.EtcdCache) (*TaskForest, error) {
	var rev int64
	var err error

	if cache == nil {
		rev, err = storage.GetCurrentRevision(etcdCli)
		if err != nil {
			return nil, err
		}
	}

	tf, err := tg.GetTaskForest(etcdCli, cache)
	if err != nil {
		return nil, err
	}

	if tf.HighestStatus == TaskStatus_Success {
		return tf, nil
	}

	if cache != nil {
		rev = cache.GetRevision()
	}

	var ops storage.EtcdTransaction
	tf.AppendTaskRecordOps(etcdCli, &ops)

	// have our own context, so we can clean up the etcd watch when we're done
	ctx2, cancel := context.WithCancel(context.Background())
	defer cancel()

	ch, err := ops.EtcdWatch(etcdCli, storage.EtcdTransactionOptions{
		Ctx:           ctx2,
		Cache:         cache,
		StartRevision: rev,
	})

	if err != nil {
		return nil, err
	}

	tf, err = func() (*TaskForest, error) {
		for {
			select {
			case <-ctx.Done():
				return nil, nil
			case _, more := <-ch:
				if !more {
					return nil, nil
				}

				tf, err = tg.GetTaskForest(etcdCli, cache)
				if err != nil {
					return nil, err
				}

				if tf.HighestStatus == TaskStatus_Success {
					return tf, nil
				}
			}
		}
	}()

	if err != nil {
		return nil, err
	}

	if tf != nil {
		return tf, nil
	}

	return tg.GetTaskForest(etcdCli, cache)
}

func (tg *TaskGoal) GetTaskForest(etcdCli *clientv3.Client, cache *storage.EtcdCache) (*TaskForest, error) {
	return tg.getTaskForestHelper(etcdCli, sets.NewSet[string](), cache)
}

func (tg *TaskGoal) getTaskForestHelper(etcdCli *clientv3.Client, visited sets.Set[string], cache *storage.EtcdCache) (*TaskForest, error) {

	if etcdCli == nil {
		e := fmt.Errorf("ReadTaskForest: etcd is nil!")
		log.Error(e)
		return nil, e
	}

	visited.Add(tg.SelfKey)

	var higheststatus TaskStatus_StatusType = 0
	var lastupdated time.Time
	num_childtasks := int64(0)

	read_goals, read_statuses, err := tg.ReadAllSubkeys(etcdCli, cache)
	if err != nil {
		return nil, err
	}

	// process subgoals
	tfs := []*TaskForest{}
	for _, sg := range read_goals {
		log.Debugf("read subgoals %s -> %s", tg.Key(), sg.SelfKey)

		if visited.Contains(sg.SelfKey) {
			e := fmt.Errorf("goal %s already visited, recursion detected!", sg.SelfKey)
			log.Error(e)
			return nil, e
		}

		visited.Add(sg.SelfKey)

		tf, err := sg.getTaskForestHelper(etcdCli, visited, cache)
		if err != nil {
			return nil, err
		}

		// don't add it if it's empty and without error
		//  - this is so the resulting structure is cleaner
		if tf.NumChildTasks > 0 || strings.Contains(strings.ToLower(tf.GetGoal().GetDesc()), "error") {
			tfs = append(tfs, tf)

			if tf.HighestStatus > higheststatus {
				higheststatus = tf.HighestStatus
			}

			if lastupdated.Before(tf.LastUpdated.AsTime()) {
				lastupdated = tf.LastUpdated.AsTime()
			}

			// if tg.Creation is nil, or if it's a goal not written in etcd and the time is lower,
			// copy over the time
			if (tg.Creation == nil) ||
				(tg.SelfVersion == 0 && tf.Goal.Creation != nil && tg.Creation.AsTime().After(tf.Goal.Creation.AsTime())) {
				tg.Creation = tf.Goal.Creation
			}

			num_childtasks += tf.NumChildTasks
		}
	}

	// process subtasks
	tts := []*TaskTree{}

	// sort the read_statuses
	sort.Slice(read_statuses,
		func(i, j int) bool {
			return natural.Less(
				read_statuses[i].GetFullName(),
				read_statuses[j].GetFullName(),
			)
		},
	)

	for _, ts := range read_statuses {
		log.Debugf("read subtasks %s -> %s", tg.SelfKey, ts.TaskKey)

		tt, err := ts.GetTaskTree(etcdCli, cache)
		if err != nil {
			return nil, err
		}

		tts = append(tts, tt)

		if tt.HighestStatus > higheststatus {
			higheststatus = tt.HighestStatus
		}

		if lastupdated.Before(tt.LastUpdated.AsTime()) {
			lastupdated = tt.LastUpdated.AsTime()
		}

		// if tg.Creation is nil, or if it's a goal not written in etcd and the time is lower,
		// copy over the time
		if (tg.Creation == nil) ||
			(ts.Creation != nil && tg.SelfVersion == 0 && tg.Creation.AsTime().After(ts.Creation.AsTime())) {
			tg.Creation = ts.Creation
		}

		if (tg.Creation == nil) ||
			(ts.When != nil && tg.SelfVersion == 0 && tg.Creation.AsTime().After(ts.When.AsTime())) {
			tg.Creation = ts.When
		}

		num_childtasks += 1 + tt.NumChildTasks
	}

	// if lastupdated hasn't been updated
	// then use the goal time
	if lastupdated == (time.Time{}) {
		lastupdated = tg.When.AsTime()
	}

	// if we didn't write any subgoals/statuses,
	// it is considered a success
	if higheststatus == TaskStatus_Undefined {
		higheststatus = TaskStatus_Success
	}

	tf := &TaskForest{
		Goal:          tg,
		Subgoals:      tfs,
		Subtasks:      tts,
		HighestStatus: higheststatus,
		LastUpdated:   timestamppb.New(lastupdated),
		NumChildTasks: num_childtasks,
	}

	tf.Organize()

	return tf, nil
}

// This groups subtasks with the same reconciler together
// This changes the TaskForest to a Supergoal to do so
func (tf *TaskForest) Organize() {

	if len(tf.Subtasks) == 0 {
		return
	}

	subtasks_groups := make(map[string]*TaskForest)
	var subtasks_order []string

	for _, st := range tf.Subtasks {
		path := st.Task.GetReconcilerPath()

		if path == "" {
			path = "Unknown"
		}

		if _, found := subtasks_groups[path]; !found {
			subtasks_groups[path] = &TaskForest{
				Goal: &TaskGoal{
					Name: path,
					Desc: st.Task.Desc,
					When: tf.Goal.When,
					Type: TaskGoal_Supertask,
				},
			}

			// add the creation date if it's a goal stored in etcd
			if tf.Goal.SelfVersion != 0 {
				subtasks_groups[path].Goal.Creation = tf.Goal.Creation
			}

			// always put the pending keys first
			if path == "Unknown" {
				subtasks_order = append([]string{path}, subtasks_order...)
			} else {
				subtasks_order = append(subtasks_order, path)
			}

		}

		subtasks_groups[path].Subtasks = append(subtasks_groups[path].Subtasks, st)

		if st.HighestStatus > subtasks_groups[path].HighestStatus {
			subtasks_groups[path].HighestStatus = st.HighestStatus
		}

		if subtasks_groups[path].Goal.Creation == nil ||
			(st.Task.Creation != nil && subtasks_groups[path].Goal.Creation.AsTime().After(st.Task.Creation.AsTime())) {
			subtasks_groups[path].Goal.Creation = st.Task.Creation
		}

		if subtasks_groups[path].LastUpdated == nil ||
			(st.LastUpdated != nil && subtasks_groups[path].LastUpdated.AsTime().Before(st.LastUpdated.AsTime())) {
			subtasks_groups[path].LastUpdated = st.LastUpdated
		}

		subtasks_groups[path].NumChildTasks += st.NumChildTasks
	}

	// Change the type to a supergoal
	tf.Goal.Type = TaskGoal_Supergoal

	tf.Subtasks = nil
	for _, p := range subtasks_order {
		tf.Subgoals = append(tf.Subgoals, subtasks_groups[p])
	}

}

// the intent of this is to merge together different task forests,
// where one is a prereq to the other, but you don't want to use
// the times of the other one
func (tf *TaskForest) PushFront(updateCreation bool, others ...*TaskForest) {
	for i := range others {
		// iterate backwards, so that they're added in order to the front
		o := others[len(others)-1-i]

		// don't add it if it's empty and without error
		//  - this is so the resulting structure is cleaner
		if o.NumChildTasks > 0 {
			res := make([]*TaskForest, len(tf.Subgoals)+1)
			res[0] = o
			copy(res[1:], tf.Subgoals)
			tf.Subgoals = res

			if o.HighestStatus > tf.HighestStatus {
				tf.HighestStatus = o.HighestStatus
			}

			if o.LastUpdated.AsTime().After(tf.LastUpdated.AsTime()) {
				tf.LastUpdated = o.LastUpdated
			}

			if updateCreation &&
				(tf.Goal.Creation == nil ||
					(tf.Goal.SelfVersion == 0 && tf.Goal.Creation.AsTime().After(o.Goal.Creation.AsTime()))) {
				tf.Goal.Creation = o.Goal.Creation
			}

			tf.NumChildTasks += o.NumChildTasks
		}
	}
}

func (tf *TaskForest) AppendTaskRecordOps(etcdCli *clientv3.Client, ops *storage.EtcdTransaction) {
	tf.GetGoal().AppendTaskRecordOps(etcdCli, ops)

	for _, t := range tf.GetSubgoals() {
		t.AppendTaskRecordOps(etcdCli, ops)
	}
}
