package reconcile

import (
	"context"
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"go.etcd.io/etcd/api/v3/mvccpb"
	"go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"
)

type heartbeatMonitor struct {
	heartbeat    chan uint64
	flatline     chan bool
	grace_period time.Duration
	timer        *time.Timer
	ctx          context.Context
}

func newHeartbeatMonitor(ctx context.Context, grace_period time.Duration) *heartbeatMonitor {
	return &heartbeatMonitor{
		heartbeat:    make(chan uint64, 1),
		flatline:     make(chan bool, 1),
		grace_period: grace_period,
		timer:        nil,
		ctx:          ctx,
	}
}

func (cm *heartbeatMonitor) handleHeartbeat(value []byte, version int64) error {
	hb := new(ServerHeartbeat)
	err := proto.Unmarshal(value, hb)
	if err != nil {
		return fmt.Errorf("could not unmarshal ServerHeartbeat from etcd: %+v", err)
	}

	log.WithFields(log.Fields{
		"version":      version,
		"timestamp":    hb.When.AsTime().Format(time.UnixDate),
		"interval":     hb.Interval,
		"grace_period": cm.grace_period,
	}).Trace("[heartbeat-monitor] heartbeat received")

	cm.heartbeat <- hb.Interval
	return nil
}

func (cm *heartbeatMonitor) handleHeartbeatDelete() error {
	log.Warn("[heartbeat-monitor] heartbeat deleted")
	cm.flatline <- true
	return nil
}

func (cm *heartbeatMonitor) watcher(cli *clientv3.Client) error {
	// if the key is already present, send out an initialization heartbeat
	ctx, cancel := context.WithTimeout(context.Background(), contextTimeout)
	resp, err := cli.KV.Get(ctx, ServerHeartbeatPath())
	cancel()

	if err != nil {
		log.Errorf("[heartbeat-monitor] server failed to read: %+v", err)
	} else if len(resp.Kvs) != 0 {
		kv := resp.Kvs[0]

		err := cm.handleHeartbeat(kv.Value, kv.Version)

		if err != nil {
			return fmt.Errorf("error processing heartbeat key: %+v", err)
		}
	}

	watch_chan := cli.Watch(context.Background(), ServerHeartbeatPath())

	for {
		select {
		case <-cm.ctx.Done():
			log.Infof("heartbeat cancellation request")
			return cm.ctx.Err()
		case resp, ok := <-watch_chan:

			if !ok || resp.Canceled || resp.Err() != nil {
				err := cm.handleHeartbeatDelete()

				if err != nil {
					return fmt.Errorf("error processing heartbeat key: %+v", err)
				}
			} else {
				for _, event := range resp.Events {

					var err error
					switch event.Type {
					case mvccpb.PUT:
						err = cm.handleHeartbeat(event.Kv.Value, event.Kv.Version)

					case mvccpb.DELETE:
						err = cm.handleHeartbeatDelete()
					}

					if err != nil {
						return fmt.Errorf("error processing heartbeat key: %+v", err)
					}
				}
			}
		}

	}

}

func (cm *heartbeatMonitor) run(cli *clientv3.Client) error {
	var duration time.Duration

	watcher_done := make(chan error, 1)

	// thread to watch for etcd heartbeats
	go func() {
		err := cm.watcher(cli)

		select {
		case <-cm.ctx.Done():
		case watcher_done <- err:
		default:
		}

		close(watcher_done)
	}()

	log.Info("[heartbeat-monitor] awaiting first heartbeat before establishing timer")
	// wait for first heartbeat before establishing timer
	select {
	case <-cm.ctx.Done():
		return cm.ctx.Err()
	case d := <-cm.heartbeat:
		duration = time.Duration(d) * time.Second
	}

	log.Infof("[heartbeat-monitor] detected first heartbeat; starting timer: interval: %s, grace_period: %s", duration, cm.grace_period)
	cm.timer = time.NewTimer(duration + cm.grace_period)

	// main processing loop to manage timer
	for {
		select {
		case <-cm.timer.C:
			// timer fired; we're dead
			return fmt.Errorf("[heartbeat-monitor] server flatline detected. %s expired without server heartbeat", duration+cm.grace_period)

		case <-cm.flatline:
			// heartbeat deleted; we're dead
			return fmt.Errorf("[heartbeat-monitor] server flatline detected. Heartbeat deleted")

		case d := <-cm.heartbeat:
			duration = time.Duration(d) * time.Second
			// received a heartbeat; restart the timer

			// stop, drain, reset is the proper restart cadence
			// https://pkg.go.dev/time#Timer.Reset
			if !cm.timer.Stop() {
				<-cm.timer.C
			}
			cm.timer.Reset(duration + cm.grace_period)

		case <-cm.ctx.Done():
			return cm.ctx.Err()
		case err := <-watcher_done:
			return err
		}
	}
}
