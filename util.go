package reconcile

import (
	"fmt"
	"path"
	"sort"
	"strings"
	"time"

	sets "github.com/deckarep/golang-set/v2"
	"github.com/maruel/natural"
	log "github.com/sirupsen/logrus"
	storage "gitlab.com/mergetb/tech/shared/storage/etcd"
	"go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var (
	tasksRoot           string = "/.tasks"
	goalsPath           string = "goals"
	statusPath          string = "status"
	alivePath           string = ".alive"
	healthPath          string = ".health"
	serverHeartbeatPath string = "/.heartbeat"
	goalsRoot           string = path.Join(tasksRoot, goalsPath)
	statusRoot          string = path.Join(tasksRoot, statusPath)
	aliveRoot           string = path.Join(tasksRoot, alivePath)
	healthRoot          string = path.Join(tasksRoot, healthPath)
	unmonitoredRoot     string = "/.unmonitored"
	// The actual reconcile health expiration time is HealthFrequency*healthFrequencyBuffer
	defaultHealthFrequency          time.Duration    = 5 * time.Minute
	healthFrequencyBuffer           float64          = 1.2
	defaultPollCheckFrequency       time.Duration    = 15 * time.Minute
	defaultHeartbeatGracePeriod     time.Duration    = 30 * time.Second
	minimumHeartbeatGracePeriod     time.Duration    = 2 * time.Second
	defaultPeriodicMessageFrequency time.Duration    = 1 * time.Minute
	defaultLogLevel                 TaskMessage_Type = TaskMessage_Warning
	taskQueueNotReadySleepInterval  time.Duration    = 100 * time.Millisecond
	taskQueueNotReadyWarnDuration   time.Duration    = 5 * time.Minute
	stopTimeout                     time.Duration    = 5 * time.Minute
	contextTimeout                  time.Duration    = 10 * time.Second
	pollcheckWaitForProgressNotify  time.Duration    = 3 * time.Second
	contextTimeoutBuffer            float64          = 1.2
	maintenanceTickerMultipler      float64          = 20 // this is so that the ensure period is at least 20*(ensure latency)
	ioTickerInterval                time.Duration    = 200 * time.Millisecond
	ioTickerMultipler               float64          = 10 // this is so that the io buffer wait period is equal to 10*(io latency)
)

const (
	HighPriority       = uint64(0 << 60)
	NormalPriority     = uint64(1 << 60)
	LowPriority        = uint64(2 << 60)
	BackgroundPriority = uint64(3 << 60)
)

func IsMonitored(key string) bool {
	return strings.HasPrefix(key, unmonitoredRoot)
}

func JoinUnmonitoredRoot(keys ...string) string {
	return storage.SmartJoin(unmonitoredRoot, keys...)
}

func JoinStatusRoot(keys ...string) string {
	return storage.SmartJoin(statusRoot, keys...)
}

func JoinGoalsRoot(keys ...string) string {
	return storage.SmartJoin(goalsRoot, keys...)
}

func JoinAliveRoot(keys ...string) string {
	return storage.SmartJoin(aliveRoot, keys...)
}

func JoinHealthRoot(keys ...string) string {
	return storage.SmartJoin(healthRoot, keys...)
}

func JoinParentPath(keys ...string) string {
	return strings.Join(keys, "|")
}

func ServerHeartbeatPath() string {
	return serverHeartbeatPath
}

func TESTINGSetSleepIntervalToZero() {
	taskQueueNotReadySleepInterval = 0
}

func periodicMessageWarnfDuration(run func(), frequency time.Duration, format string, a ...any) bool {
	c := make(chan struct{}, 1)
	defer close(c)

	go func() {
		run()
		c <- struct{}{}
	}()

	duration := time.Duration(0)
	a = append(a, duration.String())

	if frequency > 0 {
		for cont := true; cont; {
			select {
			case <-c:
				cont = false
			case <-time.After(frequency):
				duration += frequency
				a[len(a)-1] = duration.String()
				log.Warnf(format, a...)
			}
		}
	} else {
		start := time.Now()
		<-c
		duration = time.Since(start)
	}

	return duration != 0
}

func timeoutFunctionDefaultDuration(run func()) bool {
	return timeoutFunction(run, time.Duration(float64(contextTimeout)*contextTimeoutBuffer))
}

func timeoutFunction(run func(), duration time.Duration) bool {
	c := make(chan struct{}, 1)
	defer close(c)

	go func() {
		run()
		c <- struct{}{}
	}()

	select {
	case <-c:
		return false
	case <-time.After(duration):
		return true
	}
}

// This allows you to force all reconcilers watching the given keys to
// immediately re-handle the given key.
//
// If pendingStatus is true, the task status is set to pending.
//
// When passing a deleteKey of true, the key is deleted before it is re-written.
// Otherwise, the version of the key is just bumped.
func ForceRehandleKey(etcdCli *clientv3.Client, withPrefix, deleteKey, pendingStatus bool, wait time.Duration, key ...string) error {

	if len(key) == 0 {
		return fmt.Errorf("no etcd keys, doing nothing")
	}

	// read all keys
	ops := new(storage.EtcdTransaction)

	for _, k := range key {
		g := storage.NewGenericEtcdKey(k, nil)

		if withPrefix {
			if !strings.HasSuffix(k, "/") {
				return fmt.Errorf("key %s must end with '/' when using withPrefix", k)
			}

			ops.ReadWithOpts(g, clientv3.WithPrefix())
		} else {
			ops.Read(g)
		}

		if pendingStatus {
			ts := NewTaskStatusUnknown(k)

			// Since ts.AppendReadOps reads the task key, we don't need to add it again
			err := ts.AppendReadOps(etcdCli, ops)
			if err != nil {
				return err
			}
		}

	}

	txr, _, err := ops.EtcdTx(etcdCli, "")
	if err != nil {
		return fmt.Errorf("could not read keys from etcd: %+v", err)
	}

	generic_map, err := (new(storage.GenericEtcdKey)).ReadAllFromResponse(txr)
	if err != nil {
		return fmt.Errorf("could not parse generic keys: %+v", err)
	}

	// delete all of the status keys and health keys
	// from the generic key map
	var toDelete []string
	for k := range generic_map {
		if strings.HasPrefix(k, statusRoot) {
			toDelete = append(toDelete, k)
		}
	}

	for _, k := range toDelete {
		delete(generic_map, k)
	}

	if len(generic_map) == 0 {
		return fmt.Errorf("no etcd keys matched, doing nothing")
	}

	// if deleteKey,
	//   - we will delete the key and re-write them
	// else,
	//   - just bump the version of the key

	ops = new(storage.EtcdTransaction)

	sorted_taskkeys := make([]string, 0, len(generic_map))
	for k := range generic_map {
		// status related keys are not task keys
		if !strings.HasPrefix(k, tasksRoot) {
			sorted_taskkeys = append(sorted_taskkeys, k)
		}

	}
	sort.Sort(natural.StringSlice(sorted_taskkeys))

	for _, k := range sorted_taskkeys {
		obj := generic_map[k]

		g, ok := obj.(*storage.GenericEtcdKey)

		if !ok {
			return fmt.Errorf("invalid type %s: %T", g.Key(), obj)
		}

		if deleteKey {
			log.Infof("deleting and rewriting taskkey: %s", g.Key())
			ops.Delete(g)
		} else {
			log.Infof("bumping version of taskkey: %s", g.Key())
			ops.Write(g)
		}
	}

	if pendingStatus {
		// set all of the status keys to pending

		status_map, err := (new(TaskStatus)).ReadAllFromResponse(txr)
		if err != nil {
			return fmt.Errorf("could not parse status keys: %+v", err)
		}

		status_keys := make([]string, 0, len(status_map))
		for k := range status_map {
			status_keys = append(status_keys, k)
		}
		sort.Sort(natural.StringSlice(status_keys))

		for _, k := range status_keys {
			obj := status_map[k]

			ts, ok := obj.(*TaskStatus)

			if !ok {
				return fmt.Errorf("invalid type %s: %T", obj.Key(), obj)
			}

			// make sure that it matches a taskkey that we found
			found := false
			for _, k := range key {
				if ts.TaskKey == k {
					found = true
					break
				}

				if withPrefix && strings.HasPrefix(ts.TaskKey, k) {
					found = true
					break
				}
			}

			if found {
				log.Infof("setting status to pending: %s", obj.Key())

				ts.LastStatus = TaskStatus_Pending

				ops.Write(ts)
			}
		}
	}

	_, rb, err := ops.EtcdTx(etcdCli, "")
	if err != nil {
		return fmt.Errorf("could not delete statuses: %+v", err)
	}

	if deleteKey {
		if wait > 0 {
			log.Infof("waiting for %s before deleting", wait)
			time.Sleep(wait)
		}

		ops := new(storage.EtcdTransaction)

		for _, k := range sorted_taskkeys {
			obj := generic_map[k]
			g, ok := obj.(*storage.GenericEtcdKey)

			if !ok {
				// rollback changes
				rb.Rollback()
				return fmt.Errorf("invalid type, rolling back: %T", obj)
			}

			// since they are deleted, the version should be 0
			g.SetVersion(0)

			ops.Write(g)
		}

		_, _, err = ops.EtcdTx(etcdCli, "")
		if err != nil {
			// rollback changes
			rb.Rollback()
			return fmt.Errorf("could not rewrite keys, rolling back: %+v", err)
		}
	}

	return nil
}

// This allows you to set the laststatus of a status key to be success
// If force is also set, then it also updates the last value,
func ForceSuccessKey(etcdCli *clientv3.Client, withPrefix, force bool, key ...string) error {

	// read all keys
	ops := new(storage.EtcdTransaction)

	for _, k := range key {
		// check if prefix key is ok
		if withPrefix && !strings.HasSuffix(k, "/") {
			return fmt.Errorf("key %s must end with '/' when using withPrefix", k)
		}

		// only read the taskkeys if we want to point the new status key to the current task key
		if force {
			g := storage.NewGenericEtcdKey(k, nil)

			if withPrefix {
				ops.ReadWithOpts(g, clientv3.WithPrefix())
			} else {
				ops.Read(g)
			}
		}

		ts := NewTaskStatusUnknown(k)

		// Since ts.AppendReadOps reads the task key, we don't need to add it again
		err := ts.AppendReadOps(etcdCli, ops)
		if err != nil {
			return err
		}

	}

	txr, _, err := ops.EtcdTx(etcdCli, "")
	if err != nil {
		return fmt.Errorf("could not read keys from etcd: %+v", err)
	}

	status_map, generic_map, _, err := ReadAllStatusInfoFromResponse(txr)
	if err != nil {
		return fmt.Errorf("could not read all status info from response: %+v", err)
	}

	ops = new(storage.EtcdTransaction)

	status_keys := make([]string, 0, len(status_map))
	for k := range status_map {
		status_keys = append(status_keys, k)
	}
	sort.Sort(natural.StringSlice(status_keys))

	for _, k := range status_keys {
		tses := status_map[k]

		for _, ts := range tses {
			// make sure that it matches a taskkey that we asked for
			found := false
			for _, k := range key {
				if ts.TaskKey == k {
					found = true
					break
				}

				if withPrefix && strings.HasPrefix(ts.TaskKey, k) {
					found = true
					break
				}
			}

			if !found {
				continue
			}

			if ts.CurrentStatus == TaskStatus_Success {
				log.Infof("current status is already success, skipping: %s", k)
				continue
			}

			// we force the status to be a success by bumping its value and version
			if force {
				val, ok := generic_map[ts.TaskKey]
				if !ok {
					log.Infof("found a status, but did not find its taskkey, doing nothing: %s", k)
					continue
				}

				log.Infof("setting status to success and pointing the status to the current taskkey: %s", k)

				ts.LastStatus = TaskStatus_Success
				ts.PrevValue = val.EtcdValue
				ts.TaskVersion = val.Version
				ts.When = timestamppb.Now()
				ts.Messages = []*TaskMessage{TaskMessageInfo("forced success")}
			} else {
				log.Infof("setting status to success: %s", k)
				ts.LastStatus = TaskStatus_Success
			}

			ops.Write(ts)
		}

	}

	_, rb, err := ops.EtcdTx(etcdCli, "")
	if err != nil {
		// rollback changes
		rb.Rollback()
		return fmt.Errorf("could not force statuses to success, rolling back: %+v", err)
	}

	return nil
}

// Reset a raw etcd goal by removing all unread task keys from it
// delete: if true, delete all task keys from it without checking to see if task keys and statuses exist for it
func ResetGoals(etcdCli *clientv3.Client, delete bool, goals ...string) error {
	if len(goals) == 0 {
		return nil
	}

	for _, g := range goals {
		log.Infof("processing %s...", g)

		tg := NewGenericGoal(g)

		err := tg.Read(etcdCli)
		if err != nil {
			log.Fatal(err)
			return err
		}

		if tg.GetType() != TaskGoal_Supertask {
			log.Errorf("  %s is not a supertask, skipping...", g)
			continue
		}

		if delete {
			log.Infof("  setting subtasks to nil...")
			tg.Subkeys = make([]string, 0)
			_, err := tg.Update(etcdCli)

			if err != nil {
				log.Fatal(err)
				return err
			}

			continue
		}

		var ops storage.EtcdTransaction

		for _, tk := range tg.Subkeys {
			if len(strings.TrimSpace(tk)) == 0 {
				continue
			}

			ts := NewTaskStatusUnknown(tk)

			// this will append reads for:
			//   - the task key
			//   - the status key
			//   - the keepalives (unused)
			err := ts.AppendReadOps(etcdCli, &ops)
			if err != nil {
				log.Fatal(err)
				return err
			}
		}

		txn, _, err := ops.EtcdTx(etcdCli, "")
		if err != nil {
			log.Fatal(err)
			return err
		}

		// find out what we read
		exists_keys := sets.NewSet[string]()
		exists_status := sets.NewSet[string]()

		for _, r := range txn.Responses {
			rr := r.GetResponseRange()

			if rr != nil {
				for _, kv := range rr.Kvs {
					k := string(kv.Key)

					exists_keys.Add(k)

					if strings.HasPrefix(k, statusRoot) {
						exists_status.Add(k)
					}
				}
			}
		}

		var subkeys []string
		changes := false

		for _, tk := range tg.Subkeys {
			if exists_status.Contains(tk) || exists_keys.Contains(tk) {
				subkeys = append(subkeys, tk)
				log.Infof("  %s: keeping key...", tk)
			} else {
				log.Infof("  %s: missing both status and task key, removing...", tk)
				changes = true
			}
		}

		if !changes {
			log.Infof("    no changes, skipping write")
		} else {
			log.Infof("    writing %v", tg.Subkeys)
			tg.Subkeys = subkeys
			_, err = tg.Update(etcdCli)

			if err != nil {
				log.Fatal(err)
				return err
			}
		}

	}

	return nil
}
