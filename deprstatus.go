package reconcile

// This is here for backwards compatibility. In the future, we can remove this.

import (
	"context"
	"path"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	"go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"

	storage "gitlab.com/mergetb/tech/shared/storage/etcd"
)

const (
	deprStatusRoot = "/STATUS"
)

var (
	upgradeOldResult = map[DeprStatus_DeprResultType]TaskStatus_StatusType{
		DeprStatus_ResultUndefined: TaskStatus_Undefined,
		DeprStatus_Error:           TaskStatus_Error,
		DeprStatus_Success:         TaskStatus_Success,
		DeprStatus_Working:         TaskStatus_Processing,
		DeprStatus_Stale:           TaskStatus_Pending,
	}
)

func (ds *DeprStatus) Upgrade(rm *ReconcilerManager, r *Reconciler, key string, prev []byte) *TaskStatus {
	var messages []*TaskMessage

	if ds.Info != "" {
		messages = append(messages, TaskMessageInfo(ds.Info))
	}

	return &TaskStatus{
		ReconcilerManager: rm.Manager,
		ReconcilerName:    r.Name,
		Desc:              r.Desc,
		LastStatus:        upgradeOldResult[ds.Result],
		SelfVersion:       -1,
		TaskVersion:       ds.Version,
		PrevValue:         prev,
		When:              ds.When,
		TaskKey:           key,
		Messages:          messages,
	}
}

func upgradeOldStatuses(rm *ReconcilerManager, r *Reconciler) {
	ctx, cancel := context.WithTimeout(context.Background(), contextTimeout)
	defer cancel()

	// read depreciated statuses
	full_pfx := path.Join(deprStatusRoot, rm.Manager, r.Prefix) + "/"
	status_pfx := path.Join(deprStatusRoot, rm.Manager)
	old_resp, err := rm.EtcdCli.Get(ctx, full_pfx, clientv3.WithPrefix())
	if err != nil {
		log.Errorf("upgradeOldStatuses: %v", err)
		return
	}

	if len(old_resp.Kvs) == 0 {
		return
	}

	log.Infof("[%s/%s] Old statuses found; upgrading", rm.Manager, r.Name)
	log.Infof("[%s/%s] Read prefix: %s", rm.Manager, r.Name, full_pfx)

	ctx2, cancel2 := context.WithTimeout(context.Background(), contextTimeout)
	defer cancel2()

	// read current task keys
	resp, err := rm.EtcdCli.Get(ctx2, r.Prefix, clientv3.WithPrefix())
	if err != nil {
		log.Errorf("upgradeOldStatuses: %v", err)
		return
	}

	keys := make(map[string][]byte)
	for _, kv := range resp.Kvs {
		k := string(kv.Key)
		if r.Actions.Parse(k) {
			keys[k] = kv.Value
		}
	}

	ctx3, cancel3 := context.WithTimeout(context.Background(), contextTimeout)
	defer cancel3()

	// read current statuses for the keys -- we don't want to overwrite the current statuses
	resp, err = rm.EtcdCli.Get(ctx3, JoinStatusRoot(r.Prefix), clientv3.WithPrefix())
	if err != nil {
		log.Errorf("upgradeOldStatuses: %v", err)
		return
	}

	existing := make(map[string]time.Time)
	for _, kv := range resp.Kvs {
		ts := new(TaskStatus)

		err := proto.Unmarshal(kv.Value, ts)
		if err != nil {
			log.WithFields(log.Fields{"err": err}).Error("parse status")
			continue
		}

		// Check that the status key actually belongs to us
		if r.Actions.Parse(ts.TaskKey) && ts.CheckManagerAndName(rm.Manager, r.Name) {
			// Record the time, so that when we upgrade later, we can check if it's newer
			existing[ts.TaskKey] = ts.GetWhen().AsTime()
		}
	}

	ops := new(storage.EtcdTransaction)

	for _, kv := range old_resp.Kvs {
		k := strings.TrimPrefix(string(kv.Key), status_pfx)

		if !r.Actions.Parse(k) {
			log.Infof("[%s/%s] Could not parse key, skipping: %s", rm.Manager, r.Name, k)
			continue
		}

		old := new(DeprStatus)
		err := proto.Unmarshal(kv.Value, old)
		if err != nil {
			log.WithFields(log.Fields{"err": err}).Error("parse old status")
			continue
		}

		if _, ok := keys[k]; !ok && old.Result == DeprStatus_Stale {
			log.Infof("[%s/%s] Deleting old stale status, task key does not exist: %s", rm.Manager, r.Name, string(kv.Key))
			ops.Delete(storage.NewGenericEtcdKey(string(kv.Key), nil))
			continue
		}

		// Skip upgrading if we've found a new-style key that's been written sooner
		if when, ok := existing[k]; ok && when.After(old.GetWhen().AsTime()) {
			log.Infof("[%s/%s] Deleting old status, newer status version already exists: %s, %s", rm.Manager, r.Name, string(kv.Key), k)
			ops.Delete(storage.NewGenericEtcdKey(string(kv.Key), nil))
			continue
		}

		// actually upgrade the key
		log.Infof("[%s/%s] Upgrading key: %s", rm.Manager, r.Name, string(kv.Key))

		s := old.Upgrade(rm, r, k, keys[k])

		err = s.AppendCreateOps(rm.EtcdCli, ops)
		if err != nil {
			log.WithFields(log.Fields{"err": err}).Error("create status ops")
			continue
		}

		ops.Delete(storage.NewGenericEtcdKey(string(kv.Key), nil))
	}

	_, _, err = ops.EtcdTx(rm.EtcdCli, "")
	if err != nil {
		log.WithFields(log.Fields{"err": err}).Error("etcdTxn")
	}

}
