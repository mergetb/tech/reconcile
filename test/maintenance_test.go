package test

import (
	// "context"
	// "encoding/binary"
	// "fmt"
	// "strings"
	"context"
	"testing"
	"time"

	// "github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	// storage "gitlab.com/mergetb/tech/shared/storage/etcd"
	rec "gitlab.com/mergetb/tech/reconcile"
)

const (
	maintainanceTimes  = 4
	maintainancePrefix = "/maintainancetest"
)

type MaintainanceTest struct {
}

func (mt *MaintainanceTest) Parse(k string) bool {
	return true
}

func (mt *MaintainanceTest) Create(value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return nil
}

func (mt *MaintainanceTest) Update(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return nil
}

func (mt *MaintainanceTest) Ensure(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return nil
}

func (mt *MaintainanceTest) Delete(prev []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return nil
}

func TestMaintainenceHealthcheck(t *testing.T) {

	clearEtcd()
	defer clearEtcd()

	//a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    1,
		HealthFrequency:            time.Millisecond,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  maintainancePrefix + "/",
			Name:    "maintainanceinit",
			Actions: &MaintainanceTest{},
		}},
	}

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := 0; i < maintainanceTimes; i++ {
		logrus.Infof("healthchecked: %d/%d", i, maintainanceTimes)
		tm.WaitAllUntil(rec.ReconcilerOp_Health)
		tm.WaitAllUntil(rec.ReconcilerOp_Idle)
	}
}

func TestMaintainencePollcheck(t *testing.T) {

	clearEtcd()
	defer clearEtcd()

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:             maintainancePrefix + "/",
			Name:               "maintainanceinit",
			Actions:            &MaintainanceTest{},
			PollCheckFrequency: time.Millisecond,
		}},
	}

	_, err := etcdCli.Put(context.Background(), maintainancePrefix+"/"+"test", "")
	a.Nil(err)

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := 0; i < maintainanceTimes; i++ {
		logrus.Infof("pollchecked: %d/%d", i, maintainanceTimes)
		tm.WaitAllUntil(rec.ReconcilerOp_Pollcheck)
		tm.WaitAllUntil(rec.ReconcilerOp_Idle)
	}

	_, err = etcdCli.Delete(context.Background(), maintainancePrefix+"/"+"test")
	a.Nil(err)

	for i := 0; i < maintainanceTimes; i++ {
		logrus.Infof("pollchecked: %d/%d", i, maintainanceTimes)
		tm.WaitAllUntil(rec.ReconcilerOp_Pollcheck)
		tm.WaitAllUntil(rec.ReconcilerOp_Idle)
	}
}

// this one will fail by changing the prefix
func TestMaintainenceFailPollcheck(t *testing.T) {

	clearEtcd()
	defer clearEtcd()

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    1,
		JustLogErrors:              true,
		Reconcilers: []*rec.Reconciler{{
			Prefix:             maintainancePrefix + "/" + maintainancePrefix + "/",
			Name:               "maintainanceinit",
			Actions:            &MaintainanceTest{},
			PollCheckFrequency: time.Second,
		}},
	}

	c := make(chan error, 1)

	go func() {
		c <- tm.Run()
	}()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	logrus.Infof("pollchecked")
	tm.WaitAllUntil(rec.ReconcilerOp_Pollcheck)
	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	tm.Reconcilers[0].Prefix = maintainancePrefix + "/"
	_, err := etcdCli.Put(context.Background(), maintainancePrefix+"/"+"test", "")
	a.Nil(err)

	err = <-c

	a.NotNilf(err, "err should not be nil: %+v", err)
}
