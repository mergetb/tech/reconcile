package test

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"testing"
	//"time"

	sets "github.com/deckarep/golang-set/v2"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	rec "gitlab.com/mergetb/tech/reconcile"
)

var prevtestPrefix = "/prevtest"
var prevtestResults chan int = nil
var prevtestTimes = 100

type PrevTest struct {
	Key     string
	Time    uint32
	Version int64
}

func (wt *PrevTest) Handle(event string, prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	if value == nil {
		return rec.TaskMessageErrorf("error")
	}

	log.Infof("found %s -> %s: %s", event, r.TaskValue.EtcdKey, value)
	cur_val, err := strconv.Atoi(string(value))
	if err != nil || value == nil {
		cur_val = 0
	}

	prev_val, err := strconv.Atoi(string(prev))
	if err != nil || prev == nil {
		prev_val = 0
	}

	prevtestResults <- -prev_val
	prevtestResults <- cur_val
	return nil
}

func (pt *PrevTest) Parse(k string) bool {
	return strings.HasPrefix(k, prevtestPrefix)
}

func (pt *PrevTest) Create(value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return pt.Handle("create", nil, value, version, r)
}

func (pt *PrevTest) Update(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return pt.Handle("update", prev, value, version, r)
}

func (pt *PrevTest) Ensure(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return pt.Handle("ensure", prev, value, version, r)
}

func (pt *PrevTest) Delete(prev []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return pt.Handle("delete", prev, nil, version, r)
}

func TestPrevInit(t *testing.T) {

	a := assert.New(t)

	clearEtcd()
	defer clearEtcd()
	prevtestResults = make(chan int, 2)
	defer close(prevtestResults)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  prevtestPrefix + "/",
			Name:    "previnit",
			Actions: &PrevTest{},
		}},
	}

	// put -- prev is nil, so we only expect -1 for prev

	for i := 1; i <= prevtestTimes; i++ {
		k := fmt.Sprintf("%s/%02d", prevtestPrefix, i)
		v := fmt.Sprint(i)

		_, err := etcdCli.Put(context.Background(), k, v)
		a.Nilf(err, "err %+v", err)
	}

	go tm.Run()
	defer tm.Stop()

	expected := sets.NewSet[int]()
	found := sets.NewSet[int]()

	for i := 1; i <= prevtestTimes; i++ {
		expected.Add(i)
		expected.Add(0)

		for j := 0; j < 2; j++ {
			f := <-prevtestResults
			if f != 0 {
				a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
			}

			found.Add(f)
		}

	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(prevtestResults), "extra elements in prevtestResults")
}

func TestPrevTwoPuts(t *testing.T) {

	a := assert.New(t)

	clearEtcd()
	defer clearEtcd()
	prevtestResults = make(chan int, 2)
	defer close(prevtestResults)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  prevtestPrefix + "/",
			Name:    "previnit",
			Actions: &PrevTest{},
		}},
	}

	go tm.Run()
	defer tm.Stop()

	// put -- prev is nil, so we only expect -1 for prev

	for i := 1; i <= prevtestTimes; i++ {
		k := fmt.Sprintf("%s/%02d", prevtestPrefix, i)
		v := fmt.Sprint(i)

		_, err := etcdCli.Put(context.Background(), k, v)
		a.Nilf(err, "err %+v", err)
	}

	expected := sets.NewSet[int]()
	found := sets.NewSet[int]()

	for i := 1; i <= prevtestTimes; i++ {
		expected.Add(i)
		expected.Add(0)

		for j := 0; j < 2; j++ {
			f := <-prevtestResults
			if f != 0 {
				a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
			}

			found.Add(f)
		}

	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(prevtestResults), "extra elements in prevtestResults")

	// put again, +100

	for i := 1; i <= prevtestTimes; i++ {
		k := fmt.Sprintf("%s/%02d", prevtestPrefix, i)
		v := fmt.Sprint(i + 100)

		_, err := etcdCli.Put(context.Background(), k, v)
		a.Nilf(err, "err %+v", err)
	}

	expected = sets.NewSet[int]()
	found = sets.NewSet[int]()

	for i := 1; i <= prevtestTimes; i++ {
		expected.Add(-i)
		expected.Add(i + 100)

		for j := 0; j < 2; j++ {
			f := <-prevtestResults
			if f != 0 {
				a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
			}

			found.Add(f)
		}

	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(prevtestResults), "extra elements in prevtestResults")
}
