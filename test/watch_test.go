package test

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"testing"

	sets "github.com/deckarep/golang-set/v2"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"go.etcd.io/etcd/client/v3"

	rec "gitlab.com/mergetb/tech/reconcile"
	storage "gitlab.com/mergetb/tech/shared/storage/etcd"
)

var watchPrefix = "/watchtest"
var watchResults chan int = nil
var watchtestTimes = 100

type WatchTest struct {
	Key     string
	Time    uint32
	Version int64
}

func (wt *WatchTest) Handle(event string, prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	if value == nil {
		value = prev
	}

	if value == nil {
		return nil
	}

	log.Infof("found %s -> %s: %s", event, r.TaskValue.EtcdKey, value)
	i, err := strconv.Atoi(string(value))
	if err != nil {
		i = -1
	}

	watchResults <- i

	return nil
}

func (wt *WatchTest) Parse(k string) bool {
	return strings.HasPrefix(k, watchPrefix)
}

func (wt *WatchTest) Create(value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return wt.Handle("create", nil, value, version, r)
}

func (wt *WatchTest) Update(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return wt.Handle("update", prev, value, version, r)
}

func (wt *WatchTest) Ensure(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return wt.Handle("ensure", prev, value, version, r)
}

func (wt *WatchTest) Delete(prev []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return wt.Handle("delete", prev, nil, version, r)
}

func TestWatchSeparateKeys(t *testing.T) {

	a := assert.New(t)

	clearEtcd()
	defer clearEtcd()
	watchResults = make(chan int, 1)
	defer close(watchResults)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  watchPrefix + "/",
			Name:    "watchinit",
			Actions: &WatchTest{},
		}},
	}

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := 0; i < watchtestTimes; i++ {
		k := fmt.Sprintf("%s/%02d", watchPrefix, i)
		v := fmt.Sprint(i)

		_, err := etcdCli.Put(context.Background(), k, v)
		a.Nilf(err, "err %+v", err)
	}

	expected := sets.NewSet[int]()
	found := sets.NewSet[int]()

	for i := 0; i < watchtestTimes; i++ {
		expected.Add(i)

		f := <-watchResults
		a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
		found.Add(f)
	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")
}

func TestWatchMoreSeparateKeys(t *testing.T) {

	watchtestTimes := 10 * watchtestTimes

	a := assert.New(t)

	clearEtcd()
	defer clearEtcd()
	watchResults = make(chan int, 1)
	defer close(watchResults)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  watchPrefix + "/",
			Name:    "watchinit",
			Actions: &WatchTest{},
		}},
	}

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	var ops storage.EtcdTransaction

	for i := 0; i < watchtestTimes; i++ {
		k := fmt.Sprintf("%s/%02d", watchPrefix, i)
		v := fmt.Sprint(i)

		ops.WriteKey(k, []byte(v))

	}

	_, _, err := ops.EtcdTx(etcdCli, "")

	a.Nilf(err, "err %+v", err)

	expected := sets.NewSet[int]()
	found := sets.NewSet[int]()

	for i := 0; i < watchtestTimes; i++ {
		expected.Add(i)

		f := <-watchResults
		a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
		found.Add(f)
	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")
}

func TestWatchSameKey(t *testing.T) {

	a := assert.New(t)

	clearEtcd()
	defer clearEtcd()
	watchResults = make(chan int, 1)
	defer close(watchResults)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  watchPrefix + "/",
			Name:    "watchinit",
			Actions: &WatchTest{},
		}},
	}

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	log.Infof("watchtest len: %d", len(watchResults))

	for i := 0; i < watchtestTimes; i++ {
		k := fmt.Sprintf("%s/single", watchPrefix)
		v := fmt.Sprint(i)

		_, err := etcdCli.Put(context.Background(), k, v)
		a.Nilf(err, "err %+v", err)
	}

	expected := sets.NewSet[int]()
	found := sets.NewSet[int]()

	for i := 0; i < watchtestTimes; i++ {
		expected.Add(i)

		f := <-watchResults
		a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
		found.Add(f)
	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")
}

// Hidden test because it's just sometimes that causes
// the etcd compaction to cause the watch to error out
func testWatch100Compact(t *testing.T) {

	a := assert.New(t)

	clearEtcd()
	defer clearEtcd()
	watchResults = make(chan int, 1)
	defer close(watchResults)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  watchPrefix + "/",
			Name:    "watchinit",
			Actions: &WatchTest{},
		}},
	}

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := 0; i < watchtestTimes; i++ {
		k := fmt.Sprintf("%s/single", watchPrefix)
		v := fmt.Sprint(i)

		resp, err := etcdCli.Put(context.Background(), k, v)
		a.Nilf(err, "err %+v", err)

		if i == watchtestTimes/2 {
			_, err := etcdCli.Compact(context.Background(), resp.Header.Revision)
			a.Nilf(err, "err %+v", err)

			for _, e := range etcdCli.Endpoints() {
				_, err = etcdCli.Defragment(context.Background(), e)
				a.Nilf(err, "err %+v", err)
			}
		}

	}

	expected := sets.NewSet[int]()
	found := sets.NewSet[int]()

	for i := 0; i < watchtestTimes; i++ {
		expected.Add(i)

		f := <-watchResults
		a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
		found.Add(f)
	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")
}

func TestWatchInitDelete(t *testing.T) {

	a := assert.New(t)

	clearEtcd()
	defer clearEtcd()
	watchResults = make(chan int, 1)
	defer close(watchResults)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  watchPrefix + "/",
			Name:    "watchinit",
			Actions: &WatchTest{},
		}},
	}

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := 0; i < watchtestTimes; i++ {
		k := fmt.Sprintf("%s/%02d", watchPrefix, i)
		v := fmt.Sprint(i)

		_, err := etcdCli.Put(context.Background(), k, v)
		a.Nilf(err, "err %+v", err)
	}

	expected := sets.NewSet[int]()
	found := sets.NewSet[int]()

	for i := 0; i < watchtestTimes; i++ {
		expected.Add(i)

		f := <-watchResults

		a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
		found.Add(f)
	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")

	tm.Stop()

	for i := 0; i < watchtestTimes; i++ {
		k := fmt.Sprintf("%s/%02d", watchPrefix, i)

		_, err := etcdCli.Delete(context.Background(), k)
		a.Nilf(err, "err %+v", err)
	}

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	found = sets.NewSet[int]()
	for i := 0; i < watchtestTimes; i++ {
		expected.Add(i)

		f := <-watchResults
		a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
		found.Add(f)
	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")

}

func TestWatchExpired(t *testing.T) {

	a := assert.New(t)

	clearEtcd()
	defer clearEtcd()
	watchResults = make(chan int, 1)
	defer close(watchResults)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  watchPrefix + "/",
			Name:    "watchinit",
			Actions: &WatchTest{},
		}},
	}

	go tm.Run()
	defer tm.Stop()

	lease, err := etcdCli.Grant(context.Background(), 1)
	a.Nil(err)

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := 0; i < watchtestTimes; i++ {
		k := fmt.Sprintf("%s/%02d", watchPrefix, i)
		v := fmt.Sprint(i)

		_, err := etcdCli.Put(context.Background(), k, v, clientv3.WithLease(lease.ID))
		a.Nilf(err, "err %+v", err)
	}

	expected := sets.NewSet[int]()
	found := sets.NewSet[int]()

	for i := 0; i < watchtestTimes; i++ {
		expected.Add(i)

		f := <-watchResults

		a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
		found.Add(f)
	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	found = sets.NewSet[int]()
	for i := 0; i < watchtestTimes; i++ {
		expected.Add(i)

		f := <-watchResults
		a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
		found.Add(f)
	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")

}

func TestWatchInitDeleteInit(t *testing.T) {

	a := assert.New(t)

	clearEtcd()
	defer clearEtcd()
	watchResults = make(chan int, 1)
	defer close(watchResults)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  watchPrefix + "/",
			Name:    "watchinit",
			Actions: &WatchTest{},
		}},
	}

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := 0; i < watchtestTimes; i++ {
		k := fmt.Sprintf("%s/%02d", watchPrefix, i)
		v := fmt.Sprint(i)

		_, err := etcdCli.Put(context.Background(), k, v)
		a.Nilf(err, "err %+v", err)
	}

	expected := sets.NewSet[int]()
	found := sets.NewSet[int]()

	for i := 0; i < watchtestTimes; i++ {
		expected.Add(i)

		f := <-watchResults

		a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
		found.Add(f)
	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")

	tm.Stop()

	for i := 0; i < watchtestTimes; i++ {
		k := fmt.Sprintf("%s/%02d", watchPrefix, i)
		v := fmt.Sprint(i)

		_, err := etcdCli.Delete(context.Background(), k)
		a.Nilf(err, "err %+v", err)

		_, err = etcdCli.Put(context.Background(), k, v)
		a.Nilf(err, "err %+v", err)
	}

	go tm.Run()
	defer tm.Stop()

	found = sets.NewSet[int]()
	for i := 0; i < watchtestTimes; i++ {
		expected.Add(i)

		f1 := <-watchResults
		f2 := <-watchResults

		a.Equalf(f1, f2, "somehow, two values are not equal: %d vs. %d", f1, f2)

		a.Falsef(found.Contains(f1), "somehow, duplicate in found: %+v")
		found.Add(f1)
	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")
}

// Mostly to test that the same key with async doesn't oobviously break
func TestWatchSameKeyAsync(t *testing.T) {

	a := assert.New(t)

	clearEtcd()
	defer clearEtcd()
	watchResults = make(chan int, 1)
	defer close(watchResults)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  watchPrefix + "/",
			Name:    "watchinit",
			Actions: &WatchTest{},
		}},
	}

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	log.Infof("watchtest len: %d", len(watchResults))

	for i := 0; i < watchtestTimes; i++ {
		k := fmt.Sprintf("%s/single", watchPrefix)
		v := fmt.Sprint(i)

		_, err := etcdCli.Put(context.Background(), k, v)
		a.Nilf(err, "err %+v", err)
	}

	expected := sets.NewSet[int]()
	found := sets.NewSet[int]()

	for i := 0; i < watchtestTimes; i++ {
		expected.Add(i)

		f := <-watchResults
		a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
		a.Equalf(f, i, "should be equal: %d != %d", f, i)
		found.Add(f)
	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")
}
