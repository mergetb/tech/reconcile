package test

import (
	"context"
	"fmt"
	"strings"
	"sync"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"google.golang.org/protobuf/encoding/protojson"

	rec "gitlab.com/mergetb/tech/reconcile"
	storage "gitlab.com/mergetb/tech/shared/storage/etcd"
)

// Userhome is mergefs create user dir
type UserHome struct{}

func (uh *UserHome) Parse(k string) bool { return true }
func (uh *UserHome) Create(value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return nil
}
func (uh *UserHome) Update(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return nil
}
func (uh *UserHome) Ensure(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return nil
}
func (uh *UserHome) Delete(value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return nil
}
func (uh *UserHome) Expired(t *rec.Reconciler) { return }
func (uh *UserHome) TaskDescription(k string) string {
	tkns := strings.Split(k, "/")
	return fmt.Sprintf("Create $HOME for %s", tkns[1])
}

// Userkeys places user keys in user's $HOME
type WriteUserKeys struct{}

func (uk *WriteUserKeys) Parse(k string) bool { return true }
func (uk *WriteUserKeys) Create(value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return nil
}
func (uk *WriteUserKeys) Update(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return nil
}
func (uk *WriteUserKeys) Ensure(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return nil
}
func (uk *WriteUserKeys) Delete(value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return nil
}
func (uk *WriteUserKeys) Expired(t *rec.Reconciler) { return }

// CreateKeys creates user keys and certs.
type CreateKeys struct {
	user string
}

func (ck *CreateKeys) Parse(k string) bool {
	tkns := strings.Split(k, "/")
	log.Debugf("CreateKeys.Parse: %s -> %+v", k, tkns)

	ck.user = tkns[2]
	return true
}
func (ck *CreateKeys) Create(value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	ge := storage.NewGenericEtcdKey("/keys/"+ck.user, []byte(ck.user))
	_, err := ge.Update(etcdCli)

	if err != nil {
		return rec.CheckErrorToMessage(err)
	}

	r.AddWrittenKey(ge.Key())

	return rec.CheckErrorToMessage(err)
}
func (ck *CreateKeys) Update(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return nil
}
func (ck *CreateKeys) Ensure(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return nil
}
func (ck *CreateKeys) Delete(value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return nil
}
func (ck *CreateKeys) Expired(t *rec.Reconciler) { return }

func TestSubTaskTreesUnspecifiedReconciler(t *testing.T) {
	clearEtcd()
	defer clearEtcd()

	userprefix := "/users/"
	user := "murphy"
	newuser := userprefix + user
	keyprefix := "/keys/"
	newcreds := keyprefix + user

	createHomeTask := &rec.Reconciler{
		Prefix:  userprefix,
		Name:    "userhome",
		Actions: &UserHome{},
	}

	writeKeysTask := &rec.Reconciler{
		Prefix:  keyprefix,
		Name:    "userkeys",
		Desc:    "Place user keys in $HOME/.ssh",
		Actions: &WriteUserKeys{},
	}

	createKeysTask := &rec.Reconciler{
		Prefix:  userprefix,
		Name:    "createuserkeys",
		Desc:    "Create ssh creds",
		Actions: &CreateKeys{},
	}

	mergefsRec := &rec.ReconcilerManager{
		Manager:                    "mergefs",
		EtcdCli:                    etcdCli,
		Reconcilers:                []*rec.Reconciler{createHomeTask, writeKeysTask},
		ServerHeartbeatGracePeriod: -1,
	}

	credRec := &rec.ReconcilerManager{
		Manager:                    "cred",
		EtcdCli:                    etcdCli,
		Reconcilers:                []*rec.Reconciler{createKeysTask},
		ServerHeartbeatGracePeriod: -1,
	}

	a := assert.New(t)

	// write keys
	ops := new(storage.EtcdTransaction)

	ops.Write(storage.NewGenericEtcdKey(newuser, []byte(user)))

	_, _, err := ops.EtcdTx(etcdCli, "")
	if err != nil {
		t.Errorf("etcd tx: %v", err)
	}

	// create task goals. This is something that apiserver would do
	// as a top-level task creator.
	user_goal := rec.NewGoal(newuser, newuser, "Create user murphy")

	user_goal.AddTaskRecord(
		&rec.TaskRecord{
			Task: newuser,
		},
		&rec.TaskRecord{
			Task: newcreds,
		},
	)

	user_goal.Create(etcdCli)

	go credRec.Run()
	defer credRec.Stop()
	credRec.WaitAllUntil(rec.ReconcilerOp_Idle)

	go mergefsRec.Run()
	defer mergefsRec.Stop()
	mergefsRec.WaitAllUntil(rec.ReconcilerOp_Idle)

	tf, err := user_goal.GetTaskForest(etcdCli, nil)
	a.Nil(err)

	// confirm at least the correct number of tasks.
	//
	// the tree is grouped by reconciler, so it looks like:
	// "/cred/createuserkeys" ==> "/users/murphy" ==> "/keys/murphy"
	// "/mergefs/userhome" ==> "/users/murphy"
	// "/mergefs/userkeys" ==> "/keys/murphy"
	//
	//

	t.Logf("%s\n", protojson.Format(tf))

	a.NotEqual(rec.TaskStatus_Error, tf.HighestStatus)
	a.Equal(rec.TaskStatus_Success, tf.HighestStatus)
	a.Len(tf.Subgoals, 3)
	a.Equal(int64(4), tf.NumChildTasks)

	for _, tree := range tf.Subgoals {
		switch tree.Goal.SelfKey {
		case "/mergefs/userkeys":
			a.Equal(0, tree.NumChildTasks)
			a.Len(tree.Subtasks, 1)
		case "/cred/createuserkeys":
			// this spawned another task, so it should have a child task
			a.Equal(1, tree.NumChildTasks)
			a.Len(tree.Subtasks, 1)
		case "/mergefs/userhome":
			a.Equal(0, tree.NumChildTasks)
			a.Len(tree.Subtasks, 1)
		}
	}
}

func TestSubTaskTreesSpecifiedReconciler(t *testing.T) {
	clearEtcd()
	defer clearEtcd()

	userprefix := "/users/"
	user := "murphy"
	newuser := userprefix + user
	keyprefix := "/keys/"
	newcreds := keyprefix + user

	createHomeTask := &rec.Reconciler{
		Prefix:  userprefix,
		Name:    "userhome",
		Actions: &UserHome{},
	}

	writeKeysTask := &rec.Reconciler{
		Prefix:  keyprefix,
		Name:    "userkeys",
		Desc:    "Place user keys in $HOME/.ssh",
		Actions: &WriteUserKeys{},
	}

	createKeysTask := &rec.Reconciler{
		Prefix:  userprefix,
		Name:    "createuserkeys",
		Desc:    "Create ssh creds",
		Actions: &CreateKeys{},
	}

	mergefsRec := &rec.ReconcilerManager{
		Manager:                    "mergefs",
		EtcdCli:                    etcdCli,
		Reconcilers:                []*rec.Reconciler{createHomeTask, writeKeysTask},
		ServerHeartbeatGracePeriod: -1,
	}

	credRec := &rec.ReconcilerManager{
		Manager:                    "cred",
		EtcdCli:                    etcdCli,
		Reconcilers:                []*rec.Reconciler{createKeysTask},
		ServerHeartbeatGracePeriod: -1,
	}

	a := assert.New(t)

	// write keys
	ops := new(storage.EtcdTransaction)

	ops.Write(storage.NewGenericEtcdKey(newuser, []byte(user)))

	_, _, err := ops.EtcdTx(etcdCli, "")
	if err != nil {
		t.Errorf("etcd tx: %v", err)
	}

	// create task goals. This is something that apiserver would do
	// as a top-level task creator.
	user_goal := rec.NewGoal(newuser, newuser, "Create user murphy")

	user_goal.AddTaskRecord(
		&rec.TaskRecord{
			Task:              newuser,
			ReconcilerManager: mergefsRec.Manager,
			ReconcilerName:    createHomeTask.Name,
		},
		&rec.TaskRecord{
			Task:              newcreds,
			ReconcilerManager: mergefsRec.Manager,
			ReconcilerName:    writeKeysTask.Name,
		},
		&rec.TaskRecord{
			Task:              newuser,
			ReconcilerManager: credRec.Manager,
			ReconcilerName:    createKeysTask.Name,
		},
	)

	go credRec.Run()
	defer credRec.Stop()
	credRec.WaitAllUntil(rec.ReconcilerOp_Idle)

	go mergefsRec.Run()
	defer mergefsRec.Stop()
	mergefsRec.WaitAllUntil(rec.ReconcilerOp_Idle)

	tf, err := user_goal.GetTaskForest(etcdCli, nil)
	a.Nil(err)

	// confirm at least the correct number of tasks.
	//
	// the tree is grouped by reconciler, so it looks like:
	// "/cred/createuserkeys" ==> "/users/murphy" ==> "/keys/murphy"
	// "/mergefs/userhome" ==> "/users/murphy"
	// "/mergefs/userkeys" ==> "/keys/murphy"
	//
	//

	t.Logf("%s\n", protojson.Format(tf))

	a.NotEqual(rec.TaskStatus_Error, tf.HighestStatus)
	a.Equal(rec.TaskStatus_Success, tf.HighestStatus)
	a.Len(tf.Subgoals, 3)
	a.Equal(int64(4), tf.NumChildTasks)

	for _, tree := range tf.Subgoals {
		switch tree.Goal.SelfKey {
		case "/mergefs/userkeys":
			a.Equal(0, tree.NumChildTasks)
			a.Len(tree.Subtasks, 1)
		case "/cred/createuserkeys":
			// this spawned another task, so it should have a child task
			a.Equal(1, tree.NumChildTasks)
			a.Len(tree.Subtasks, 1)
		case "/mergefs/userhome":
			a.Equal(0, tree.NumChildTasks)
			a.Len(tree.Subtasks, 1)
		}
	}
}

func TestSubTaskTreesSpecifiedReconcilerMissing(t *testing.T) {
	clearEtcd()
	defer clearEtcd()

	userprefix := "/users/"
	user := "murphy"
	newuser := userprefix + user
	keyprefix := "/keys/"
	newcreds := keyprefix + user

	createHomeTask := &rec.Reconciler{
		Prefix:  userprefix,
		Name:    "userhome",
		Actions: &UserHome{},
	}

	writeKeysTask := &rec.Reconciler{
		Prefix:  keyprefix,
		Name:    "userkeys",
		Desc:    "Place user keys in $HOME/.ssh",
		Actions: &WriteUserKeys{},
	}

	createKeysTask := &rec.Reconciler{
		Prefix:  userprefix,
		Name:    "createuserkeys",
		Desc:    "Create ssh creds",
		Actions: &CreateKeys{},
	}

	mergefsRec := &rec.ReconcilerManager{
		Manager:                    "mergefs",
		EtcdCli:                    etcdCli,
		Reconcilers:                []*rec.Reconciler{createHomeTask, writeKeysTask},
		ServerHeartbeatGracePeriod: -1,
	}

	credRec := &rec.ReconcilerManager{
		Manager:                    "cred",
		EtcdCli:                    etcdCli,
		Reconcilers:                []*rec.Reconciler{createKeysTask},
		ServerHeartbeatGracePeriod: -1,
	}

	a := assert.New(t)

	// write keys
	ops := new(storage.EtcdTransaction)

	ops.Write(storage.NewGenericEtcdKey(newuser, []byte(user)))

	_, _, err := ops.EtcdTx(etcdCli, "")
	if err != nil {
		t.Errorf("etcd tx: %v", err)
	}

	// create task goals. This is something that apiserver would do
	// as a top-level task creator.
	user_goal := rec.NewGoal(newuser, newuser, "Create user murphy")

	user_goal.AddTaskRecord(
		&rec.TaskRecord{
			Task:              newuser,
			ReconcilerManager: mergefsRec.Manager,
			ReconcilerName:    createHomeTask.Name,
		},
		&rec.TaskRecord{
			Task:              newcreds,
			ReconcilerManager: mergefsRec.Manager,
			ReconcilerName:    writeKeysTask.Name,
		},
		&rec.TaskRecord{
			Task:              newuser,
			ReconcilerManager: credRec.Manager,
			ReconcilerName:    createKeysTask.Name,
		},
	)

	// go credRec.Run()
	// defer credRec.Stop()
	// credRec.WaitAllUntil(rec.ReconcilerOp_Idle)

	go mergefsRec.Run()
	defer mergefsRec.Stop()
	mergefsRec.WaitAllUntil(rec.ReconcilerOp_Idle)

	tf, err := user_goal.GetTaskForest(etcdCli, nil)
	a.Nil(err)

	// confirm at least the correct number of tasks.
	//
	// the tree is grouped by reconciler, so it should look like:
	// "/cred/createuserkeys" ==> "/users/murphy" ==> "/keys/murphy"
	// "/mergefs/userhome" ==> "/users/murphy"
	// "/mergefs/userkeys" ==> "/keys/murphy"
	//
	// however, as cred was not run, it actually looks like
	// "/cred/createuserkeys" ==> missing "/users/murphy"
	// "/mergefs/userhome" ==> "/users/murphy"
	// "/mergefs/userkeys" ==> missing "/keys/murphy"

	t.Logf("%s\n", protojson.Format(tf))

	a.NotEqual(rec.TaskStatus_Error, tf.HighestStatus)
	a.Equal(rec.TaskStatus_Pending, tf.HighestStatus)
	a.Len(tf.Subgoals, 3)
	a.Equal(int64(3), tf.NumChildTasks)

	for _, tree := range tf.Subgoals {
		switch tree.Goal.SelfKey {
		case "/mergefs/userkeys":
			a.Equal(0, tree.NumChildTasks)
			a.Len(tree.Subtasks, 1)
			a.Equal(rec.TaskStatus_Pending, tree.HighestStatus)
		case "/cred/createuserkeys":
			// cred never ran, so it didn't spawn the key
			a.Equal(0, tree.NumChildTasks)
			a.Len(tree.Subtasks, 1)
			a.Equal(rec.TaskStatus_Pending, tree.HighestStatus)
		case "/mergefs/userhome":
			a.Equal(0, tree.NumChildTasks)
			a.Len(tree.Subtasks, 1)
			a.Equal(rec.TaskStatus_Success, tree.HighestStatus)
		}
	}
}

func TestSubTaskTreesWatchWithoutCache(t *testing.T) {
	clearEtcd()
	defer clearEtcd()

	userprefix := "/users/"
	user := "murphy"
	newuser := userprefix + user
	keyprefix := "/keys/"
	newcreds := keyprefix + user

	createHomeTask := &rec.Reconciler{
		Prefix:  userprefix,
		Name:    "userhome",
		Actions: &UserHome{},
	}

	writeKeysTask := &rec.Reconciler{
		Prefix:  keyprefix,
		Name:    "userkeys",
		Desc:    "Place user keys in $HOME/.ssh",
		Actions: &WriteUserKeys{},
	}

	createKeysTask := &rec.Reconciler{
		Prefix:  userprefix,
		Name:    "createuserkeys",
		Desc:    "Create ssh creds",
		Actions: &CreateKeys{},
	}

	mergefsRec := &rec.ReconcilerManager{
		Manager:                    "mergefs",
		EtcdCli:                    etcdCli,
		Reconcilers:                []*rec.Reconciler{createHomeTask, writeKeysTask},
		ServerHeartbeatGracePeriod: -1,
	}

	credRec := &rec.ReconcilerManager{
		Manager:                    "cred",
		EtcdCli:                    etcdCli,
		Reconcilers:                []*rec.Reconciler{createKeysTask},
		ServerHeartbeatGracePeriod: -1,
	}

	a := assert.New(t)

	// write keys
	ops := new(storage.EtcdTransaction)

	ops.Write(storage.NewGenericEtcdKey(newuser, []byte(user)))

	_, _, err := ops.EtcdTx(etcdCli, "")
	if err != nil {
		t.Errorf("etcd tx: %v", err)
	}

	// create task goals. This is something that apiserver would do
	// as a top-level task creator.
	user_goal := rec.NewGoal(newuser, newuser, "Create user murphy")

	user_goal.AddTaskRecord(
		&rec.TaskRecord{
			Task:              newuser,
			ReconcilerManager: mergefsRec.Manager,
			ReconcilerName:    createHomeTask.Name,
		},
		&rec.TaskRecord{
			Task:              newcreds,
			ReconcilerManager: mergefsRec.Manager,
			ReconcilerName:    writeKeysTask.Name,
		},
		&rec.TaskRecord{
			Task:              newuser,
			ReconcilerManager: credRec.Manager,
			ReconcilerName:    createKeysTask.Name,
		},
	)

	var wg sync.WaitGroup
	var tf *rec.TaskForest

	wg.Add(1)
	go func() {
		defer wg.Done()

		tf, err = user_goal.WatchTaskForestWithOptions(context.Background(), etcdCli, nil)
		a.Nil(err)
	}()

	go credRec.Run()
	defer credRec.Stop()

	go mergefsRec.Run()
	defer mergefsRec.Stop()

	// confirm at least the correct number of tasks.
	//
	// the tree is grouped by reconciler, so it looks like:
	// "/cred/createuserkeys" ==> "/users/murphy" ==> "/keys/murphy"
	// "/mergefs/userhome" ==> "/users/murphy"
	// "/mergefs/userkeys" ==> "/keys/murphy"
	//
	//

	wg.Wait()

	t.Logf("%s\n", protojson.Format(tf))

	a.NotEqual(rec.TaskStatus_Error, tf.HighestStatus)
	a.Equal(rec.TaskStatus_Success, tf.HighestStatus)
	a.Len(tf.Subgoals, 3)
	a.Equal(int64(4), tf.NumChildTasks)

	for _, tree := range tf.Subgoals {
		switch tree.Goal.SelfKey {
		case "/mergefs/userkeys":
			a.Equal(0, tree.NumChildTasks)
			a.Len(tree.Subtasks, 1)
		case "/cred/createuserkeys":
			// this spawned another task, so it should have a child task
			a.Equal(1, tree.NumChildTasks)
			a.Len(tree.Subtasks, 1)
		case "/mergefs/userhome":
			a.Equal(0, tree.NumChildTasks)
			a.Len(tree.Subtasks, 1)
		}
	}
}

func TestSubTaskTreesWatchCache(t *testing.T) {
	clearEtcd()
	defer clearEtcd()

	userprefix := "/users/"
	user := "murphy"
	newuser := userprefix + user
	keyprefix := "/keys/"
	newcreds := keyprefix + user

	createHomeTask := &rec.Reconciler{
		Prefix:  userprefix,
		Name:    "userhome",
		Actions: &UserHome{},
	}

	writeKeysTask := &rec.Reconciler{
		Prefix:  keyprefix,
		Name:    "userkeys",
		Desc:    "Place user keys in $HOME/.ssh",
		Actions: &WriteUserKeys{},
	}

	createKeysTask := &rec.Reconciler{
		Prefix:  userprefix,
		Name:    "createuserkeys",
		Desc:    "Create ssh creds",
		Actions: &CreateKeys{},
	}

	mergefsRec := &rec.ReconcilerManager{
		Manager:                    "mergefs",
		EtcdCli:                    etcdCli,
		Reconcilers:                []*rec.Reconciler{createHomeTask, writeKeysTask},
		ServerHeartbeatGracePeriod: -1,
	}

	credRec := &rec.ReconcilerManager{
		Manager:                    "cred",
		EtcdCli:                    etcdCli,
		Reconcilers:                []*rec.Reconciler{createKeysTask},
		ServerHeartbeatGracePeriod: -1,
	}

	a := assert.New(t)

	// write keys
	ops := new(storage.EtcdTransaction)

	ops.Write(storage.NewGenericEtcdKey(newuser, []byte(user)))

	_, _, err := ops.EtcdTx(etcdCli, "")
	if err != nil {
		t.Errorf("etcd tx: %v", err)
	}

	// create task goals. This is something that apiserver would do
	// as a top-level task creator.
	user_goal := rec.NewGoal(newuser, newuser, "Create user murphy")

	user_goal.AddTaskRecord(
		&rec.TaskRecord{
			Task:              newuser,
			ReconcilerManager: mergefsRec.Manager,
			ReconcilerName:    createHomeTask.Name,
		},
		&rec.TaskRecord{
			Task:              newcreds,
			ReconcilerManager: mergefsRec.Manager,
			ReconcilerName:    writeKeysTask.Name,
		},
		&rec.TaskRecord{
			Task:              newuser,
			ReconcilerManager: credRec.Manager,
			ReconcilerName:    createKeysTask.Name,
		},
	)

	var wg sync.WaitGroup
	var tf *rec.TaskForest

	wg.Add(1)
	go func() {
		defer wg.Done()

		cache := storage.NewReadCache(0, nil)
		defer cache.Close()

		tf, err = user_goal.WatchTaskForestWithOptions(context.Background(), etcdCli, cache)
		a.Nil(err)
	}()

	go credRec.Run()
	defer credRec.Stop()

	go mergefsRec.Run()
	defer mergefsRec.Stop()

	// confirm at least the correct number of tasks.
	//
	// the tree is grouped by reconciler, so it looks like:
	// "/cred/createuserkeys" ==> "/users/murphy" ==> "/keys/murphy"
	// "/mergefs/userhome" ==> "/users/murphy"
	// "/mergefs/userkeys" ==> "/keys/murphy"
	//
	//

	wg.Wait()

	t.Logf("%s\n", protojson.Format(tf))

	a.NotEqual(rec.TaskStatus_Error, tf.HighestStatus)
	a.Equal(rec.TaskStatus_Success, tf.HighestStatus)
	a.Len(tf.Subgoals, 3)
	a.Equal(int64(4), tf.NumChildTasks)

	for _, tree := range tf.Subgoals {
		switch tree.Goal.SelfKey {
		case "/mergefs/userkeys":
			a.Equal(0, tree.NumChildTasks)
			a.Len(tree.Subtasks, 1)
		case "/cred/createuserkeys":
			// this spawned another task, so it should have a child task
			a.Equal(1, tree.NumChildTasks)
			a.Len(tree.Subtasks, 1)
		case "/mergefs/userhome":
			a.Equal(0, tree.NumChildTasks)
			a.Len(tree.Subtasks, 1)
		}
	}
}

func TestSubTaskTreesSpecifiedReconcilerMissingWatch(t *testing.T) {
	clearEtcd()
	defer clearEtcd()

	userprefix := "/users/"
	user := "murphy"
	newuser := userprefix + user
	keyprefix := "/keys/"
	newcreds := keyprefix + user

	createHomeTask := &rec.Reconciler{
		Prefix:  userprefix,
		Name:    "userhome",
		Actions: &UserHome{},
	}

	writeKeysTask := &rec.Reconciler{
		Prefix:  keyprefix,
		Name:    "userkeys",
		Desc:    "Place user keys in $HOME/.ssh",
		Actions: &WriteUserKeys{},
	}

	createKeysTask := &rec.Reconciler{
		Prefix:  userprefix,
		Name:    "createuserkeys",
		Desc:    "Create ssh creds",
		Actions: &CreateKeys{},
	}

	mergefsRec := &rec.ReconcilerManager{
		Manager:                    "mergefs",
		EtcdCli:                    etcdCli,
		Reconcilers:                []*rec.Reconciler{createHomeTask, writeKeysTask},
		ServerHeartbeatGracePeriod: -1,
	}

	credRec := &rec.ReconcilerManager{
		Manager:                    "cred",
		EtcdCli:                    etcdCli,
		Reconcilers:                []*rec.Reconciler{createKeysTask},
		ServerHeartbeatGracePeriod: -1,
	}

	a := assert.New(t)

	// write keys
	ops := new(storage.EtcdTransaction)

	ops.Write(storage.NewGenericEtcdKey(newuser, []byte(user)))

	_, _, err := ops.EtcdTx(etcdCli, "")
	if err != nil {
		t.Errorf("etcd tx: %v", err)
	}

	// create task goals. This is something that apiserver would do
	// as a top-level task creator.
	user_goal := rec.NewGoal(newuser, newuser, "Create user murphy")

	user_goal.AddTaskRecord(
		&rec.TaskRecord{
			Task:              newuser,
			ReconcilerManager: mergefsRec.Manager,
			ReconcilerName:    createHomeTask.Name,
		},
		&rec.TaskRecord{
			Task:              newcreds,
			ReconcilerManager: mergefsRec.Manager,
			ReconcilerName:    writeKeysTask.Name,
		},
		&rec.TaskRecord{
			Task:              newuser,
			ReconcilerManager: credRec.Manager,
			ReconcilerName:    createKeysTask.Name,
		},
	)

	var wg sync.WaitGroup
	var tf *rec.TaskForest

	wg.Add(1)
	go func() {
		defer wg.Done()

		tf, err = user_goal.WatchTaskForest(etcdCli, 2*time.Second)
		a.Nil(err)
	}()

	// go credRec.Run()
	// defer credRec.Stop()
	// credRec.WaitAllUntil(rec.ReconcilerOp_Idle)

	go mergefsRec.Run()
	defer mergefsRec.Stop()
	mergefsRec.WaitAllUntil(rec.ReconcilerOp_Idle)

	// confirm at least the correct number of tasks.
	//
	// the tree is grouped by reconciler, so it should look like:
	// "/cred/createuserkeys" ==> "/users/murphy" ==> "/keys/murphy"
	// "/mergefs/userhome" ==> "/users/murphy"
	// "/mergefs/userkeys" ==> "/keys/murphy"
	//
	// however, as cred was not run, it actually looks like
	// "/cred/createuserkeys" ==> missing "/users/murphy"
	// "/mergefs/userhome" ==> "/users/murphy"
	// "/mergefs/userkeys" ==> missing "/keys/murphy"

	wg.Wait()

	t.Logf("%s\n", protojson.Format(tf))

	if tf == nil {
		t.FailNow()
	}

	a.NotEqual(rec.TaskStatus_Error, tf.HighestStatus)
	a.Equal(rec.TaskStatus_Pending, tf.HighestStatus)
	a.Len(tf.Subgoals, 3)
	a.Equal(int64(3), tf.NumChildTasks)

	for _, tree := range tf.Subgoals {
		switch tree.Goal.SelfKey {
		case "/mergefs/userkeys":
			a.Equal(0, tree.NumChildTasks)
			a.Len(tree.Subtasks, 1)
			a.Equal(rec.TaskStatus_Pending, tree.HighestStatus)
		case "/cred/createuserkeys":
			// cred never ran, so it didn't spawn the key
			a.Equal(0, tree.NumChildTasks)
			a.Len(tree.Subtasks, 1)
			a.Equal(rec.TaskStatus_Pending, tree.HighestStatus)
		case "/mergefs/userhome":
			a.Equal(0, tree.NumChildTasks)
			a.Len(tree.Subtasks, 1)
			a.Equal(rec.TaskStatus_Success, tree.HighestStatus)
		}
	}
}

func TestSubTaskTreesSpecifiedReconcilerPrefix(t *testing.T) {
	clearEtcd()
	defer clearEtcd()

	userprefix := "/users/"
	users := []string{"murphy", "olive"}
	keyprefix := "/keys/"

	createHomeTask := &rec.Reconciler{
		Prefix:  userprefix,
		Name:    "userhome",
		Actions: &UserHome{},
	}

	writeKeysTask := &rec.Reconciler{
		Prefix:  keyprefix,
		Name:    "userkeys",
		Desc:    "Place user keys in $HOME/.ssh",
		Actions: &WriteUserKeys{},
	}

	createKeysTask := &rec.Reconciler{
		Prefix:  userprefix,
		Name:    "createuserkeys",
		Desc:    "Create ssh creds",
		Actions: &CreateKeys{},
	}

	mergefsRec := &rec.ReconcilerManager{
		Manager:                    "mergefs",
		EtcdCli:                    etcdCli,
		Reconcilers:                []*rec.Reconciler{createHomeTask, writeKeysTask},
		ServerHeartbeatGracePeriod: -1,
	}

	credRec := &rec.ReconcilerManager{
		Manager:                    "cred",
		EtcdCli:                    etcdCli,
		Reconcilers:                []*rec.Reconciler{createKeysTask},
		ServerHeartbeatGracePeriod: -1,
	}

	a := assert.New(t)

	// write keys
	ops := new(storage.EtcdTransaction)

	for _, user := range users {
		ops.Write(storage.NewGenericEtcdKey(userprefix+user, []byte(user)))
	}

	_, _, err := ops.EtcdTx(etcdCli, "")
	if err != nil {
		t.Errorf("etcd tx: %v", err)
	}

	// create task goals. This is something that apiserver would do
	// as a top-level task creator.
	user_goal := rec.NewGoal(userprefix, userprefix, "Create all users")

	user_goal.AddTaskRecord(
		&rec.TaskRecord{
			Task:              userprefix,
			ReconcilerManager: mergefsRec.Manager,
			ReconcilerName:    createHomeTask.Name,
			KeyType:           rec.TaskRecord_PrefixSpace,
		},
	)

	go credRec.Run()
	defer credRec.Stop()
	credRec.WaitAllUntil(rec.ReconcilerOp_Idle)

	go mergefsRec.Run()
	defer mergefsRec.Stop()
	mergefsRec.WaitAllUntil(rec.ReconcilerOp_Idle)

	tf, err := user_goal.GetTaskForest(etcdCli, nil)
	a.Nil(err)

	// confirm at least the correct number of tasks.
	//
	// the tree is grouped by reconciler, so it looks like:
	// "/cred/createuserkeys" ==> "/users/murphy" ==> "/keys/murphy"
	// "/mergefs/userhome" ==> "/users/murphy"
	// "/mergefs/userkeys" ==> "/keys/murphy"
	//
	//

	t.Logf("%s\n", protojson.Format(tf))

	a.NotEqual(rec.TaskStatus_Error, tf.HighestStatus)
	a.Equal(rec.TaskStatus_Success, tf.HighestStatus)
	a.Len(tf.Subgoals, 2)
	a.Equal(int64(6), tf.NumChildTasks)
}

func TestSubTaskTreesSpecifiedReconcilerPrefixMissing(t *testing.T) {
	clearEtcd()
	defer clearEtcd()

	userprefix := "/users/"
	users := []string{"murphy", "olive"}
	keyprefix := "/keys/"

	createHomeTask := &rec.Reconciler{
		Prefix:  userprefix,
		Name:    "userhome",
		Actions: &UserHome{},
	}

	writeKeysTask := &rec.Reconciler{
		Prefix:  keyprefix,
		Name:    "userkeys",
		Desc:    "Place user keys in $HOME/.ssh",
		Actions: &WriteUserKeys{},
	}

	// createKeysTask := &rec.Reconciler{
	// 	Prefix:  userprefix,
	// 	Name:    "createuserkeys",
	// 	Desc:    "Create ssh creds",
	// 	Actions: &CreateKeys{},
	// }

	mergefsRec := &rec.ReconcilerManager{
		Manager:                    "mergefs",
		EtcdCli:                    etcdCli,
		Reconcilers:                []*rec.Reconciler{createHomeTask, writeKeysTask},
		ServerHeartbeatGracePeriod: -1,
	}

	// credRec := &rec.ReconcilerManager{
	// 	Manager:                    "cred",
	// 	EtcdCli:                    etcdCli,
	// 	Reconcilers:                []*rec.Reconciler{createKeysTask},
	// 	ServerHeartbeatGracePeriod: -1,
	// }

	a := assert.New(t)

	// write keys
	ops := new(storage.EtcdTransaction)

	for _, user := range users {
		ops.Write(storage.NewGenericEtcdKey(userprefix+user, []byte(user)))
	}

	_, _, err := ops.EtcdTx(etcdCli, "")
	if err != nil {
		t.Errorf("etcd tx: %v", err)
	}

	// create task goals. This is something that apiserver would do
	// as a top-level task creator.
	user_goal := rec.NewGoal(userprefix, userprefix, "Create all users")

	user_goal.AddTaskRecord(
		&rec.TaskRecord{
			Task:              userprefix,
			ReconcilerManager: mergefsRec.Manager,
			ReconcilerName:    createHomeTask.Name,
			KeyType:           rec.TaskRecord_PrefixSpace,
		},
	)

	tf, err := user_goal.GetTaskForest(etcdCli, nil)
	a.Nil(err)

	// confirm at least the correct number of tasks.
	//
	// the tree is grouped by reconciler, so it looks like:
	// "/cred/createuserkeys" ==> "/users/murphy" ==> "/keys/murphy"
	// "/mergefs/userhome" ==> "/users/murphy"
	// "/mergefs/userkeys" ==> "/keys/murphy"
	//
	//

	t.Logf("%s\n", protojson.Format(tf))

	a.NotEqual(rec.TaskStatus_Error, tf.HighestStatus)
	a.Equal(rec.TaskStatus_Pending, tf.HighestStatus)
	a.Len(tf.Subgoals, 1)
	a.Equal(int64(2), tf.NumChildTasks)
}
