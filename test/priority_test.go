/*
  This tests that the init/ensure low priority system actually works
*/

package test

import (
	//"context"
	"encoding/binary"
	"fmt"
	"strings"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	rec "gitlab.com/mergetb/tech/reconcile"
	storage "gitlab.com/mergetb/tech/shared/storage/etcd"
)

var priorityPrefix = "/prioritytest"
var priorityConcurrent uint32 = 3
var priorityDelayMS uint32 = 100
var priorityUseDifferentDeleteMS bool = true
var priorityDeleteDelayMS uint32 = 0
var priorityResults = make(chan *PriorityTest, priorityConcurrent)

type PriorityTest struct {
	Key     string
	Time    uint32
	Version int64
}

func (pt *PriorityTest) Handle(event string, prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	if value == nil {
		value = prev
	}

	if value == nil {
		priorityResults <- nil
		log.Infof("%s: %s no sleep", event, r.TaskValue.EtcdKey)
		return nil
	}

	new_pt := &PriorityTest{
		Key:     r.TaskValue.EtcdKey,
		Time:    binary.BigEndian.Uint32(value),
		Version: version,
	}

	if priorityUseDifferentDeleteMS && event == "delete" {
		new_pt.Time = priorityDeleteDelayMS
	}

	log.Infof("%s: %s sleeping for: %d ms", event, new_pt.Key, new_pt.Time)
	time.Sleep(time.Duration(new_pt.Time) * time.Millisecond)

	priorityResults <- new_pt
	log.Infof("  %s: %s done!", event, new_pt.Key)

	return nil
}

func (at *PriorityTest) Equals(o *PriorityTest) bool {
	if at == nil && o == nil {
		return true
	}

	// both cannot be nil now, so if 1 is nil, the other isn't
	if at == nil || o == nil {
		return false
	}

	return at.Key == o.Key && at.Time == o.Time && at.Version == o.Version

}

func (pt *PriorityTest) Parse(k string) bool {
	return strings.HasPrefix(k, priorityPrefix)
}

func (pt *PriorityTest) Create(value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return pt.Handle("create", nil, value, version, r)
}

func (pt *PriorityTest) Update(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return pt.Handle("update", prev, value, version, r)
}

func (pt *PriorityTest) Ensure(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return pt.Handle("ensure", prev, value, version, r)
}

func (pt *PriorityTest) Delete(prev []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return pt.Handle("delete", prev, nil, version, r)
}

func TestPriorityInitPutInterrupt(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	priorityResults = make(chan *PriorityTest, priorityConcurrent)
	priorityUseDifferentDeleteMS = true
	defer close(priorityResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  priorityPrefix + "/",
			Name:    "priorityinit",
			Actions: &PriorityTest{},
		}},
	}

	var expectedResults []*PriorityTest
	var ops storage.EtcdTransaction

	for i := uint32(0); i < priorityConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)
		v := priorityDelayMS * 10

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		if i == 0 {
			expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 1})
		}
	}

	_, _, err := ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	go tm.Run()
	defer tm.Stop()

	ops = storage.EtcdTransaction{}

	for i := uint32(0); i < priorityConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)
		v := priorityDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 2})
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Create, rec.ReconcilerOp_Ensure, rec.ReconcilerOp_Delete)
	_, _, err = ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	for _, expected := range expectedResults {
		res := <-priorityResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	assert.Equal(t, 0, len(priorityResults), "extra elements in priorityResults")
}

func TestPriorityInitDeleteInterrupt(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	priorityResults = make(chan *PriorityTest, priorityConcurrent)
	priorityUseDifferentDeleteMS = true
	defer close(priorityResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  priorityPrefix + "/",
			Name:    "priorityinit",
			Actions: &PriorityTest{},
		}},
	}

	var expectedResults []*PriorityTest
	var ops storage.EtcdTransaction

	for i := uint32(0); i < priorityConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)
		v := priorityDelayMS * 10

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		if i == 0 {
			expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 1})
		}
	}

	_, _, err := ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	go tm.Run()
	defer tm.Stop()

	ops = storage.EtcdTransaction{}

	for i := uint32(0); i < priorityConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)
		v := priorityDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.DeleteKey(k)
		if i == 0 {
			expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: 0, Version: 1})
		}
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Create, rec.ReconcilerOp_Ensure, rec.ReconcilerOp_Delete)
	_, _, err = ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	for _, expected := range expectedResults {
		res := <-priorityResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	assert.Equal(t, 0, len(priorityResults), "extra elements in priorityResults")
}

func TestPriorityInitDeleteInitInterrupt(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	priorityResults = make(chan *PriorityTest, priorityConcurrent)
	priorityUseDifferentDeleteMS = true
	defer close(priorityResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  priorityPrefix + "/",
			Name:    "priorityinit",
			Actions: &PriorityTest{},
		}},
	}

	var expectedResults []*PriorityTest
	var ops storage.EtcdTransaction

	for i := uint32(0); i < priorityConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)
		v := priorityDelayMS * 10

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 1})
	}

	_, _, err := ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	go tm.Run()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	tm.Stop()

	ops = storage.EtcdTransaction{}

	for i := uint32(0); i < priorityConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)

		ops.DeleteKey(k)
		if i == 0 {
			expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: 0, Version: 1})
		}
	}

	_, _, err = ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	ops = storage.EtcdTransaction{}

	for i := uint32(0); i < priorityConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)
		v := priorityDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		if i == 0 {
			expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 1})
		}
	}

	_, _, err = ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	go tm.Run()
	defer tm.Stop()

	ops = storage.EtcdTransaction{}

	for i := uint32(0); i < priorityConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)
		v := priorityDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		if i != 0 {
			expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: 0, Version: 1})
		}
		expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 2})
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Create, rec.ReconcilerOp_Ensure, rec.ReconcilerOp_Delete)
	_, _, err = ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	for _, expected := range expectedResults {
		res := <-priorityResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	assert.Equal(t, 0, len(priorityResults), "extra elements in priorityResults")
}

func TestPriorityInitInitDeletePut(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	priorityResults = make(chan *PriorityTest, 4*priorityConcurrent)
	priorityUseDifferentDeleteMS = true
	defer close(priorityResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  priorityPrefix + "/",
			Name:    "priorityinit",
			Actions: &PriorityTest{},
		}},
	}

	var expectedResults []*PriorityTest
	var ops storage.EtcdTransaction

	for i := uint32(0); i < priorityConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)
		v := priorityDelayMS * 10

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 1})
	}

	for i := uint32(0); i < 1; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)
		v := priorityDelayMS * 10

		expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 1})
	}

	_, _, err := ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	go tm.Run()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	tm.Stop()

	log.Infof("stopped")

	go tm.Run()
	defer tm.Stop()

	ops = storage.EtcdTransaction{}

	for i := uint32(0); i < priorityConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)

		ops.DeleteKey(k)
		expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: 0, Version: 1})
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Ensure)

	_, _, err = ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	time.Sleep(time.Second)

	ops = storage.EtcdTransaction{}

	for i := uint32(0); i < priorityConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)
		v := priorityDelayMS * 10

		log.Infof("%s/%d", k, v)

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 1})
	}

	_, _, err = ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	for _, expected := range expectedResults {
		res := <-priorityResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	assert.Equal(t, 0, len(priorityResults), "extra elements in priorityResults")
}

func TestPriorityDeleteInitInterrupt(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	priorityResults = make(chan *PriorityTest, priorityConcurrent)
	priorityUseDifferentDeleteMS = false
	defer close(priorityResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  priorityPrefix + "/",
			Name:    "priorityinit",
			Actions: &PriorityTest{},
		}},
	}

	var expectedResults []*PriorityTest
	var ops storage.EtcdTransaction

	for i := uint32(0); i < priorityConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)
		v := priorityDelayMS * 10

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 1})
	}

	_, _, err := ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	go tm.Run()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)
	tm.Stop()

	ops = storage.EtcdTransaction{}

	for i := uint32(0); i < priorityConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)
		v := priorityDelayMS * 10

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.DeleteKey(k)
		if i == 0 {
			expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 1})
		}
	}

	_, _, err = ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	ops = storage.EtcdTransaction{}

	for i := uint32(0); i < priorityConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)
		v := priorityDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		if i != 0 {
			expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v * 10, Version: 1})
		}
		expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 1})
	}

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Create, rec.ReconcilerOp_Ensure, rec.ReconcilerOp_Delete)
	_, _, err = ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	for _, expected := range expectedResults {
		res := <-priorityResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	assert.Equal(t, 0, len(priorityResults), "extra elements in priorityResults")
}

func TestPriorityEnsureInterrupt(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	priorityResults = make(chan *PriorityTest, priorityConcurrent)
	priorityUseDifferentDeleteMS = true
	defer close(priorityResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:          priorityPrefix + "/",
			Name:            "priorityinit",
			Actions:         &PriorityTest{},
			EnsureFrequency: 1100 * time.Millisecond,
		}},
	}

	var expectedResults []*PriorityTest
	var ops storage.EtcdTransaction

	for i := uint32(0); i < priorityConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)
		v := priorityDelayMS * 10

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)

		expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 1})
	}

	for i := uint32(0); i < 1; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)
		v := priorityDelayMS * 10

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 1})

		// yeah i know i could like replace all the i's with 0's, but whatever
		break
	}

	_, _, err := ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)
	tm.WaitAllUntil(rec.ReconcilerOp_Ensure)

	ops = storage.EtcdTransaction{}

	for i := uint32(0); i < priorityConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)
		v := priorityDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 2})
	}

	_, _, err = ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	for _, expected := range expectedResults {
		res := <-priorityResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	assert.Equal(t, 0, len(priorityResults), "extra elements in priorityResults")
}

func TestPriorityEnsureDeleteInterrupt(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	priorityResults = make(chan *PriorityTest, priorityConcurrent)
	priorityUseDifferentDeleteMS = true
	defer close(priorityResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:          priorityPrefix + "/",
			Name:            "priorityinit",
			Actions:         &PriorityTest{},
			EnsureFrequency: 1100 * time.Millisecond,
		}},
	}

	var expectedResults []*PriorityTest
	var ops storage.EtcdTransaction

	for i := uint32(0); i < priorityConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)
		v := priorityDelayMS * 10

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)

		expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 1})
	}

	for i := uint32(0); i < 1; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)
		v := priorityDelayMS * 10

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 1})

		// yeah i know i could like replace all the i's with 0's, but whatever
		break
	}

	_, _, err := ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)
	tm.WaitAllUntil(rec.ReconcilerOp_Ensure)

	ops = storage.EtcdTransaction{}

	for i := uint32(0); i < priorityConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", priorityPrefix, i)
		v := priorityDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.DeleteKey(k)
		expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: 0, Version: 1})
	}

	_, _, err = ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	for _, expected := range expectedResults {
		res := <-priorityResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	assert.Equal(t, 0, len(priorityResults), "extra elements in priorityResults")
}

func TestPriorityInitMulti(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	priorityResults = make(chan *PriorityTest, priorityConcurrent)
	priorityUseDifferentDeleteMS = true
	defer close(priorityResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Reconcilers: []*rec.Reconciler{
			{
				Prefix:  priorityPrefix + "/a/",
				Name:    "priorityinita",
				Actions: &PriorityTest{},
			},
			{
				Prefix:  priorityPrefix + "/b/",
				Name:    "priorityinitb",
				Actions: &PriorityTest{},
			},
			{
				Prefix:  priorityPrefix + "/c/",
				Name:    "priorityinitc",
				Actions: &PriorityTest{},
			},
		},
	}

	var expectedResults []*PriorityTest
	var ops storage.EtcdTransaction

	for _, r := range tm.Reconcilers {
		for i := uint32(0); i < priorityConcurrent; i++ {
			k := fmt.Sprintf("%s%d", r.Prefix, i)
			v := uint32(0)

			bs := make([]byte, 4)
			binary.BigEndian.PutUint32(bs, v)

			ops.WriteKey(k, bs)
			expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 1})
		}
	}

	_, _, err := ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	go tm.Run()
	defer tm.Stop()

	for _, expected := range expectedResults {
		res := <-priorityResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	assert.Equal(t, 0, len(priorityResults), "extra elements in priorityResults")
}

func TestPriorityPutMulti(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	priorityResults = make(chan *PriorityTest, priorityConcurrent)
	priorityUseDifferentDeleteMS = true
	defer close(priorityResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Reconcilers: []*rec.Reconciler{
			{
				Prefix:  priorityPrefix + "/a/",
				Name:    "priorityinita",
				Actions: &PriorityTest{},
			},
			{
				Prefix:  priorityPrefix + "/b/",
				Name:    "priorityinitb",
				Actions: &PriorityTest{},
			},
			{
				Prefix:  priorityPrefix + "/c/",
				Name:    "priorityinitc",
				Actions: &PriorityTest{},
			},
		},
	}

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Init)

	var expectedResults []*PriorityTest
	var ops storage.EtcdTransaction

	k := fmt.Sprintf("%sINIT", tm.Reconcilers[0].Prefix)
	v := priorityDelayMS * 2

	bs := make([]byte, 4)
	binary.BigEndian.PutUint32(bs, v)

	ops.WriteKey(k, bs)
	expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 1})

	_, _, err := ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	time.Sleep(time.Millisecond * 1 * time.Duration(priorityDelayMS))

	ops = storage.EtcdTransaction{}

	for _, r := range tm.Reconcilers {
		for i := uint32(0); i < priorityConcurrent; i++ {
			k := fmt.Sprintf("%s%d", r.Prefix, i)
			v := uint32(0)

			bs := make([]byte, 4)
			binary.BigEndian.PutUint32(bs, v)

			ops.WriteKey(k, bs)
			expectedResults = append(expectedResults, &PriorityTest{Key: k, Time: v, Version: 1})
		}
	}

	_, _, err = ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	for _, expected := range expectedResults {
		res := <-priorityResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	assert.Equal(t, 0, len(priorityResults), "extra elements in priorityResults")
}
