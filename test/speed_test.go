package test

import (
	"fmt"
	"strings"
	"sync/atomic"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	rec "gitlab.com/mergetb/tech/reconcile"
	storage "gitlab.com/mergetb/tech/shared/storage/etcd"
)

var speedTestKeys = 10000

var speedPrefix = "/speedtest"
var results atomic.Uint32

type SpeedTest struct {
}

func (st *SpeedTest) Parse(k string) bool {
	return strings.HasPrefix(k, speedPrefix)
}

func (st *SpeedTest) Handle() {
	x := results.Add(1)
	log.Infof("progress: %d/%d", x, speedTestKeys)
}

func (st *SpeedTest) Create(value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	st.Handle()
	return nil
}

func (st *SpeedTest) Update(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	st.Handle()
	return nil
}

func (st *SpeedTest) Ensure(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	st.Handle()
	return nil
}

func (st *SpeedTest) Delete(prev []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	st.Handle()
	return nil
}

func TestSpeedInitAsync(t *testing.T) {

	results.Store(0)
	clearEtcd()
	defer clearEtcd()

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  speedPrefix + "/",
			Name:    "speedinit",
			Actions: &SpeedTest{},
		}},
	}

	var ops storage.EtcdTransaction

	log.Infof("writing keys...")
	start := time.Now()
	for i := 0; i < speedTestKeys; i++ {
		k := fmt.Sprintf("%s/%05d", speedPrefix, i)
		v := fmt.Sprint(i)

		ops.WriteKey(k, []byte(v))
	}

	_, _, err := ops.EtcdTx(etcdCli, "")
	a.Nilf(err, "err %+v", err)

	log.Infof("writing %d keys took: %s", speedTestKeys, time.Since(start))

	start = time.Now()
	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)
	duration := time.Since(start)

	log.Infof("processing %d keys took: %s", speedTestKeys, duration)

	a.EqualValues(results.Load(), speedTestKeys)

}

func TestSpeedInitSync(t *testing.T) {

	results.Store(0)
	clearEtcd()
	defer clearEtcd()

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  speedPrefix + "/",
			Name:    "speedinit",
			Actions: &SpeedTest{},
		}},
	}

	var ops storage.EtcdTransaction

	log.Infof("writing %d keys...", speedTestKeys)
	start := time.Now()
	for i := 0; i < speedTestKeys; i++ {
		k := fmt.Sprintf("%s/%05d", speedPrefix, i)
		v := fmt.Sprint(i)

		ops.WriteKey(k, []byte(v))
	}

	_, _, err := ops.EtcdTx(etcdCli, "")
	a.Nilf(err, "err %+v", err)

	log.Infof("writing %d keys took: %s", speedTestKeys, time.Since(start))

	start = time.Now()
	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)
	duration := time.Since(start)

	log.Infof("processing %d keys took: %s", speedTestKeys, duration)

	a.EqualValues(results.Load(), speedTestKeys)
}
