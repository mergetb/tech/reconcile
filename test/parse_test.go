/*
  This tests that Parse can write things to shared structures and read them back
*/

package test

import (
	"encoding/binary"
	"fmt"
	"strings"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	rec "gitlab.com/mergetb/tech/reconcile"
	storage "gitlab.com/mergetb/tech/shared/storage/etcd"
)

var parsePrefix = "/parsetest"
var parseDelayMS uint32 = 100
var parseAmount uint32 = 8
var parseDeleteDelayMS uint32 = 0
var parseReturn = true
var parseResults = make(chan *ParseTest, parseAmount)

type ParseTest struct {
	oldKey  string
	key     string
	time    uint32
	version int64
}

func (pt *ParseTest) Handle(event string, prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	if value == nil {
		value = prev
	}

	if value == nil {
		parseResults <- nil
		log.Infof("%s: %s no sleep", event, r.TaskValue.EtcdKey)
		return nil
	}

	new_pt := &ParseTest{
		key:     r.TaskValue.EtcdKey,
		time:    binary.BigEndian.Uint32(value),
		version: version,
	}

	if event == "delete" {
		new_pt.time = parseDeleteDelayMS
	}

	var err error

	if pt.oldKey != r.TaskValue.EtcdKey {
		new_pt.oldKey = "bad"
		err = fmt.Errorf("parse failed: %s != %s", pt.oldKey, r.TaskValue.EtcdKey)
	}

	log.Infof("%s: %s sleeping for: %d ms", event, new_pt.key, new_pt.time)
	time.Sleep(time.Duration(new_pt.time) * time.Millisecond)

	parseResults <- new_pt
	log.Infof("  %s: %s done!", event, new_pt.key)

	return rec.CheckErrorToMessage(err)
}

func (pt *ParseTest) Equals(o *ParseTest) bool {
	if pt == nil && o == nil {
		return true
	}

	// both cannot be nil now, so if 1 is nil, the other isn't
	if pt == nil || o == nil {
		return false
	}

	return pt.key == o.key &&
		pt.oldKey == o.oldKey &&
		pt.time == o.time &&
		pt.version == o.version

}

func (pt *ParseTest) Parse(k string) bool {
	pt.oldKey = k
	return strings.HasPrefix(k, parsePrefix) && parseReturn
}

func (pt *ParseTest) Create(value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return pt.Handle("create", nil, value, version, r)
}

func (pt *ParseTest) Update(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return pt.Handle("update", prev, value, version, r)
}

func (pt *ParseTest) Ensure(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return pt.Handle("ensure", prev, value, version, r)
}

func (pt *ParseTest) Delete(prev []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return pt.Handle("delete", prev, nil, version, r)
}

func TestParseInit(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	parseResults = make(chan *ParseTest, parseAmount)
	defer close(parseResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  parsePrefix + "/",
			Name:    "parseinit",
			Actions: &ParseTest{},
		}},
	}

	var expectedResults []*ParseTest
	var ops storage.EtcdTransaction

	for i := uint32(0); i < uint32(parseAmount); i++ {
		k := fmt.Sprintf("%s/%d", parsePrefix, i)
		v := asyncDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &ParseTest{key: k, time: v, version: 1})
	}

	_, _, err := ops.EtcdTx(etcdCli, "")

	a.Nil(err)

	go tm.Run()
	defer tm.Stop()

	for _, expected := range expectedResults {
		res := <-parseResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	assert.Equal(t, 0, len(parseResults), "extra elements in parseResults")
}

func TestParseInitStopEnsure(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	parseResults = make(chan *ParseTest, parseAmount)
	defer close(parseResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  parsePrefix + "/",
			Name:    "parseinit",
			Actions: &ParseTest{},
		}},
	}

	var expectedResults []*ParseTest
	var ops storage.EtcdTransaction

	for i := uint32(0); i < uint32(parseAmount); i++ {
		k := fmt.Sprintf("%s/%d", parsePrefix, i)
		v := asyncDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &ParseTest{key: k, time: v, version: 1})
	}

	_, _, err := ops.EtcdTx(etcdCli, "")

	a.Nil(err)

	go tm.Run()

	for _, expected := range expectedResults {
		res := <-parseResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	assert.Equal(t, 0, len(parseResults), "extra elements in parseResults")

	tm.Stop()
	go tm.Run()
	defer tm.Stop()

	for _, expected := range expectedResults {
		res := <-parseResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	assert.Equal(t, 0, len(parseResults), "extra elements in parseResults")

}

func TestParsePut(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	parseResults = make(chan *ParseTest, parseAmount)
	defer close(parseResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  parsePrefix + "/",
			Name:    "parseinit",
			Actions: &ParseTest{},
		}},
	}

	var expectedResults []*ParseTest
	var ops storage.EtcdTransaction

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := uint32(0); i < uint32(parseAmount); i++ {
		k := fmt.Sprintf("%s/%d", parsePrefix, i)
		v := asyncDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &ParseTest{key: k, time: v, version: 1})
	}

	_, _, err := ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	for _, expected := range expectedResults {
		res := <-parseResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	assert.Equal(t, 0, len(parseResults), "extra elements in parseResults")
}

func TestParseAsyncPut(t *testing.T) {
	parseAmount := parseAmount * 8

	clearEtcd()
	defer clearEtcd()
	parseResults = make(chan *ParseTest, parseAmount)
	defer close(parseResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    int(parseAmount),
		Reconcilers: []*rec.Reconciler{{
			Prefix:  parsePrefix + "/",
			Name:    "parseinit",
			Actions: &ParseTest{},
		}},
	}

	var ops storage.EtcdTransaction

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := uint32(0); i < uint32(parseAmount); i++ {
		k := fmt.Sprintf("%s/%d", parsePrefix, i)
		v := asyncDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
	}

	_, _, err := ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	foundbad := false

	for i := uint32(0); i < uint32(parseAmount); i++ {
		res := <-parseResults

		if res.oldKey == "bad" {
			foundbad = true
		}
	}

	assert.Falsef(t, foundbad, "we found a bad value")

	assert.Equal(t, 0, len(parseResults), "extra elements in parseResults")
}

func TestParseAsyncPutDelete(t *testing.T) {
	parseAmount := parseAmount * 8

	clearEtcd()
	defer clearEtcd()
	parseResults = make(chan *ParseTest, parseAmount)
	defer close(parseResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    int(parseAmount),
		Reconcilers: []*rec.Reconciler{{
			Prefix:  parsePrefix + "/",
			Name:    "parseinit",
			Actions: &ParseTest{},
		}},
	}

	var ops storage.EtcdTransaction

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := uint32(0); i < uint32(parseAmount); i++ {
		k := fmt.Sprintf("%s/%d", parsePrefix, i)
		v := asyncDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
	}

	_, _, err := ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	foundbad := false

	for i := uint32(0); i < uint32(parseAmount); i++ {
		res := <-parseResults

		if res.oldKey == "bad" {
			foundbad = true
		}
	}

	ops = storage.EtcdTransaction{}

	for i := uint32(0); i < uint32(parseAmount); i++ {
		k := fmt.Sprintf("%s/%d", parsePrefix, i)

		ops.DeleteKey(k)
	}

	_, _, err = ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	for i := uint32(0); i < uint32(parseAmount); i++ {
		res := <-parseResults

		if res.oldKey == "bad" {
			foundbad = true
		}
	}

	assert.Falsef(t, foundbad, "we found a bad value")

	assert.Equal(t, 0, len(parseResults), "extra elements in parseResults")
}

func TestParseAsyncPutInit(t *testing.T) {
	parseAmount := parseAmount * 8

	clearEtcd()
	defer clearEtcd()
	parseResults = make(chan *ParseTest, parseAmount)
	defer close(parseResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    int(parseAmount),
		Reconcilers: []*rec.Reconciler{{
			Prefix:  parsePrefix + "/",
			Name:    "parseinit",
			Actions: &ParseTest{},
		}},
	}

	var ops storage.EtcdTransaction

	for i := uint32(0); i < uint32(parseAmount); i++ {
		k := fmt.Sprintf("%s/%d", parsePrefix, i)
		v := asyncDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
	}

	_, _, err := ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	foundbad := false

	for i := uint32(0); i < uint32(parseAmount); i++ {
		res := <-parseResults

		if res.oldKey == "bad" {
			foundbad = true
		}
	}

	assert.Falsef(t, foundbad, "we found a bad value")

	assert.Equal(t, 0, len(parseResults), "extra elements in parseResults")
}

func TestParseAsyncInitDelete(t *testing.T) {
	parseAmount := parseAmount * 8

	clearEtcd()
	defer clearEtcd()
	parseResults = make(chan *ParseTest, parseAmount)
	defer close(parseResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    int(parseAmount),
		Reconcilers: []*rec.Reconciler{{
			Prefix:  parsePrefix + "/",
			Name:    "parseinit",
			Actions: &ParseTest{},
		}},
	}

	var ops storage.EtcdTransaction

	go tm.Run()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := uint32(0); i < uint32(parseAmount); i++ {
		k := fmt.Sprintf("%s/%d", parsePrefix, i)
		v := asyncDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
	}

	_, _, err := ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	foundbad := false

	for i := uint32(0); i < uint32(parseAmount); i++ {
		res := <-parseResults

		if res == nil {
			foundbad = true
		} else if res.oldKey == "bad" {
			foundbad = true
		}
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)
	tm.Stop()

	ops = storage.EtcdTransaction{}

	for i := uint32(0); i < uint32(parseAmount); i++ {
		k := fmt.Sprintf("%s/%d", parsePrefix, i)

		ops.DeleteKey(k)
	}

	_, _, err = ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	go tm.Run()
	defer tm.Stop()

	for i := uint32(0); i < uint32(parseAmount); i++ {
		res := <-parseResults

		if res == nil {
			foundbad = true
		} else if res.oldKey == "bad" {
			foundbad = true
		}
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	assert.Falsef(t, foundbad, "we found a bad value")

	assert.Equal(t, 0, len(parseResults), "extra elements in parseResults")
}

func TestParseAsyncPollcheckChange(t *testing.T) {
	parseAmount := parseAmount * 8

	clearEtcd()
	defer clearEtcd()
	parseResults = make(chan *ParseTest, parseAmount)
	defer close(parseResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    int(parseAmount),
		Reconcilers: []*rec.Reconciler{{
			Prefix:  parsePrefix + "/",
			Name:    "changeparse",
			Actions: &ParseTest{},
		}},
	}

	var ops storage.EtcdTransaction

	go tm.Run()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := uint32(0); i < uint32(parseAmount); i++ {
		k := fmt.Sprintf("%s/%d", parsePrefix, i)
		v := asyncDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
	}

	_, _, err := ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	foundbad := false

	for i := uint32(0); i < uint32(parseAmount); i++ {
		res := <-parseResults

		if res == nil {
			foundbad = true
		} else if res.oldKey == "bad" {
			foundbad = true
		}
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)
	tm.Stop()

	ops = storage.EtcdTransaction{}

	for i := uint32(0); i < uint32(parseAmount); i++ {
		k := fmt.Sprintf("%s/%d", parsePrefix, i)

		ops.DeleteKey(k)
	}

	_, _, err = ops.EtcdTx(etcdCli, "")
	a.Nil(err)

	parseReturn = false
	defer func() {
		parseReturn = true
	}()

	tm.Reconcilers[0].PollCheckFrequency = time.Millisecond

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Pollcheck)
	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	assert.Falsef(t, foundbad, "we found a bad value")

	assert.Equal(t, 0, len(parseResults), "extra elements in parseResults")
}
