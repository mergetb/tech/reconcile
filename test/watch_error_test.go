package test

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"sync/atomic"
	"testing"
	"time"

	sets "github.com/deckarep/golang-set/v2"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	rec "gitlab.com/mergetb/tech/reconcile"
)

var watchPrefixError = "/watchtesterror"
var watchResultsError chan int = nil
var watchtestTimesError = 100
var returnError = new(atomic.Bool)

type WatchTestError struct {
	Key     string
	Time    uint32
	Version int64
}

func (wt *WatchTestError) Handle(event string, prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	if value == nil {
		value = prev
	}

	if value == nil {
		return rec.TaskMessageErrorf("error")
	}

	log.Infof("found %s -> %s: %s", event, r.TaskValue.EtcdKey, value)
	i, err := strconv.Atoi(string(value))
	if err != nil {
		i = -1
	}

	if returnError.Load() {
		// use -i to indicate that we had an error
		watchResults <- -i
		return rec.TaskMessageErrorf("error %d", i)
	}

	watchResults <- i
	return nil
}

func (wt *WatchTestError) Parse(k string) bool {
	return strings.HasPrefix(k, watchPrefixError)
}

func (wt *WatchTestError) Create(value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return wt.Handle("create", nil, value, version, r)
}

func (wt *WatchTestError) Update(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return wt.Handle("update", prev, value, version, r)
}

func (wt *WatchTestError) Ensure(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return wt.Handle("ensure", prev, value, version, r)
}

func (wt *WatchTestError) Delete(prev []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return wt.Handle("delete", prev, nil, version, r)
}

func TestWatchErrorInit(t *testing.T) {

	a := assert.New(t)

	clearEtcd()
	defer clearEtcd()
	watchResults = make(chan int, 1)
	defer close(watchResults)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  watchPrefixError + "/",
			Name:    "watchinit",
			Actions: &WatchTestError{},
		}},
	}

	// put -- error
	returnError.Store(true)

	for i := 0; i < watchtestTimesError; i++ {
		k := fmt.Sprintf("%s/%02d", watchPrefixError, i)
		v := fmt.Sprint(i)

		_, err := etcdCli.Put(context.Background(), k, v)
		a.Nilf(err, "err %+v", err)
	}

	go tm.Run()
	defer tm.Stop()

	expected := sets.NewSet[int]()
	found := sets.NewSet[int]()

	for i := 0; i < watchtestTimesError; i++ {
		expected.Add(-i)

		f := <-watchResults
		a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
		found.Add(f)
	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")
}

func TestWatchErrorWatch(t *testing.T) {

	a := assert.New(t)

	clearEtcd()
	defer clearEtcd()
	watchResults = make(chan int, 1)
	defer close(watchResults)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  watchPrefixError + "/",
			Name:    "watchinit",
			Actions: &WatchTestError{},
		}},
	}

	go tm.Run()
	defer tm.Stop()

	// put -- error
	returnError.Store(true)
	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := 0; i < watchtestTimesError; i++ {
		k := fmt.Sprintf("%s/%02d", watchPrefixError, i)
		v := fmt.Sprint(i)

		_, err := etcdCli.Put(context.Background(), k, v)
		a.Nilf(err, "err %+v", err)
	}

	expected := sets.NewSet[int]()
	found := sets.NewSet[int]()

	for i := 0; i < watchtestTimesError; i++ {
		expected.Add(-i)

		f := <-watchResults
		a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
		found.Add(f)
	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")
}

func TestWatchErrorThenSuccess(t *testing.T) {

	a := assert.New(t)

	clearEtcd()
	defer clearEtcd()
	watchResults = make(chan int, 1)
	defer close(watchResults)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:          watchPrefixError + "/",
			Name:            "watchinit",
			Actions:         &WatchTestError{},
			EnsureFrequency: 2 * time.Second,
		}},
	}

	go tm.Run()
	defer tm.Stop()

	// put -- error
	returnError.Store(true)

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := 0; i < watchtestTimesError; i++ {
		k := fmt.Sprintf("%s/%02d", watchPrefixError, i)
		v := fmt.Sprint(i)

		_, err := etcdCli.Put(context.Background(), k, v)
		a.Nilf(err, "err %+v", err)
	}

	expected := sets.NewSet[int]()
	found := sets.NewSet[int]()

	for i := 0; i < watchtestTimesError; i++ {
		expected.Add(-i)

		f := <-watchResults
		a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
		found.Add(f)
	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")

	expected = sets.NewSet[int]()
	found = sets.NewSet[int]()

	// ensure -- success
	returnError.Store(false)
	tm.WaitAllUntil(rec.ReconcilerOp_Ensure)

	for i := 0; i < watchtestTimesError; i++ {
		expected.Add(i)

		f := <-watchResults
		a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
		found.Add(f)
	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")
}

func TestWatchDeleteErrorThenSuccess(t *testing.T) {

	a := assert.New(t)

	clearEtcd()
	defer clearEtcd()
	watchResults = make(chan int, 1)
	defer close(watchResults)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:          watchPrefixError + "/",
			Name:            "watchinit",
			Actions:         &WatchTestError{},
			EnsureFrequency: 2 * time.Second,
		}},
	}

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	// put -- success
	returnError.Store(false)

	for i := 0; i < watchtestTimesError; i++ {
		k := fmt.Sprintf("%s/%02d", watchPrefixError, i)
		v := fmt.Sprint(i)

		_, err := etcdCli.Put(context.Background(), k, v)
		a.Nilf(err, "err %+v", err)
	}

	expected := sets.NewSet[int]()
	found := sets.NewSet[int]()

	for i := 0; i < watchtestTimesError; i++ {
		expected.Add(i)

		f := <-watchResults
		a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
		found.Add(f)
	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")

	// delete -- error
	returnError.Store(true)

	for i := 0; i < watchtestTimesError; i++ {
		k := fmt.Sprintf("%s/%02d", watchPrefixError, i)

		_, err := etcdCli.Delete(context.Background(), k)
		a.Nilf(err, "err %+v", err)
	}

	expected = sets.NewSet[int]()
	found = sets.NewSet[int]()

	for i := 0; i < watchtestTimesError; i++ {
		expected.Add(-i)

		f := <-watchResults
		a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
		found.Add(f)
	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")

	// ensure - error, should be delete
	returnError.Store(true)
	tm.WaitAllUntil(rec.ReconcilerOp_Delete)

	expected = sets.NewSet[int]()
	found = sets.NewSet[int]()

	for i := 0; i < watchtestTimesError; i++ {
		expected.Add(-i)

		f := <-watchResults
		a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
		found.Add(f)
	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")

	// ensure - success
	returnError.Store(false)
	tm.WaitAllUntil(rec.ReconcilerOp_Delete)

	expected = sets.NewSet[int]()
	found = sets.NewSet[int]()

	for i := 0; i < watchtestTimesError; i++ {
		expected.Add(i)

		f := <-watchResults
		a.Falsef(found.Contains(f), "somehow, duplicate in found: %+v", f)
		found.Add(f)
	}

	a.Truef(expected.Equal(found),
		"expected and found are not equal: missing: %+v, extra: %+v",
		expected.Difference(found),
		found.Difference(expected),
	)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")

	// make sure that after we delete, no more ensures
	time.Sleep(4 * time.Second)
	assert.Equal(t, 0, len(watchResults), "extra elements in watchResults")
}
