/*
	This file test async functionality,
	mostly that things:
	 - execute AND
	 - execute in the correct order,
	enforced via sleep
*/

package test

import (
	"context"
	"encoding/binary"
	"fmt"
	"strings"
	"testing"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	rec "gitlab.com/mergetb/tech/reconcile"
	storage "gitlab.com/mergetb/tech/shared/storage/etcd"
)

var asyncPrefix = "/asynctest"
var asyncConcurrent uint32 = 8
var asyncDelayMS uint32 = 100
var asyncDeleteDelayMS uint32 = 0
var asyncResults = make(chan *AsyncTest, asyncConcurrent)

type AsyncTest struct {
	Key     string
	Time    uint32
	Version int64
}

func (at *AsyncTest) Handle(event string, prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	if value == nil {
		value = prev
	}

	if value == nil {
		asyncResults <- nil
		log.Infof("%s: %s no sleep @ %d", event, r.TaskValue.EtcdKey, r.TaskValue.ModRevision)
		return nil
	}

	new_at := &AsyncTest{
		Key:     r.TaskValue.EtcdKey,
		Time:    binary.BigEndian.Uint32(value),
		Version: version,
	}

	if event == "delete" {
		new_at.Time = asyncDeleteDelayMS
	}

	log.Infof("%s: %s sleeping for: %d ms @ %d", event, new_at.Key, new_at.Time, r.TaskValue.ModRevision)
	time.Sleep(time.Duration(new_at.Time) * time.Millisecond)

	asyncResults <- new_at
	log.Infof("  %s: %s done @ %d!", event, new_at.Key, r.TaskValue.ModRevision)

	return nil
}

func (at *AsyncTest) Equals(o *AsyncTest) bool {
	if at == nil && o == nil {
		return true
	}

	// both cannot be nil now, so if 1 is nil, the other isn't
	if at == nil || o == nil {
		return false
	}

	return at.Key == o.Key && at.Time == o.Time && at.Version == o.Version

}

func (at *AsyncTest) Parse(k string) bool {
	return strings.HasPrefix(k, asyncPrefix)
}

func (at *AsyncTest) Create(value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return at.Handle("create", nil, value, version, r)
}

func (at *AsyncTest) Update(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return at.Handle("update", prev, value, version, r)
}

func (at *AsyncTest) Ensure(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return at.Handle("ensure", prev, value, version, r)
}

func (at *AsyncTest) Delete(prev []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return at.Handle("delete", prev, nil, version, r)
}

func TestAsyncInit(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	asyncResults = make(chan *AsyncTest, asyncConcurrent)
	defer close(asyncResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    int(asyncConcurrent),
		Reconcilers: []*rec.Reconciler{{
			Prefix:  asyncPrefix + "/",
			Name:    "asyncinit",
			Actions: &AsyncTest{},
		}},
	}

	var expectedResults []*AsyncTest
	var ops storage.EtcdTransaction

	for i := uint32(0); i < asyncConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", asyncPrefix, i)
		v := i * asyncDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &AsyncTest{Key: k, Time: v, Version: 1})
	}

	_, _, err := ops.EtcdTx(etcdCli, "")

	a.Nil(err)

	go tm.Run()
	defer tm.Stop()

	for _, expected := range expectedResults {
		res := <-asyncResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	assert.Equal(t, 0, len(asyncResults), "extra elements in asyncResults")
}

func TestAsyncPut(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	asyncResults = make(chan *AsyncTest, asyncConcurrent)
	defer close(asyncResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    int(asyncConcurrent),
		Reconcilers: []*rec.Reconciler{{
			Prefix:  asyncPrefix + "/",
			Name:    "asyncput",
			Actions: &AsyncTest{},
		}},
	}

	var expectedResults []*AsyncTest
	var ops storage.EtcdTransaction

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := uint32(0); i < asyncConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", asyncPrefix, i)
		v := i * asyncDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &AsyncTest{Key: k, Time: v, Version: 1})
	}

	_, _, err := ops.EtcdTx(etcdCli, "")

	a.Nil(err)

	for _, expected := range expectedResults {
		res := <-asyncResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	assert.Equal(t, 0, len(asyncResults), "extra elements in asyncResults")

}
func TestAsyncPutDelete(t *testing.T) {

	asyncConcurrent := asyncConcurrent / 2

	clearEtcd()
	defer clearEtcd()
	asyncResults = make(chan *AsyncTest, asyncConcurrent)
	defer close(asyncResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    1000,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  asyncPrefix + "/",
			Name:    "asyncput",
			Actions: &AsyncTest{},
		}},
	}

	var expectedResults []*AsyncTest
	var ops storage.EtcdTransaction

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := uint32(0); i < asyncConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", asyncPrefix, i)
		v := i * asyncDelayMS * 4

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &AsyncTest{Key: k, Time: v, Version: 1})
		expectedResults = append(expectedResults, &AsyncTest{Key: k, Time: asyncDeleteDelayMS, Version: 1})
	}

	_, _, err := ops.EtcdTx(etcdCli, "")

	a.Nil(err)

	ops = storage.EtcdTransaction{}

	for i := uint32(0); i < asyncConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", asyncPrefix, i)
		v := i * asyncDelayMS * 5

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.DeleteKey(k)
	}

	_, _, err = ops.EtcdTx(etcdCli, "")

	a.Nil(err)

	for _, expected := range expectedResults {
		res := <-asyncResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	assert.Equal(t, 0, len(asyncResults), "extra elements in asyncResults")

}

// This one puts and deletes four times in a row immediately
// This makes sure that individual are handled in order (put -> delete) X 4
// However, there are two keys that are being put/deleted,
// and the each put of /asynctest/1 takes 1000+ MS,
// while all executions of /asynctest/0 takes < 1000 MS, so the order is
// /asynctest/0: (put -> delete) X 4
// /asynctest/1: (put -> delete) X 4
func TestAsyncPutDeleteFourTimesAsync(t *testing.T) {

	asyncConcurrent := asyncConcurrent / 4

	clearEtcd()
	defer clearEtcd()
	asyncResults = make(chan *AsyncTest, asyncConcurrent)
	defer close(asyncResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    1000,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  asyncPrefix + "/",
			Name:    "asyncput",
			Actions: &AsyncTest{},
		}},
	}

	var expectedResults [][]*AsyncTest
	var ops storage.EtcdTransaction

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	expectedResults = make([][]*AsyncTest, asyncConcurrent)

	for i := uint32(0); i < 4; i++ {
		ops = storage.EtcdTransaction{}

		for j := uint32(0); j < asyncConcurrent; j++ {
			k := fmt.Sprintf("%s/%d", asyncPrefix, j)
			v := (5*asyncConcurrent*j + i) * asyncDelayMS

			bs := make([]byte, 4)
			binary.BigEndian.PutUint32(bs, v)

			ops.WriteKey(k, bs)
			expectedResults[j] = append(expectedResults[j], &AsyncTest{Key: k, Time: v, Version: 1})
			expectedResults[j] = append(expectedResults[j], &AsyncTest{Key: k, Time: asyncDeleteDelayMS, Version: 1})
		}

		log.Debugf("ops: %+v", ops.String())

		_, _, err := ops.EtcdTx(etcdCli, "")

		a.Nil(err)

		ops = storage.EtcdTransaction{}

		for i := uint32(0); i < asyncConcurrent; i++ {
			k := fmt.Sprintf("%s/%d", asyncPrefix, i)
			ops.DeleteKey(k)
		}

		log.Debugf("ops: %+v", ops.String())

		_, _, err = ops.EtcdTx(etcdCli, "")

		a.Nil(err)
	}

	for _, list := range expectedResults {
		for _, expected := range list {
			res := <-asyncResults

			assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
		}
	}

	assert.Equal(t, 0, len(asyncResults), "extra elements in asyncResults")

}

// Unlike the other one, this only has a single worker
// So, the order of execution should be different,
// enforcing when there's a single worker,
// that everything is handled in revision order:
//   - Put: /asynctest/0
//   - Put: /asynctest/1
//   - Del: /asynctest/0
//   - Del: /asynctest/1
//
// X 4
func TestAsyncPutDeleteFourTimesSingle(t *testing.T) {

	asyncConcurrent := asyncConcurrent / 4

	clearEtcd()
	defer clearEtcd()
	asyncResults = make(chan *AsyncTest, asyncConcurrent)
	defer close(asyncResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:  asyncPrefix + "/",
			Name:    "asyncput",
			Actions: &AsyncTest{},
		}},
	}

	var expectedResults []*AsyncTest

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := uint32(0); i < 4; i++ {
		for j := uint32(0); j < asyncConcurrent; j++ {
			k := fmt.Sprintf("%s/%d", asyncPrefix, j)
			v := i

			bs := make([]byte, 4)
			binary.BigEndian.PutUint32(bs, v)

			_, err := etcdCli.Put(context.TODO(), k, string(bs))
			a.Nil(err)

			expectedResults = append(expectedResults, &AsyncTest{Key: k, Time: v, Version: 1})
		}

		for i := uint32(0); i < asyncConcurrent; i++ {
			k := fmt.Sprintf("%s/%d", asyncPrefix, i)

			_, err := etcdCli.Delete(context.TODO(), k)
			a.Nil(err)

			expectedResults = append(expectedResults, &AsyncTest{Key: k, Time: asyncDeleteDelayMS, Version: 1})
		}
	}

	for _, expected := range expectedResults {
		res := <-asyncResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	assert.Equal(t, 0, len(asyncResults), "extra elements in asyncResults")

}

// This is to test that we get messages if things are taking too long
// This doesn't fail if there are no messages, unfortunately
func TestAsyncLongPut(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	asyncResults = make(chan *AsyncTest, asyncConcurrent)
	defer close(asyncResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		Workers:                    int(asyncConcurrent),
		ServerHeartbeatGracePeriod: -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:                   asyncPrefix + "/",
			Name:                     "asyncput",
			Actions:                  &AsyncTest{},
			PeriodicMessageFrequency: 4 * time.Second,
		}},
	}

	var expectedResults []*AsyncTest
	var ops storage.EtcdTransaction

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := uint32(0); i < asyncConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", asyncPrefix, i)
		v := i*asyncDelayMS + uint32(time.Duration(10*time.Second).Milliseconds())

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &AsyncTest{Key: k, Time: v, Version: 1})
	}

	_, _, err := ops.EtcdTx(etcdCli, "")

	a.Nil(err)

	for _, expected := range expectedResults {
		res := <-asyncResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	assert.Equal(t, 0, len(asyncResults), "extra elements in asyncResults")

}

// This is to test that we get messages if something's been blocked for too long
func testAsyncReallyLongPut(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	asyncResults = make(chan *AsyncTest, asyncConcurrent)
	defer close(asyncResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		Workers:                    -1,
		ServerHeartbeatGracePeriod: -1,
		Reconcilers: []*rec.Reconciler{{
			Prefix:                   asyncPrefix + "/",
			Name:                     "asyncput",
			Actions:                  &AsyncTest{},
			PeriodicMessageFrequency: 1 * time.Second,
		}},
	}

	var expectedResults []*AsyncTest
	var ops storage.EtcdTransaction

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	k := fmt.Sprintf("%s/%d", asyncPrefix, 0)
	v := uint32(time.Duration(10*time.Minute + 10*time.Second).Milliseconds())

	bs := make([]byte, 4)
	binary.BigEndian.PutUint32(bs, v)

	ops.WriteKey(k, bs)
	expectedResults = append(expectedResults, &AsyncTest{Key: k, Time: v, Version: 1})

	_, _, err := ops.EtcdTx(etcdCli, "")

	a.Nil(err)

	k = fmt.Sprintf("%s/%d", asyncPrefix, 0)
	v = uint32(time.Duration(10*time.Minute + 10*time.Second).Milliseconds())

	bs = make([]byte, 4)
	binary.BigEndian.PutUint32(bs, v)

	ops = storage.EtcdTransaction{}
	ops.WriteKey(k, bs)
	expectedResults = append(expectedResults, &AsyncTest{Key: k, Time: v, Version: 2})

	_, _, err = ops.EtcdTx(etcdCli, "")

	a.Nil(err)

	for _, expected := range expectedResults {
		res := <-asyncResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

}

func TestAsyncEnsureOnceSingleThread(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	asyncResults = make(chan *AsyncTest, asyncConcurrent)
	defer close(asyncResults)

	a := assert.New(t)

	last := asyncConcurrent * asyncDelayMS
	delay := (asyncConcurrent * asyncDelayMS) / 1000
	delay += 1
	// if the time difference between the last key and the ensure frequency
	// is smaller than the interval, add another second
	if asyncConcurrent > (delay - 1000*last) {
		delay += 1
	}

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    int(asyncConcurrent),
		Reconcilers: []*rec.Reconciler{{
			Prefix:          asyncPrefix + "/",
			Name:            "asyncensure",
			Actions:         &AsyncTest{},
			EnsureFrequency: time.Duration(delay) * time.Second,
		}},
	}

	var expectedResults []*AsyncTest
	var ops storage.EtcdTransaction

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := uint32(0); i < asyncConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", asyncPrefix, i)
		v := i * asyncDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &AsyncTest{Key: k, Time: v, Version: 1})
	}

	_, _, err := ops.EtcdTx(etcdCli, "")

	a.Nil(err)

	for _, expected := range expectedResults {
		res := <-asyncResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	assert.Equal(t, 0, len(asyncResults), "init: extra elements in asyncResults")

	tm.WaitAllUntil(rec.ReconcilerOp_Ensure)

	for _, expected := range expectedResults {
		res := <-asyncResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	assert.Equal(t, 0, len(asyncResults), "ensure: extra elements in asyncResults")
	if len(asyncResults) != 0 {
		res := <-asyncResults
		log.Errorf("something extra: %+v", res)
	}

}

func TestAsyncEnsureMultiThread(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	asyncResults = make(chan *AsyncTest, asyncConcurrent)
	defer close(asyncResults)

	a := assert.New(t)

	asyncConcurrent := asyncConcurrent / 2
	last := asyncConcurrent * asyncDelayMS
	delay := last / 1000
	delay += 1
	// if the time difference between the last key and the ensure frequency
	// is smaller than the interval, add another second
	if asyncConcurrent > (delay - 1000*last) {
		delay += 1
	}

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    int(asyncConcurrent),
		Reconcilers: []*rec.Reconciler{{
			Prefix:          asyncPrefix + "/",
			Name:            "asyncensure",
			Actions:         &AsyncTest{},
			EnsureFrequency: time.Duration(delay) * time.Second,
		}},
	}

	var expectedResults []*AsyncTest
	var ops storage.EtcdTransaction

	go tm.Run()
	defer tm.Stop()

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := uint32(0); i < asyncConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", asyncPrefix, i)
		v := i * asyncDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &AsyncTest{Key: k, Time: v, Version: 1})
	}

	_, _, err := ops.EtcdTx(etcdCli, "")

	a.Nil(err)

	for _, expected := range expectedResults {
		res := <-asyncResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	assert.Equal(t, 0, len(asyncResults), "init: extra elements in asyncResults")

	start := time.Now()

	for i := 0; i < 4; i++ {

		tm.WaitAllUntil(rec.ReconcilerOp_Ensure)

		log.Infof("ensure frequency: %s", time.Since(start).String())

		for _, expected := range expectedResults {
			res := <-asyncResults

			assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
		}

		tm.WaitAllUntil(rec.ReconcilerOp_Idle)

		assert.Equal(t, 0, len(asyncResults), "ensure: extra elements in asyncResults")
		if len(asyncResults) != 0 {
			res := <-asyncResults
			log.Errorf("something extra: %+v", res)
		}

		start = time.Now()
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	assert.Equal(t, 0, len(asyncResults), "ensure: extra elements in asyncResults")
	if len(asyncResults) != 0 {
		res := <-asyncResults
		log.Errorf("something extra: %+v", res)
	}
}

func TestAsyncInitEnsureMultiThread(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	asyncResults = make(chan *AsyncTest, asyncConcurrent)
	defer close(asyncResults)

	a := assert.New(t)

	asyncConcurrent := asyncConcurrent / 2
	last := asyncConcurrent * asyncDelayMS
	delay := last / 1000
	delay += 1
	// if the time difference between the last key and the ensure frequency
	// is smaller than the interval, add another second
	if asyncConcurrent > (delay - 1000*last) {
		delay += 1
	}
	tm := &rec.ReconcilerManager{
		Manager: "test",
		EtcdCli: etcdCli,
		Workers: 2 * int(asyncConcurrent),
		Reconcilers: []*rec.Reconciler{
			{
				Prefix:  asyncPrefix + "/a/",
				Name:    "asyncinita",
				Actions: &AsyncTest{},
			},
			{
				Prefix:  asyncPrefix + "/b/",
				Name:    "asyncinitb",
				Actions: &AsyncTest{},
			},
		},
	}

	var expectedResults []*AsyncTest
	var ops storage.EtcdTransaction

	for i := uint32(0); i < asyncConcurrent; i++ {
		k := fmt.Sprintf("%s/a/%d", asyncPrefix, i)
		v := i * asyncDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &AsyncTest{Key: k, Time: v, Version: 1})
	}

	for i := uint32(asyncConcurrent); i < 2*asyncConcurrent; i++ {
		k := fmt.Sprintf("%s/b/%d", asyncPrefix, i)
		v := i * asyncDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &AsyncTest{Key: k, Time: v, Version: 1})
	}

	_, _, err := ops.EtcdTx(etcdCli, "")

	a.Nil(err)

	go tm.Run()
	defer tm.Stop()

	for _, expected := range expectedResults {
		res := <-asyncResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	assert.Equal(t, 0, len(asyncResults), "ensure: extra elements in asyncResults")
	if len(asyncResults) != 0 {
		res := <-asyncResults
		log.Errorf("something extra: %+v", res)
	}
}

func TestAsyncEnsureMultiTasks(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	asyncResults = make(chan *AsyncTest, asyncConcurrent)
	defer close(asyncResults)

	a := assert.New(t)

	last := asyncConcurrent * asyncDelayMS
	delay := last / 1000
	delay += 1
	// if the time difference between the last key and the ensure frequency
	// is smaller than the interval, add another second
	if asyncConcurrent > (delay - 1000*last) {
		delay += 1
	}

	tm := &rec.ReconcilerManager{
		Manager: "test",
		EtcdCli: etcdCli,
		Workers: 2 * int(asyncConcurrent),
		Reconcilers: []*rec.Reconciler{
			{
				Prefix:          asyncPrefix + "/a/",
				Name:            "asyncensurea",
				Actions:         &AsyncTest{},
				EnsureFrequency: time.Duration(delay) * time.Second,
			}, {
				Prefix:          asyncPrefix + "/b/",
				Name:            "asyncensureb",
				Actions:         &AsyncTest{},
				EnsureFrequency: 2 * time.Duration(delay) * time.Second,
			},
		},
	}

	var expectedResults []*AsyncTest
	var ops storage.EtcdTransaction

	for i := uint32(0); i < asyncConcurrent; i++ {
		k := fmt.Sprintf("%s/a/%d", asyncPrefix, i)
		v := i * asyncDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &AsyncTest{Key: k, Time: v, Version: 1})
	}

	for i := uint32(asyncConcurrent); i < 2*asyncConcurrent; i++ {
		k := fmt.Sprintf("%s/b/%d", asyncPrefix, i)
		v := i * asyncDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &AsyncTest{Key: k, Time: v, Version: 1})
	}

	_, _, err := ops.EtcdTx(etcdCli, "")

	a.Nil(err)

	go tm.Run()
	defer tm.Stop()

	for _, expected := range expectedResults {
		res := <-asyncResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	assert.Equal(t, 0, len(asyncResults), "init: extra elements in asyncResults")

	tm.WaitAllUntil(rec.ReconcilerOp_Ensure)

	for _, expected := range expectedResults {
		res := <-asyncResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	assert.Equal(t, 0, len(asyncResults), "ensure: extra elements in asyncResults")
	if len(asyncResults) != 0 {
		res := <-asyncResults
		log.Errorf("something extra: %+v", res)
	}
}

// This tests that when we stop, we do not handle any more keys
func TestAsyncStopWhileHandling(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	asyncResults = make(chan *AsyncTest, asyncConcurrent)
	defer close(asyncResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    int(asyncConcurrent),
		Reconcilers: []*rec.Reconciler{{
			Prefix:  asyncPrefix + "/",
			Name:    "asyncstop",
			Actions: &AsyncTest{},
		}},
	}

	var expectedResults []*AsyncTest
	var ops storage.EtcdTransaction

	go tm.Run()
	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	for i := uint32(0); i < asyncConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", asyncPrefix, i)
		v := i * asyncDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
		expectedResults = append(expectedResults, &AsyncTest{Key: k, Time: v, Version: 1})
	}

	_, _, err := ops.EtcdTx(etcdCli, "")

	a.Nil(err)

	tm.WaitAllUntil(rec.ReconcilerOp_Create, rec.ReconcilerOp_Ensure, rec.ReconcilerOp_Update)

	tm.Stop()

	for _, expected := range expectedResults {
		res := <-asyncResults

		assert.Truef(t, expected.Equals(res), "%+v != %+v", expected, res)
	}

	assert.Equal(t, 0, len(asyncResults), "ensure: extra elements in asyncResults")
	if len(asyncResults) != 0 {
		res := <-asyncResults
		log.Errorf("something extra: %+v", res)
	}

}

// This tests that we do not handle any keys if we're immediately stopped
func TestAsyncStopBeforeHandle(t *testing.T) {
	clearEtcd()
	defer clearEtcd()
	asyncResults = make(chan *AsyncTest, asyncConcurrent)
	defer close(asyncResults)

	a := assert.New(t)

	tm := &rec.ReconcilerManager{
		Manager:                    "test",
		EtcdCli:                    etcdCli,
		ServerHeartbeatGracePeriod: -1,
		Workers:                    int(asyncConcurrent),
		Reconcilers: []*rec.Reconciler{{
			Prefix:  asyncPrefix + "/",
			Name:    "asyncstop",
			Actions: &AsyncTest{},
		}},
	}

	var ops storage.EtcdTransaction

	go tm.Run()
	tm.WaitAllUntil(rec.ReconcilerOp_Idle)

	tm.Stop()

	for i := uint32(0); i < asyncConcurrent; i++ {
		k := fmt.Sprintf("%s/%d", asyncPrefix, i)
		v := i * asyncDelayMS

		bs := make([]byte, 4)
		binary.BigEndian.PutUint32(bs, v)

		ops.WriteKey(k, bs)
	}

	_, _, err := ops.EtcdTx(etcdCli, "")

	a.Nil(err)

	time.Sleep(time.Second)

	assert.Equal(t, 0, len(asyncResults), "ensure: extra elements in asyncResults")
	if len(asyncResults) != 0 {
		res := <-asyncResults
		log.Errorf("something extra: %+v", res)
	}

}
