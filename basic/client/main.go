package main

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"go.etcd.io/etcd/client/v3"

	"gitlab.com/mergetb/tech/reconcile"
	"gitlab.com/mergetb/tech/reconcile/basic"
	"gitlab.com/mergetb/tech/reconcile/pkg/heartbeat"
)

var etcdCli *clientv3.Client
var endpoint string

func main() {

	var root = &cobra.Command{
		Use:   "basicclient",
		Short: "Puts keys in for basic/server to reconcile against",
	}
	root.PersistentFlags().StringVarP(&endpoint, "endpoint", "e", "localhost:2379", "set the endpoint (overridden by env var TEST_ETCD_ENDPOINT)")

	everything := false
	clearcmd := &cobra.Command{
		Use:   "clear",
		Short: "Clear keys under /basic/",
		Args:  cobra.ExactArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			initEtcd()
			clearEtcd(everything)
		},
	}
	clearcmd.Flags().BoolVarP(&everything, "delete", "d", false, "delete everything, not just /basic/")
	root.AddCommand(clearcmd)

	start := 0
	diff := 0
	manycmd := &cobra.Command{
		Use:   "many [num_keys]",
		Short: "Write many keys",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			num, err := strconv.Atoi(args[0])
			if err != nil {
				log.Fatal(err)
			}

			initEtcd()
			writeMany(start, diff, num)
		},
	}
	manycmd.Flags().IntVarP(&start, "start", "s", 0, "Where to start")
	manycmd.Flags().IntVarP(&diff, "diff", "d", 100, "Interval between keys")
	root.AddCommand(manycmd)

	samecmd := &cobra.Command{
		Use:   "same [key] [num_keys]",
		Short: "Write same key",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			num, err := strconv.Atoi(args[1])
			if err != nil {
				log.Fatal(err)
			}

			initEtcd()
			writeSame(args[0], start, diff, num)
		},
	}
	samecmd.Flags().IntVarP(&start, "start", "s", 0, "Where to start")
	samecmd.Flags().IntVarP(&diff, "diff", "d", 100, "Interval between keys")
	root.AddCommand(samecmd)

	heartbeats := &cobra.Command{
		Use:   "heartbeats [frequency (s)]",
		Short: "Sends heartbeats",
		Args:  cobra.MaximumNArgs(1),
		Run: func(cmd *cobra.Command, args []string) {

			if len(args) == 0 {
				log.Infof("clearing heartbeat")
				initEtcd()
				etcdCli.Delete(context.Background(), reconcile.ServerHeartbeatPath())
			} else {
				num, err := strconv.Atoi(args[0])
				if err != nil {
					log.Fatal(err)
				}
				initEtcd()
				heartbeat.PeriodicallySendEtcdHeartbeats(context.Background(), etcdCli, uint64(num))
			}

		},
	}
	root.AddCommand(heartbeats)

	root.Execute()
}

func initEtcd() {
	env, found := os.LookupEnv("TEST_ETCD_ENDPOINT")
	if found && len(env) != 0 {
		endpoint = env
	}
	cli, err := basic.CreateEtcdClient(endpoint, false)
	if err != nil {
		log.Fatal(err)
	}
	etcdCli = cli
}

func writeMany(start, diff, num int) {
	x := uint32(start)
	for i := 0; i < num; i++ {
		err := basic.WriteBasic(etcdCli, fmt.Sprint(i), x)
		if err != nil {
			log.Fatal(err)
		}
		x += uint32(diff)
	}
}

func writeSame(key string, start, diff, num int) {
	x := uint32(start)
	for i := 0; i < num; i++ {
		err := basic.WriteBasic(etcdCli, key, x)
		if err != nil {
			log.Fatal(err)
		}
		x += uint32(diff)
	}
}

func clearEtcd(everything bool) {
	target := basic.BasicPrefix
	if everything {
		target = ""
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	_, err := etcdCli.Delete(ctx, target, clientv3.WithPrefix())
	if err != nil {
		log.Fatal(err)
	}

	if everything {
		log.Infof("cleared /")
	} else {
		log.Infof("cleared /basic/")
	}
}
