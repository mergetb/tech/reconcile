package basic

import (
	"context"
	"fmt"
	"time"

	"github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/logging"
	log "github.com/sirupsen/logrus"
	"go.etcd.io/etcd/client/v3"
	"go.uber.org/zap"
	"google.golang.org/grpc"
)

func CreateEtcdClient(endpoint string, debug bool) (*clientv3.Client, error) {

	if endpoint == "" {
		endpoint = "localhost:2379"
	}
	log.Infof("endpoint: %s", endpoint)

	cfg := clientv3.Config{
		Endpoints:            []string{endpoint},
		DialKeepAliveTime:    10 * time.Minute,
		DialKeepAliveTimeout: 20 * time.Second,
		DialTimeout:          2 * time.Second,
		PermitWithoutStream:  true,
	}

	if debug {
		lg, err := zap.NewDevelopment()
		if err != nil {
			return nil, err
		}

		cfg.Logger = lg
		cfg.DialOptions = []grpc.DialOption{
			grpc.WithChainStreamInterceptor(logging.StreamClientInterceptor(InterceptorLogger(lg))),
			grpc.WithChainUnaryInterceptor(logging.UnaryClientInterceptor(InterceptorLogger(lg))),
			grpc.WithStreamInterceptor(logging.StreamClientInterceptor(InterceptorLogger(lg))),
		}
	}

	cli, err := clientv3.New(cfg)
	if err != nil {
		return nil, fmt.Errorf("etcd client: %v", err)
	}

	return cli, nil
}

func InterceptorLogger(l *zap.Logger) logging.Logger {
	return logging.LoggerFunc(func(ctx context.Context, lvl logging.Level, msg string, fields ...any) {
		f := make([]zap.Field, 0, len(fields)/2)

		for i := 0; i < len(fields); i += 2 {
			key := fields[i]
			value := fields[i+1]

			switch v := value.(type) {
			case string:
				f = append(f, zap.String(key.(string), v))
			case int:
				f = append(f, zap.Int(key.(string), v))
			case bool:
				f = append(f, zap.Bool(key.(string), v))
			default:
				f = append(f, zap.Any(key.(string), v))
			}
		}

		logger := l.WithOptions(zap.AddCallerSkip(1)).With(f...)

		switch lvl {
		case logging.LevelDebug:
			logger.Debug(msg)
		case logging.LevelInfo:
			logger.Info(msg)
		case logging.LevelWarn:
			logger.Warn(msg)
		case logging.LevelError:
			logger.Error(msg)
		default:
			panic(fmt.Sprintf("unknown level %v", lvl))
		}
	})
}
