package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	rec "gitlab.com/mergetb/tech/reconcile"
	"go.etcd.io/etcd/client/v3"

	"gitlab.com/mergetb/tech/reconcile/basic"
)

var etcdCli *clientv3.Client
var endpoint string

func main() {

	var root = &cobra.Command{
		Use:   "basicclient",
		Short: "Puts keys in for basic/server to reconcile against",
		Args:  cobra.ExactArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			initEtcd()

			tm := &rec.ReconcilerManager{
				Manager: "basictest",
				EtcdCli: etcdCli,
				Workers: -1,
				Reconcilers: []*rec.Reconciler{{
					Prefix:  basic.BasicPrefix,
					Name:    "basic",
					Actions: &basic.BasicTest{},
				}},
			}

			tm.Run()
		},
	}
	root.PersistentFlags().StringVarP(&endpoint, "endpoint", "e", "localhost:2379", "set the endpoint (overridden by env var TEST_ETCD_ENDPOINT)")
	root.Execute()

}

func initEtcd() {
	env, found := os.LookupEnv("TEST_ETCD_ENDPOINT")
	if found && len(env) != 0 {
		endpoint = env
	}
	cli, err := basic.CreateEtcdClient(endpoint, false)
	if err != nil {
		log.Fatal(err)
	}
	etcdCli = cli
}
