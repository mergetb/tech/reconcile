package basic

import (
	"context"
	"encoding/binary"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	rec "gitlab.com/mergetb/tech/reconcile"
	"go.etcd.io/etcd/client/v3"

	storage "gitlab.com/mergetb/tech/shared/storage/etcd"
)

var BasicPrefix string = "/basic/"

type BasicTest struct {
	Key  string
	Time uint32
}

func (bt *BasicTest) Handle(event string, prev, value []byte, version int64, td *rec.TaskData) *rec.TaskMessage {
	if value == nil {
		value = prev
	}

	delay := binary.BigEndian.Uint32(value)

	log.Infof("%s: %s sleeping for: %d ms", event, td.TaskValue.EtcdKey, delay)
	time.Sleep(time.Duration(delay) * time.Millisecond)

	log.Infof("  %s done!", td.TaskValue.EtcdKey)

	return nil
}

func (bt *BasicTest) Parse(k string) bool {
	return strings.HasPrefix(k, BasicPrefix)
}

func (bt *BasicTest) Create(value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return bt.Handle("create", nil, value, version, r)
}

func (bt *BasicTest) Update(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return bt.Handle("update", prev, value, version, r)
}

func (bt *BasicTest) Ensure(prev, value []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return bt.Handle("ensure", prev, value, version, r)
}

func (bt *BasicTest) Delete(prev []byte, version int64, r *rec.TaskData) *rec.TaskMessage {
	return nil
}

func WriteBasic(client *clientv3.Client, name string, value uint32) error {
	v := value

	bs := make([]byte, 4)
	binary.BigEndian.PutUint32(bs, v)

	key := storage.SmartJoin(BasicPrefix, name)

	log.Infof("Writing: %s -> %d", key, value)

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	_, err := client.Put(ctx, key, string(bs))

	return err
}
