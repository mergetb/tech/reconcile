package pworkerpool

import (
	"math/rand"
	"sync"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

type composite struct {
	priority uint64
	value    string
}

var fruits = []string{
	"0apple",
	"1banana",
	"2cherry",
	"3durian",
	"4elderberry",
	"5fig",
	"6grape",
	"7honeydew",
}

func TestQueueForwards(t *testing.T) {

	a := assert.New(t)

	var p PriorityQueue[string]

	for i, x := range fruits {
		p.Push(uint64(i), x)
	}

	logrus.Info(p.String())

	for _, x := range fruits {
		y := p.Pop()
		a.EqualValues(x, y)
	}

}

func TestQueueRandom(t *testing.T) {

	a := assert.New(t)

	var p PriorityQueue[string]

	var items_sorted []composite
	var items_random []composite

	for i, x := range fruits {
		items_sorted = append(items_sorted, composite{uint64(i), x})
		items_random = append(items_random, composite{uint64(i), x})
	}

	rand.Shuffle(len(items_random), func(i, j int) {
		items_random[i], items_random[j] = items_random[j], items_random[i]
	})

	for _, x := range items_random {
		p.Push(x.priority, x.value)
	}

	logrus.Info(p.String())

	for _, x := range items_sorted {
		y := p.Pop()
		a.EqualValues(x.value, y)
	}
}

func TestQueueRandomAsync(t *testing.T) {

	a := assert.New(t)

	var p PriorityQueue[string]

	var items_sorted []composite
	var items_random []composite

	for i, x := range fruits {
		items_sorted = append(items_sorted, composite{uint64(i), x})
		items_random = append(items_random, composite{uint64(i), x})
	}

	rand.Shuffle(len(items_random), func(i, j int) {
		items_random[i], items_random[j] = items_random[j], items_random[i]
	})

	var wg sync.WaitGroup

	for _, x := range items_random {
		x := x

		wg.Add(1)
		go func() {
			p.Push(x.priority, x.value)
			wg.Done()
		}()
	}

	wg.Wait()

	logrus.Info(p.String())

	for _, x := range items_sorted {
		y := p.Pop()
		a.EqualValues(x.value, y)
	}
}

// due to the properties of a heap,
// if all of the priorities are identical
// we will not get things in exactly the same order as we put them
// instead, it will be [0, n-1, n-2, ..., 2, 1]
func TestQueueIdentical(t *testing.T) {

	a := assert.New(t)

	var p PriorityQueue[string]

	for _, x := range fruits {
		p.Push(0, x)
	}

	logrus.Info(p.String())

	for i := range fruits {
		y := p.Pop()
		j := len(fruits) - i
		if i == 0 {
			j = 0
		}
		a.EqualValues(fruits[j], y)
	}
}
