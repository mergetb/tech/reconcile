package pworkerpool

import (
	"container/heap"
	"fmt"
	"sync"

	"github.com/edwingeng/deque/v2"
)

// This is the public struct that has more ergonomic methods
// and is thread safe
type PriorityQueue[T any] struct {
	pq    priorityQueue[T]
	mutex sync.RWMutex
	init  sync.Once
}

func (pq *PriorityQueue[T]) Init() {
	pq.init.Do(func() {
		pq.pq.d = deque.NewDeque[*item[T]]()
	})
}

func (pq *PriorityQueue[T]) Len() int {
	pq.mutex.RLock()
	defer pq.mutex.RUnlock()

	pq.Init()

	return pq.pq.Len()
}

func (pq *PriorityQueue[T]) Push(p uint64, x T) {
	pq.mutex.Lock()
	defer pq.mutex.Unlock()

	pq.Init()

	item := &item[T]{
		value:    x,
		priority: uint64(p),
	}

	heap.Push(&pq.pq, item)
}

func (pq *PriorityQueue[T]) Front() T {
	pq.mutex.RLock()
	defer pq.mutex.RUnlock()

	pq.Init()

	if pq.pq.Len() == 0 {
		panic("Front() called while empty")
	}

	x := pq.pq.d.Peek(0)

	return x.value
}

func (pq *PriorityQueue[T]) Pop() T {
	pq.mutex.Lock()
	defer pq.mutex.Unlock()

	pq.Init()

	if pq.pq.Len() == 0 {
		panic("Pop() called while empty")
	}

	x := (heap.Pop(&pq.pq)).(*item[T])

	return x.value
}

func (pq *PriorityQueue[T]) Remove(f func(x T) bool) bool {
	pq.mutex.Lock()
	defer pq.mutex.Unlock()

	pq.Init()

	remove := -1

	pq.pq.d.Range(func(i int, v *item[T]) bool {
		if f(v.value) {
			remove = i
			return false
		}

		return true
	})

	if remove < 0 {
		return false
	}

	heap.Remove(&pq.pq, remove)
	return true
}

func (pq *PriorityQueue[T]) String() string {
	pq.mutex.Lock()
	defer pq.mutex.Unlock()

	pq.Init()

	s := "["
	pq.pq.d.Range(func(_ int, v *item[T]) bool {
		s += fmt.Sprintf("{%d: %+v}, ", v.priority, v.value)
		return true
	})

	s += "]"

	return s

}

// Here, the number closest to 0 has the highest priority
// An Item is something we manage in a priority queue.
type item[T any] struct {
	value    T      // The value of the item; arbitrary.
	priority uint64 // The priority of the item in the queue.
}

// priorityQueue implements heap.Interface,
// so the golang package can use it as a
type priorityQueue[T any] struct {
	// this is an optimized queue,
	// faster than just using slices
	d *deque.Deque[*item[T]]
}

func (pq priorityQueue[T]) Len() int {
	return pq.d.Len()
}

func (pq priorityQueue[T]) Less(i, j int) bool {
	return pq.d.Peek(i).priority < pq.d.Peek(j).priority
}

func (pq priorityQueue[T]) Swap(i, j int) {
	pq.d.Swap(i, j)
}

func (pq *priorityQueue[T]) Push(x any) {
	item := x.(*item[T])
	pq.d.PushBack(item)
}

func (pq *priorityQueue[T]) Pop() any {
	item := pq.d.PopBack()
	return item
}
