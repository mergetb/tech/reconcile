package altsync

// This is an alternative version of Waitgroup,
// that allows you to add and wait in any order
//
// The "sync" package has many more requirements on
// when you can call Add/Done/Wait and we do not always fulfill them
//
// This works with brute force, using an RWMutex
// Positive Adds can be done at the same time, as we are using an atomic.Int64
// Negative Adds and Waits cannot, as we might have to write to the waiters list
//
// Use the normal one if you can fulfill the behavior

import (
	"sync"
	"sync/atomic"
)

type WaitGroup struct {
	counter atomic.Int64
	waiters []chan struct{}
	mut     sync.RWMutex
}

func (wg *WaitGroup) Add(delta int) {

	if wg == nil {
		panic("altsync: WaitGroup is nil")
	}

	if delta == 0 {
		return
	}

	// can just take an rlock instead
	// positive adds can be done simultaneously
	if delta > 0 {
		wg.mut.RLock()
		defer wg.mut.RUnlock()

		wg.counter.Add(int64(delta))
		return
	}

	wg.mut.Lock()
	defer wg.mut.Unlock()

	counter := wg.counter.Add(int64(delta))
	if counter < 0 {
		panic("altsync: negative WaitGroup counter")
	}

	if counter == 0 {
		for _, w := range wg.waiters {
			close(w)
		}

		wg.waiters = nil
	}
}

func (wg *WaitGroup) Done() {

	if wg == nil {
		panic("altsync: WaitGroup is nil")
	}

	wg.Add(-1)
}

func (wg *WaitGroup) Wait() {

	if wg == nil {
		panic("altsync: WaitGroup is nil")
	}

	wg.mut.Lock()

	counter := wg.counter.Load()
	if counter < 0 {
		wg.mut.Unlock()
		panic("altsync: negative WaitGroup counter")
	}

	if counter == 0 {
		for _, w := range wg.waiters {
			close(w)
		}

		wg.waiters = nil

		wg.mut.Unlock()
		return
	}

	w := make(chan struct{})
	wg.waiters = append(wg.waiters, w)

	wg.mut.Unlock()

	<-w
}
