//go:build !race
// +build !race

package altsync

import (
	"sync"
	"testing"
)

// the point is that the original dies,
// while the new package does not
func TestNormal(t *testing.T) {
	var wg sync.WaitGroup
	if !testWaitgroup(t, &wg, 1) {
		t.Fatal("expected panic")
	}
}
