package altsync

import (
	log "go.uber.org/zap"
	"testing"
)

var logger, _ = log.NewDevelopment()

const (
	waitTimes      = 10000
	totalWrites    = 5120
	progressModulo = 100
	maxWorkers     = 128
)

type waitgroup interface {
	Add(delta int)
	Done()
	Wait()
}

func TestAlt(t *testing.T) {
	var wg WaitGroup
	for i := 1; i < maxWorkers; i = 2 * i {
		if testWaitgroup(t, &wg, i) {
			t.Fatal("did not expect panic")
		}
	}
}

func testWaitgroup(t *testing.T, wg waitgroup, workers int) (panicked bool) {
	defer func() {
		if r := recover(); r != nil {
			logger.Sugar().Infof("%+v", r)
			panicked = true
		}
	}()

	started_ch := make(chan struct{}, workers)
	stop_ch := make(chan struct{})
	ended_ch := make(chan struct{}, workers)
	waiter_ch := make(chan struct{}, workers)

	for progress := 0; progress < waitTimes; progress++ {

		wg.Add(1)

		// waiters
		for worker := 0; worker < workers; worker++ {
			go func() {
				defer func() {
					if r := recover(); r != nil {
						logger.Sugar().Infof("%+v", r)
						panicked = true
					}

					waiter_ch <- struct{}{}
				}()

				wg.Wait()
			}()
		}

		// spawner
		for worker := 0; worker < workers; worker++ {
			go func() {
				defer func() {
					if r := recover(); r != nil {
						logger.Sugar().Infof("%+v", r)
						panicked = true
					}

					ended_ch <- struct{}{}
				}()

				started_ch <- struct{}{}

				// keep the number of total number of writes constant
				for i := 0; i < totalWrites/workers; i++ {
					select {
					case <-stop_ch:
						return
					default:
					}

					wg.Add(1)
					wg.Done()
				}
			}()
		}

		for i := 0; i < workers; i++ {
			<-started_ch
		}

		wg.Done()

		if progress%progressModulo == 0 {
			logger.Info("progress report", log.Int("workers", workers), log.Int("current", progress), log.Int("total", waitTimes))
		}

		for i := 0; i < workers; i++ {
			<-waiter_ch
		}

		close(stop_ch)

		for i := 0; i < workers; i++ {
			<-ended_ch
		}

		stop_ch = make(chan struct{})

		if panicked {
			return
		}

	}

	return
}
