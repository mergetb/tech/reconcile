package heartbeat

import (
	"context"
	"fmt"
	"time"

	"go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/timestamppb"

	"gitlab.com/mergetb/tech/reconcile"
)

func SendEtcdHeartbeat(cli *clientv3.Client, interval uint64) error {
	out, err := proto.Marshal(&reconcile.ServerHeartbeat{
		Interval: interval,
		When:     timestamppb.Now(),
	})
	if err != nil {
		return fmt.Errorf("failed to marshal server heartbeat message: %+v", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	_, err = cli.Put(ctx, reconcile.ServerHeartbeatPath(), string(out))
	cancel()

	if err != nil {
		return fmt.Errorf("failed to write to etcd: %+v", err)
	}

	return nil
}

func PeriodicallySendEtcdHeartbeats(ctx context.Context, cli *clientv3.Client, interval uint64) error {
	ticker := time.NewTicker(time.Duration(interval) * time.Second)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			return nil
		case <-ticker.C:
			err := SendEtcdHeartbeat(cli, interval)
			if err != nil {
				return err
			}
		}
	}
}
