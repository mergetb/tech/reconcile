package qosticker

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

// make sure times are within 2 percent
const margin_numerator = 102
const margin_denominator = 100

// check that it works for 1 second
func TestTickerSimple(t *testing.T) {

	a := assert.New(t)

	target := time.Second

	ticker := NewQOSTicker(target, 1)

	start := time.Now()
	ticker.Start()
	<-ticker.TickChannel()
	actual := time.Since(start)

	lower := target * margin_denominator / margin_numerator
	upper := target * margin_numerator / margin_denominator

	a.Truef(lower <= actual, "lower %+v vs. actual time: %+v", lower.String(), actual.String())
	a.Truef(upper >= actual, "upper %+v vs. actual time: %+v", upper.String(), actual.String())

}

// pause it halfway through and check that it doesn't tick again
func TestTickerPause(t *testing.T) {

	target := 4 * time.Second

	ticker := NewQOSTicker(target, 1)

	ticker.Start()
	time.Sleep(target / 2)
	ticker.Pause()

	select {
	case <-time.After(target):
		return
	case <-ticker.TickChannel():
		t.Fatalf("somehow, the channel ticked after pausing")
	}
}

func TestTickerRestart(t *testing.T) {
	a := assert.New(t)

	wait := 6 * time.Second
	target := 10 * time.Second

	ticker := NewQOSTicker(wait, 1)

	start := time.Now()

	ticker.Start()

	// pause it after 2 seconds, then sleep for 4 seconds
	time.Sleep(2 * time.Second) // not counted

	ticker.Pause()
	time.Sleep(4 * time.Second)
	ticker.Start()

	<-ticker.TickChannel()

	actual := time.Since(start)

	lower := target * margin_denominator / margin_numerator
	upper := target * margin_numerator / margin_denominator

	a.Truef(lower <= actual, "lower %+v vs. actual time: %+v", lower.String(), actual.String())
	a.Truef(upper >= actual, "upper %+v vs. actual time: %+v", upper.String(), actual.String())
}

// run the ticker twice
func TestTickerCompleteTwice(t *testing.T) {
	a := assert.New(t)

	wait := 6 * time.Second
	target := 12 * time.Second

	ticker := NewQOSTicker(wait, 1)

	start := time.Now()

	ticker.Start()
	<-ticker.TickChannel()
	ticker.Start()
	<-ticker.TickChannel()

	actual := time.Since(start)

	lower := target * margin_denominator / margin_numerator
	upper := target * margin_numerator / margin_denominator

	a.Truef(lower <= actual, "lower %+v vs. actual time: %+v", lower.String(), actual.String())
	a.Truef(upper >= actual, "upper %+v vs. actual time: %+v", upper.String(), actual.String())
}

func TestTickerCompleteTwiceWithPause(t *testing.T) {
	a := assert.New(t)

	wait := 3 * time.Second
	target := 10 * time.Second

	ticker := NewQOSTicker(wait, 1)

	start := time.Now()

	ticker.Start()

	time.Sleep(time.Second) // not counted
	ticker.Pause()
	time.Sleep(4 * time.Second)
	ticker.Start()

	<-ticker.TickChannel()
	ticker.Start()
	<-ticker.TickChannel()

	actual := time.Since(start)

	lower := target * margin_denominator / margin_numerator
	upper := target * margin_numerator / margin_denominator

	a.Truef(lower <= actual, "lower %+v vs. actual time: %+v", lower.String(), actual.String())
	a.Truef(upper >= actual, "upper %+v vs. actual time: %+v", upper.String(), actual.String())
}

func TestTickerQOSIncreaseBefore(t *testing.T) {
	a := assert.New(t)

	wait := 4 * time.Second
	target := 6 * time.Second

	ticker := NewQOSTicker(wait, 1)

	start := time.Now()

	ticker.Start()
	ticker.UpdateQOS(8 * time.Second)
	<-ticker.TickChannel()

	actual := time.Since(start)

	lower := target * margin_denominator / margin_numerator
	upper := target * margin_numerator / margin_denominator

	a.Truef(lower <= actual, "lower %+v vs. actual time: %+v", lower.String(), actual.String())
	a.Truef(upper >= actual, "upper %+v vs. actual time: %+v", upper.String(), actual.String())
}

func TestTickerQOSIncreaseAfter(t *testing.T) {
	a := assert.New(t)

	wait := 4 * time.Second
	target := 12 * time.Second

	ticker := NewQOSTicker(wait, 1)

	start := time.Now()

	// wait 4 seconds
	ticker.Start()
	<-ticker.TickChannel()

	ticker.UpdateQOS(8 * time.Second)

	// wait 2 seconds
	time.Sleep(2 * time.Second)

	// it should be set to 6 seconds now (4+8)/2=6
	ticker.Start()
	<-ticker.TickChannel()

	actual := time.Since(start)

	lower := target * margin_denominator / margin_numerator
	upper := target * margin_numerator / margin_denominator

	a.Truef(lower <= actual, "lower %+v vs. actual time: %+v", lower.String(), actual.String())
	a.Truef(upper >= actual, "upper %+v vs. actual time: %+v", upper.String(), actual.String())
}

func TestTickerQOSDecreaseBefore(t *testing.T) {
	a := assert.New(t)

	wait := 4 * time.Second
	target := 5 * time.Second

	ticker := NewQOSTicker(wait, 1)
	ticker.current_frequency = (10 * time.Second)
	ticker.UpdateQOS(0)

	start := time.Now()
	ticker.Start()

	<-ticker.TickChannel()

	actual := time.Since(start)

	lower := target * margin_denominator / margin_numerator
	upper := target * margin_numerator / margin_denominator

	a.Truef(lower <= actual, "lower %+v vs. actual time: %+v", lower.String(), actual.String())
	a.Truef(upper >= actual, "upper %+v vs. actual time: %+v", upper.String(), actual.String())
}

func TestTickerQOSDecreaseZero(t *testing.T) {
	a := assert.New(t)

	wait := 10 * time.Second
	target := 8 * time.Second

	start := time.Now()

	ticker := NewQOSTicker(wait, 1)
	ticker.min_frequency = 2 * time.Second

	ticker.Start()
	time.Sleep(8 * time.Second)
	ticker.UpdateQOS(0)

	<-ticker.TickChannel()

	actual := time.Since(start)

	lower := target * margin_denominator / margin_numerator
	upper := target * margin_numerator / margin_denominator

	a.Truef(lower <= actual, "lower %+v vs. actual time: %+v", lower.String(), actual.String())
	a.Truef(upper >= actual, "upper %+v vs. actual time: %+v", upper.String(), actual.String())
}

func TestTickerQOSResetTimer(t *testing.T) {
	a := assert.New(t)

	wait := 5 * time.Second
	target := 10 * time.Second

	start := time.Now()

	ticker := NewQOSTicker(wait, 1)
	ticker.min_frequency = 2 * time.Second

	ticker.Start()

	select {
	case <-ticker.TickChannel():
		t.Fatalf("timer not supposed to tick")
	case <-time.After(2 * time.Second):
		ticker.ResetTimer()
	}

	select {
	case <-ticker.TickChannel():
		t.Fatalf("timer not supposed to tick")
	case <-time.After(3 * time.Second):
		ticker.ResetTimer()
	}

	<-ticker.TickChannel()

	actual := time.Since(start)

	lower := target * margin_denominator / margin_numerator
	upper := target * margin_numerator / margin_denominator

	a.Truef(lower <= actual, "lower %+v vs. actual time: %+v", lower.String(), actual.String())
	a.Truef(upper >= actual, "upper %+v vs. actual time: %+v", upper.String(), actual.String())
}
