package qosticker

import (
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
)

type QOSTicker struct {
	tick_channel       chan struct{}
	pause_channel      chan struct{}
	stopped_channel    chan struct{}
	min_frequency      time.Duration
	current_frequency  time.Duration
	remaining_duration time.Duration
	ticker_multipler   float64
	running            sync.Mutex
	modlock            sync.Mutex
	stop               sync.Once
	handle_stop        sync.Once
}

func NewQOSTicker(min_frequency time.Duration, ticker_multipler float64) *QOSTicker {
	return &QOSTicker{
		tick_channel:      make(chan struct{}, 1),
		pause_channel:     make(chan struct{}),
		stopped_channel:   make(chan struct{}),
		min_frequency:     min_frequency,
		current_frequency: min_frequency,
		ticker_multipler:  ticker_multipler,
	}
}

func (t *QOSTicker) TickChannel() <-chan struct{} {
	return t.tick_channel
}

func (t *QOSTicker) GetCurrentFrequency() time.Duration {
	return t.current_frequency
}

func (t *QOSTicker) Start() bool {

	if !t.running.TryLock() {
		return false
	}

	go func() {
		defer t.running.Unlock()

		select {
		case <-t.stopped_channel:
			t.finish()
			return
		default:
		}

		t.modlock.Lock()
		sleep_time := t.remaining_duration

		// less than zero (due to updateQOSTime decreasing the duration),
		// so just tick immediately
		if sleep_time < 0 {
			t.remaining_duration = time.Duration(0)
			t.tick_channel <- struct{}{}
			t.modlock.Unlock()
			return
		}

		// the current frequency is 0, so just tick immediately
		if t.current_frequency <= 0 {
			t.remaining_duration = time.Duration(0)
			t.tick_channel <- struct{}{}
			t.modlock.Unlock()
			return
		}

		t.modlock.Unlock()

		if sleep_time == 0 {
			sleep_time = t.current_frequency
		}

		timer := time.NewTimer(sleep_time)
		start := time.Now()

		select {
		case <-t.stopped_channel:
			t.finish()

			t.modlock.Lock()
			t.remaining_duration = sleep_time - time.Since(start)
			t.modlock.Unlock()

			if !timer.Stop() {
				<-timer.C
			}

		case <-t.pause_channel:
			t.modlock.Lock()
			t.remaining_duration = sleep_time - time.Since(start)
			t.modlock.Unlock()

			if !timer.Stop() {
				<-timer.C
			}

		case <-timer.C:
			t.modlock.Lock()
			t.remaining_duration = time.Duration(0)
			t.modlock.Unlock()

			select {
			case t.tick_channel <- struct{}{}:
			case <-t.stopped_channel:
			}

		}
	}()

	return true
}

func (t *QOSTicker) Pause() bool {
	select {
	case <-t.stopped_channel:
		return false
	default:
	}

	select {
	case t.pause_channel <- struct{}{}:
		// wait until the pause has been processed before returning
		t.running.Lock()
		t.running.Unlock()

		return true

	default:
		return false
	}
}

func (t *QOSTicker) ResetTimer() {
	paused := t.Pause()

	t.modlock.Lock()
	t.remaining_duration = 0
	t.modlock.Unlock()

	if paused {
		t.Start()
	}
}

func (t *QOSTicker) Stop() {
	t.stop.Do(func() {
		close(t.stopped_channel)

		t.running.Lock()
		t.finish()
		t.running.Unlock()
	})
}

func (t *QOSTicker) finish() {
	t.handle_stop.Do(func() {
		close(t.pause_channel)
		close(t.tick_channel)
	})
}

func (t *QOSTicker) UpdateQOS(latency time.Duration) {
	select {
	case <-t.stopped_channel:
		return
	default:
	}

	if latency < 0 {
		latency = 0
	}

	old_frequency := t.current_frequency

	paused := t.Pause()

	adjusted_time := time.Duration(t.ticker_multipler * float64(latency))

	diff := (adjusted_time - t.current_frequency) / 2

	t.modlock.Lock()

	t.current_frequency += diff

	if t.min_frequency > t.current_frequency {
		t.current_frequency = t.min_frequency
	}

	old_remaining := t.remaining_duration
	diff = t.current_frequency - old_frequency
	if t.remaining_duration > 0 {
		t.remaining_duration += diff
	}

	log.WithFields(log.Fields{
		"latency":       latency.String(),
		"old":           old_frequency.String(),
		"current":       t.current_frequency.String(),
		"target":        adjusted_time.String(),
		"remaining":     t.remaining_duration.String(),
		"old_remaining": old_remaining.String(),
		"paused":        paused,
	}).Trace("UpdateQOS times:")

	t.modlock.Unlock()

	if paused {
		t.Start()
	}

}
