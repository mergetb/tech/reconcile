package reconcile

import (
	"bytes"
	"fmt"
	"path"
	"reflect"
	"strings"

	log "github.com/sirupsen/logrus"
	"go.etcd.io/etcd/api/v3/mvccpb"
	"go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/types/known/timestamppb" // for .Now()

	storage "gitlab.com/mergetb/tech/shared/storage/etcd"
)

func NewTaskStatus(rm *ReconcilerManager, r *Reconciler, key string) *TaskStatus {

	return &TaskStatus{
		ReconcilerManager: rm.Manager,
		ReconcilerName:    r.Name,
		TaskKey:           key,
		Desc:              r.Desc,
		When:              timestamppb.Now(),
	}
}

func NewTaskStatusRaw(rec_manager, name, key, desc string) *TaskStatus {

	return &TaskStatus{
		ReconcilerManager: rec_manager,
		ReconcilerName:    name,
		TaskKey:           key,
		Desc:              desc,
		When:              timestamppb.Now(),
	}
}

// Use this when you only know the taskkey
func NewTaskStatusUnknown(key string) *TaskStatus {
	return &TaskStatus{
		TaskKey: key,
		When:    timestamppb.Now(),
	}
}

func NewTaskStatusFromTaskRecord(tr *TaskRecord) *TaskStatus {
	return &TaskStatus{
		TaskKey:           tr.Task,
		ReconcilerManager: tr.ReconcilerManager,
		ReconcilerName:    tr.ReconcilerName,
	}
}

// This is returned when there is some sort of error with the task, like if it's missing
func NewTaskErrorfStatus(key, format string, a ...interface{}) *TaskStatus {

	return &TaskStatus{
		TaskKey: key,
		Messages: []*TaskMessage{{
			Level:   TaskMessage_Warning,
			Message: fmt.Sprintf(format, a...),
		}},
		LastStatus:    TaskStatus_Error,
		CurrentStatus: TaskStatus_Error,
		When:          timestamppb.Now(),
	}
}

// This is returned when there is some sort of error with the task, like if it's missing
func NewTaskMissingStatus(tr *TaskRecord) *TaskStatus {

	message := TaskMessageWarning("")

	switch tr.KeyType {
	case TaskRecord_SingleKey:
		message = TaskMessageWarning("waiting for a reconciler to process this key...")

		if tr.ReconcilerManager != "" && tr.ReconcilerName != "" {
			message = TaskMessageWarningf(
				"waiting for /%s/%s to process this key...",
				tr.ReconcilerManager,
				tr.ReconcilerName,
			)
		}
	case TaskRecord_PrefixSpace:
		message = TaskMessageWarning("waiting for a reconciler to process this keyspace...")

		if tr.ReconcilerManager != "" && tr.ReconcilerName != "" {
			message = TaskMessageWarningf(
				"waiting for /%s/%s to process this keyspace...",
				tr.ReconcilerManager,
				tr.ReconcilerName,
			)
		}
	}

	return &TaskStatus{
		TaskKey:           tr.Task,
		ReconcilerManager: tr.ReconcilerManager,
		ReconcilerName:    tr.ReconcilerName,
		Messages:          []*TaskMessage{message},
		LastStatus:        TaskStatus_Pending,
		CurrentStatus:     TaskStatus_Pending,
		When:              timestamppb.Now(),
		Desc:              "Pending keys",
	}
}

// This is returned when there the status seems to be unresponsive
func NewTaskUnresponsiveStatus(tr *TaskRecord) *TaskStatus {

	message := TaskMessageWarning("")

	switch tr.KeyType {
	case TaskRecord_SingleKey:
		message = TaskMessageWarning("waiting for a reconciler to come back online...")

		if tr.ReconcilerManager != "" && tr.ReconcilerName != "" {
			message = TaskMessageWarningf(
				"waiting for /%s/%s to come back online...",
				tr.ReconcilerManager,
				tr.ReconcilerName,
			)
		}
	case TaskRecord_PrefixSpace:
		message = TaskMessageWarning("waiting for a reconciler to come back online...")

		if tr.ReconcilerManager != "" && tr.ReconcilerName != "" {
			message = TaskMessageWarningf(
				"waiting for /%s/%s to come back online...",
				tr.ReconcilerManager,
				tr.ReconcilerName,
			)
		}
	}

	return &TaskStatus{
		TaskKey:           tr.Task,
		ReconcilerManager: tr.ReconcilerManager,
		ReconcilerName:    tr.ReconcilerName,
		Messages:          []*TaskMessage{message},
		LastStatus:        TaskStatus_Pending,
		CurrentStatus:     TaskStatus_Pending,
		When:              timestamppb.Now(),
		Desc:              "Pending keys",
	}
}

// Storage ObjectIO interface implementation ==================================

func (ts *TaskStatus) Key() string {
	return ts.GetStatusKey()
}

func (ts *TaskStatus) Value() interface{} {
	return ts
}

// ignore the version of the key; we should always be able to write it
func (ts *TaskStatus) GetVersion() int64 {
	return -1
}

func (ts *TaskStatus) SetVersion(v int64) {
	ts.SelfVersion = v
	return
}

// EtcdTxTimestamped interface implementation =================================

func (ts *TaskStatus) WriteTimestamp() {
	ts.When = timestamppb.Now()
}

// Clone helper function ======================================================

func (ts *TaskStatus) Clone() *TaskStatus {
	return proto.Clone(ts).(*TaskStatus)
}

// Prev Helper Function

func (ts *TaskStatus) GetPrevAsGenericEtcdKey() *storage.GenericEtcdKey {
	return &storage.GenericEtcdKey{
		EtcdKey:        ts.TaskKey,
		EtcdValue:      ts.PrevValue,
		Version:        ts.TaskVersion,
		ModRevision:    ts.TaskRevision,
		CreateRevision: ts.TaskRevision,
	}
}

// Storage BufferedObject interface implementation ============================

func (ts *TaskStatus) AppendCreateOps(etcdCli *clientv3.Client, ops *storage.EtcdTransaction) error {

	if ts.TaskKey == "" {
		return fmt.Errorf("taskkey is empty")
	}

	if ts.HasUnknownReconciler() {
		return fmt.Errorf("cannot create task status without handler")
	}

	ops.Write(ts)

	return nil

}

func (ts *TaskStatus) AppendUpdateOps(etcdCli *clientv3.Client, ops *storage.EtcdTransaction) error {
	return ts.AppendCreateOps(etcdCli, ops)
}

func (ts *TaskStatus) AppendDeleteOps(etcdCli *clientv3.Client, ops *storage.EtcdTransaction) error {

	if ts.TaskKey == "" {
		return fmt.Errorf("taskkey is empty")
	}

	if ts.HasUnknownReconciler() {
		return fmt.Errorf("cannot delete task status without handler")
	}

	ops.Delete(ts)

	return nil

}

func (ts *TaskStatus) AppendReadOps(etcdCli *clientv3.Client, ops *storage.EtcdTransaction) error {

	if ts.TaskKey == "" {
		return fmt.Errorf("taskkey is empty")
	}

	// when reading task status with an unknown handler, we read all possible handlers for the key
	if ts.HasUnknownReconciler() {

		// read all possible statuses
		ops.ReadKeyWithOpt(
			ts.GetStatusPrefix(),
			clientv3.WithPrefix(),
		)

		// read all possible health keepalives
		ops.ReadKeyWithOpt(
			aliveRoot,
			clientv3.WithPrefix(),
		)

		// read the task key
		ops.Read(storage.NewGenericEtcdKey(ts.TaskKey, nil))
	} else {

		// read the status key
		ops.Read(ts)

		// read reconciler health keepalive
		ops.Read(
			NewReconcilerHealthFromStatus(ts).KeepAliveObject(),
		)

		// read the task key
		ops.Read(storage.NewGenericEtcdKey(ts.TaskKey, nil))
	}

	return nil
}

func (ts *TaskStatus) AppendReadOpsAsPrefix(etcdCli *clientv3.Client, ops *storage.EtcdTransaction) error {

	if ts.TaskKey == "" {
		return fmt.Errorf("taskkey is empty")
	}

	// just read everything possible for this key

	// read all possible statuses keys/handlers
	ops.ReadKeyWithOpt(
		ts.GetStatusPrefix(),
		clientv3.WithPrefix(),
	)

	// read all possible health keepalives
	ops.ReadKeyWithOpt(
		aliveRoot,
		clientv3.WithPrefix(),
	)

	// read all possible task keys
	ops.ReadKeyWithOpt(
		ts.TaskKey,
		clientv3.WithPrefix(),
	)

	return nil
}

func (_ *TaskStatus) ReadAllFromResponse(txn *clientv3.TxnResponse) (map[string]storage.EtcdObjectIO, error) {

	s_m, _, _, err := ReadAllStatusInfoFromResponse(txn)

	if err != nil {
		return nil, err
	}

	m := make(map[string]storage.EtcdObjectIO)
	for k, vs := range s_m {
		for _, v := range vs {
			m[k] = v
		}
	}

	return m, err
}

// returns:
//   - a map containing the statuses (with the task key as the key)
//   - a map containing the task key and value (as a GenericEtcdKey)
//   - a map containing the if we found the keepalive key
func ReadAllStatusInfoFromResponse(txn *clientv3.TxnResponse) (map[string][]*TaskStatus, map[string]*storage.GenericEtcdKey, map[string]bool, error) {

	// the status map
	s_m := make(map[string][]*TaskStatus)

	// the task key value map
	t_m := make(map[string]*storage.GenericEtcdKey)

	// the keepalive map
	k_m := make(map[string]bool)

	// find out what we read
	data := make(map[string]*mvccpb.KeyValue)

	for _, r := range txn.Responses {
		rr := r.GetResponseRange()

		if rr != nil {
			for _, kv := range rr.Kvs {
				data[string(kv.Key)] = kv
			}
		}
	}

	for k, kv := range data {
		if strings.HasPrefix(k, statusRoot) {
			t := new(TaskStatus)

			err := proto.Unmarshal(kv.Value, t)
			if err != nil {
				log.Warnf("failed to unmarshal task status, ignoring %s: %+v", k, err)
				continue
			}

			// check version and value of key
			g := &storage.GenericEtcdKey{
				EtcdKey:        t.TaskKey,
				EtcdValue:      nil,
				Version:        0,
				ModRevision:    0,
				CreateRevision: 0,
			}

			if task_response, ok := data[t.TaskKey]; ok {
				g = storage.NewGenericEtcdKeyFromKV(task_response)
			}
			akey := NewReconcilerHealthFromStatus(t).KeepAliveObject().Key()

			_, alive := data[akey]

			t.ComputeCurrentStatus(g, alive)

			s_m[t.TaskKey] = append(s_m[t.TaskKey], t)
			t_m[t.TaskKey] = g
			k_m[t.Key()] = alive
		}
	}

	return s_m, t_m, k_m, nil

}

// Singleton storage functions ================================================

func (ts *TaskStatus) Read(etcdCli *clientv3.Client) error {
	return storage.UnbufferedRead(etcdCli, ts)
}

func (ts *TaskStatus) Create(etcdCli *clientv3.Client) (*storage.EtcdRollback, error) {
	return storage.UnbufferedCreate(etcdCli, ts)
}

func (ts *TaskStatus) Update(etcdCli *clientv3.Client) (*storage.EtcdRollback, error) {
	return storage.UnbufferedUpdate(etcdCli, ts)
}

func (ts *TaskStatus) Delete(etcdCli *clientv3.Client) (*storage.EtcdRollback, error) {
	return storage.UnbufferedDelete(etcdCli, ts)
}

// Key helper functions =======================================================

func (ts *TaskStatus) GetStatusKey() string {
	return storage.SmartJoin(
		statusRoot,
		ts.TaskKey,
		ts.ReconcilerManager,
		ts.ReconcilerName,
	)
}

func (ts *TaskStatus) GetStatusPrefix() string {
	return storage.SmartJoin(
		statusRoot,
		ts.TaskKey,
	) + "/"
}

func (ts *TaskStatus) GetFullName() string {
	return path.Join(ts.TaskKey, ts.ReconcilerManager, ts.ReconcilerName)
}

func (ts *TaskStatus) CheckManagerAndName(rec_manager, rec_name string) bool {
	if ts == nil {
		return false
	}

	return ts.ReconcilerManager == rec_manager && ts.ReconcilerName == rec_name
}

func (ts *TaskStatus) GetReconcilerPath() string {
	if ts.HasUnknownReconciler() {
		return ""
	}

	return path.Join(
		"/",
		ts.ReconcilerManager,
		ts.ReconcilerName,
	)
}

// Various functions ==========================================================

// This only reads the task status and skips reading other the task key and the keepalive key
// This is only intended if you are manually writing statuses, as most normal reconciler statuses
// need to check the task key (and maybe the keepalive key) to determine the actual status
func (ts *TaskStatus) ReadRawStatus(etcdCli *clientv3.Client) error {

	if ts.TaskKey == "" {
		return fmt.Errorf("taskkey is empty")
	}

	ops := new(storage.EtcdTransaction)

	ops.Read(ts)

	_, _, err := ops.EtcdTx(etcdCli, "")
	if err != nil {
		return err
	}

	return nil
}

func (ts *TaskStatus) HasUnknownReconciler() bool {
	return ts.ReconcilerManager == "" || ts.ReconcilerName == ""
}

func (ts *TaskStatus) FullfillsTaskRecord(tr *TaskRecord) bool {
	if ts == nil || tr == nil {
		return true
	}

	matchesReconciler := tr.ReconcilerManager == ts.ReconcilerManager && tr.ReconcilerName == ts.ReconcilerName

	// if either are nil, we match any reconciler
	if tr.ReconcilerManager == "" || tr.ReconcilerName == "" {
		matchesReconciler = true
	}

	var matchesKey bool

	switch tr.KeyType {
	case TaskRecord_SingleKey:
		matchesKey = ts.TaskKey == tr.Task
	case TaskRecord_PrefixSpace:
		matchesKey = strings.HasPrefix(ts.TaskKey, tr.Task)
	default:
		panic(fmt.Errorf("unknown key type; %s", tr.KeyType))
	}

	return matchesKey && matchesReconciler
}

// Returns whether the status key is reconciled and the value is identical
// if version is negative, ignore the version
func (ts *TaskStatus) IsReconciled(ge *storage.GenericEtcdKey) bool {
	if ts == nil || ts.LastStatus != TaskStatus_Success {
		return false
	}

	// if we have a task revision, use it, otherwise fallback to version/value checking
	if ts.TaskRevision != 0 {
		return ts.TaskRevision == ge.ModRevision
	} else {
		return (ge.Version < 0 || ts.TaskVersion == ge.Version) &&
			bytes.Equal(ge.EtcdValue, ts.PrevValue)
	}

}

func (ts *TaskStatus) ComputeCurrentStatus(tk *storage.GenericEtcdKey, alive bool) {

	if ts == nil {
		return
	}

	// If the task key is deleted/missing, check if we deleted it
	if tk.Version <= 0 {
		if ts.LastStatus == TaskStatus_Deleted {
			ts.CurrentStatus = TaskStatus_Deleted
			return
		}

		ts.CurrentStatus = TaskStatus_Pending
		return
	}

	// don't have a revision, so fallback to version/value checking
	if ts.TaskRevision == 0 {
		if (ts.TaskVersion != tk.Version) || !bytes.Equal(tk.EtcdValue, ts.PrevValue) {
			ts.CurrentStatus = TaskStatus_Pending
			return
		}
	} else {
		if ts.TaskRevision != tk.ModRevision {
			ts.CurrentStatus = TaskStatus_Pending
			return
		}
	}

	if !alive {
		ts.CurrentStatus = TaskStatus_Unresponsive
		return
	}

	// Everything else passed, so just return it as is
	ts.CurrentStatus = ts.LastStatus
	return
}

func (ts *TaskStatus) RewriteCurrentMessage(tr *TaskRecord) *TaskStatus {

	if ts == nil {
		return ts
	}

	if ts.CurrentStatus == ts.LastStatus {
		return ts
	}

	switch ts.CurrentStatus {
	case TaskStatus_Pending:
		return NewTaskMissingStatus(tr)
	case TaskStatus_Unresponsive:
		return NewTaskUnresponsiveStatus(tr)
	}

	return ts
}

// determines to write status on ensure if:
//   - the previous status is not a success
//   - the current status is not a success
//   - the current status has a message that is warning or error
//   - the return message was undefined and the new status is different than the old status
func (ts *TaskStatus) updateStatusOkOnEnsure(m *TaskMessage, old *TaskStatus) bool {
	if ts == nil {
		return false
	}

	for _, m := range ts.GetMessages() {
		if m.GetLevel() == TaskMessage_Warning || m.GetLevel() == TaskMessage_Error {
			return true
		}
	}

	return ts.GetLastStatus() != TaskStatus_Success ||
		old.GetLastStatus() != TaskStatus_Success ||
		m != nil && m.Level == TaskMessage_Undefined && !ts.Equals(old)
}

func (ts *TaskStatus) Equals(other *TaskStatus) bool {
	// ignore Duration and When,
	// as they are timestamps and highly unlikely to ever be equal,
	// unless the task status was cloned/the same pointer

	td := ts.Duration
	od := other.Duration
	tw := ts.When
	ow := other.When

	ts.Duration = nil
	other.Duration = nil
	ts.When = nil
	other.When = nil

	ans := reflect.DeepEqual(ts, other)

	ts.Duration = td
	other.Duration = od
	ts.When = tw
	other.When = ow

	return ans
}

// Read all subtasks at once
func ReadAllTaskStatusSubkeys(etcdCli *clientv3.Client, subkeys []string, cache *storage.EtcdCache) (map[string][]*TaskStatus, error) {
	ops := new(storage.EtcdTransaction)
	found_map := make(map[string]bool)

	for _, c := range subkeys {

		// when reading task status with an unknown handler, we read the entire space
		ts := NewTaskStatusUnknown(c)
		ts.AppendReadOps(etcdCli, ops)

		// initialize everything to false, we will set them to true later
		found_map[c] = false
	}

	txr, _, err := ops.EtcdTxWithOptions(etcdCli, storage.EtcdTransactionOptions{Cache: cache})
	if err != nil {
		return nil, err
	}

	sub_statuses, _, _, err := ReadAllStatusInfoFromResponse(txr)
	if err != nil {
		return nil, err
	}

	ans := make(map[string][]*TaskStatus)

	for k, tses := range sub_statuses {

		for _, ts := range tses {
			k = ts.TaskKey

			found_map[k] = true

			ans[k] = append(ans[k], ts)
		}

	}

	for _, k := range subkeys {
		if !found_map[k] && !IsMonitored(k) {
			ans[k] = append(ans[k],
				NewTaskMissingStatus(
					&TaskRecord{
						Task: k,
					},
				),
			)
		}

	}

	return ans, nil
}
