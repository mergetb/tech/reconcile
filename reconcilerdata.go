package reconcile

import (
	"go.etcd.io/etcd/client/v3"

	storage "gitlab.com/mergetb/tech/shared/storage/etcd"
)

type TaskData struct {
	Status    *TaskStatus             // during a task handler, this will be the task status before the handler is called
	TaskValue *storage.GenericEtcdKey // Current key/value we are reconciling against

	cli               *clientv3.Client   // etcd client
	reconciler        *Reconciler        // Task used to create this
	reconcilerManager *ReconcilerManager // Task manager this belongs to
}

func NewTaskData(taskvalue *storage.GenericEtcdKey, rm *ReconcilerManager, r *Reconciler) *TaskData {

	return &TaskData{
		Status:            NewTaskStatus(rm, r, taskvalue.Key()),
		TaskValue:         taskvalue,
		cli:               rm.EtcdCli,
		reconciler:        r,
		reconcilerManager: rm,
	}
}

func NewTaskDataRaw(etcdCli *clientv3.Client, taskvalue *storage.GenericEtcdKey, rec_manager, rec_name string) *TaskData {

	return &TaskData{
		Status:            NewTaskStatusRaw(rec_manager, rec_name, taskvalue.EtcdKey, ""),
		TaskValue:         taskvalue,
		cli:               etcdCli,
		reconciler:        &Reconciler{Name: rec_name},
		reconcilerManager: &ReconcilerManager{Manager: rec_manager, EtcdCli: etcdCli},
	}
}

func (td *TaskData) getCachedTaskStatusOrDefault() {

	new_ts, found := td.reconciler.ensuredTaskStatuses.Load(td.Status.TaskKey)
	td.Status = new_ts

	if !found {
		td.Status = NewTaskStatus(td.reconcilerManager, td.reconciler, td.TaskValue.EtcdKey)
	}
}

func (td *TaskData) EtcdClient() *clientv3.Client {
	return td.cli
}

func (td *TaskData) AddWrittenKey(key string) bool {

	td.getCachedTaskStatusOrDefault()

	found := false
	for _, k := range td.Status.SubtaskKeys {
		if k == key {
			found = true
			break
		}
	}

	if !found {
		td.Status.SubtaskKeys = append(td.Status.SubtaskKeys, key)
	}

	return !found
}

// Update the task with an error.
func (td *TaskData) SetError(err *TaskMessage) {
	td.SetStatusClearMessages(
		TaskStatus_Error,
		err,
	)
}

// Update the task as processing, for handling puts
func (td *TaskData) SetProcessingHandlePut() {
	td.SetStatusClearMessages(
		TaskStatus_Processing,
		TaskMessageInfo("processing put..."),
	)
}

// Update the task as complete.
func (td *TaskData) SetComplete() {
	td.SetStatusClearMessages(
		TaskStatus_Success,
		TaskMessageInfo("task complete"),
	)
}

// Update the task as pending.
func (td *TaskData) SetPending() {
	td.SetStatusClearMessages(
		TaskStatus_Pending,
		TaskMessageWarning("task pending"),
	)
}

// Update the task as deleted.
func (r *TaskData) SetDeleted() {
	r.SetStatusClearMessages(
		TaskStatus_Deleted,
		TaskMessageInfo("task deleted"),
	)
}

// Update the status
func (td *TaskData) SetStatusAppendMessages(s TaskStatus_StatusType, info *TaskMessage) {

	td.Status.TaskVersion = td.TaskValue.Version
	td.Status.PrevValue = td.TaskValue.EtcdValue
	td.Status.TaskRevision = td.TaskValue.ModRevision

	td.Status.LastStatus = s
	td.Status.Messages = append(td.Status.Messages, info)
}

func (td *TaskData) SetStatusClearMessages(s TaskStatus_StatusType, info *TaskMessage) {
	td.Status.TaskVersion = td.TaskValue.Version
	td.Status.PrevValue = td.TaskValue.EtcdValue
	td.Status.TaskRevision = td.TaskValue.ModRevision

	td.Status.LastStatus = s
	td.Status.Messages = []*TaskMessage{info}
}
