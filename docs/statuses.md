## Status Values
Here is what each status value means, listed in order of increasing priority:
| Value | Description |
|-------------:|-----------------------------------------------------------------|
| Success | This task's desired state is met. |
| Pending | The task is not currently being actively worked upon. |
| Unresponsive | The service responsible for this task seems to be unresponsive. |
| Processing | A service is actively working on this task. |
| Error | When a service was working on this task, it failed. |
| Undefined | If you see this, there's a bug in the implementation. |

## Goals
Although it's fairly easy to get the status of any individual task,
emergent behavior depends on a whole collection of tasks being complete.

Goals are a way to keep track of this, while also considering that tasks can spawn other tasks.
Fundamentally, goals are just a list of tasks to look at, with extra useful metadata included.

With this top down approach, the individual reconcilers do not need to know about which goals they are in,
just that they need to report their individual results.

As a result of this decision, a status tree is not a singular object, but an empheral state of the system,
which needs to be re-traversed whenever a status changes.

Thus, `Goals` represent the tasks we want to change, but does not in itself represent a status tree.
The actual status trees are encoded in `TaskForests` and `TaskTrees`, which are computed from a goal.

### Goal Types

There are two types of goals:
 - one whose children are other goals (`Supergoals`)
 - one whose children are tasks (`Supertasks`)

In the future, if we want goals that have both types of children,
this can be done, but for now, these are the two types of goals.

When reading a goal's status tree, it will recursively descend through its children,
whether they are other goals or tasks.

There are two types of status trees, mostly due to technical type reasons.
The first is the `TaskTree`. A `TaskTree` is a status tree produced from an individual task.
It was designed for uses like if an `apiserver` writes `user/bob`, which spawns `filesystem/bob`.
With this method of accounting, the `apiserver` does not have to know in advance that `user/bob` will write `filesystem/bob`,
and `filesystem/bob` does not need to know who wrote that key.
Most `TaskTrees` just contain their own status though, as most tasks do not spawn a subtask.

The second is the `TaskForest`. A `TaskForest` is a status tree produced from a goal.
Unlike a goal, it can have both goals and tasks as children.
A `TaskForest` does not match up exactly with the goal that it's produced from.
The reason is because individual tasks under `TaskForest` become grouped by reconciler if there are multiple of them.

TODO: Goals have the ability to account for keys need to be tracked,
but should also be extended to also declare which reconcilers will handle those keys.
