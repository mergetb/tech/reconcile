# Implementation Core Ideas
## pworkerpool and task scheduling
At the center of the implementation is a `pworkerpool`,
a gently modified version of [workerpool](https://github.com/gammazero/workerpool),
to support a priority queue instead of a regular queue.
In short, `pworkerpool` is a concurrency limiting goroutine pool that also supports assigning tasks a priority.
Priority is interpreted as least-first, so a task with priority 0 executes before a task with priority 10.

There is one primary `pworkerpool` for each reconciler manager.
This is how each reconciler under a reconciler manager shares its CPU resources with each other.
When task keys arrive, they are queued into `pworkerpool` as they patiently wait for their turn to execute.
The priority of task keys is based upon their revision and their order when received in the etcd watch (`util.go:composePriority`).
The reason for this is that tasks should be executed in the order that they've been received in.
Due to the data structure used for the priority queue being a heap,
tasks with an identical priority would not be processed in order,
which is why revision cannot be used alone, and we append the index to it.

When the number of tasks keys is greater or equal to the number of workers,
even a normal task queue would be sufficient, but things get more complicated when this condition fails.
Consider this sequence, written in `key`:`time taken to execute` pairs:
 - Write `/sleep/a`:`1000ms` @ revision 1
 - Write `/sleep/a`:`0010ms` @ revision 2

If we have 1 worker, then this will be executed in order.
But if we have 2 or more workers, then under the implementation described so far,
both revision 1 and 2 would be executed at the same time and revision 2 would finish before revision 1,
breaking the requirement that all task keys be handled in the order received.

Basically then, what we need is a lock per key,
with the added condition that only the lowest revision task can take the lock.
Additionally, waiting for a lock shouldn't block the execution of other task keys.

The implementation that ended up fulfilling these is the function is at `reconcilermanager.go:submitTaskWithKey`.
In short, we also keep a heap of revisions for each key.
Before we are to execute on a task key, we check to see if:
 - Is our revision at the front of the key's revision heap?
   - If not, requeue ourselves back in the `pworkerpool` after a small wait
     - This is to prevent the blocking of other tasks while waiting to be the lowest revision
     - Since this is a busy wait algorithm, the small wait is so we don't hammer CPU
   - If so, execute, and afterwards, remove ourselves from the start of the heap

## Buffered Status Writing
As it turns out, a significant aspect of task throughput is writing the returned task statuses to etcd.
To help throughput, we buffer the returned task statuses and flush when:
 - There is a buffered task status with the same key as an incoming task status
   - This is because etcd does not allow you to have a single transaction in which a key is written multiple times
 - The buffer runs out of space
 - There are no other task statuses to wait for
 - We go a small amount of time without buffering a task status
   - This time is based upon the latency it takes to write to etcd

To make accesses to the buffer thread safe, instead of actually making the buffer thread safe,
we use a `pworkerpool` with 1 worker to append and flush it,
with the priority being incremented once per appended task status (so things are added in order),
and the flushing operation having the most delayed priority.

This makes a tradeoff between the latency of an individual status return and the throughput of all statuses.

## QOSTicker
Since we have periodic events that we need to handle (pollcheck, ensure, health),
we need a way to trigger periodic events. In addition, as these periodic events themselves take time,
we want a way to adjust the amount of time between events for QOS reasons.
For example, the amount of time that ensure takes is dependent on the number of keys present,
which cannot really be predicted at build time.
Also, being able to Pause the ticker is useful.

QOSTicker is ultimately a wrapper around around `time.Timer` with extra stuff so it supports those features.

## Heartbeat
Sometimes, etcd watching kind of hangs, especially around transient connection errors.
So, heartbeats forces the reconciler to be able to watch for a key's changes.
If we cannot observe changes to the key, then we terminate, since that implies other issues.
Note when restarting etcd or whatever writes the heartbeat key,
expect the reconcilers to also restart.

## Keeping Accurate Counts
Keeping an accurate count of what's currently executing is very useful.
The main counts are:
 - Number of status keys to expect to write
 - Number of remaining tasks keys to execute (per reconciler)

These are used to:
 - Quickly flush status keys, flush all statuses before stopping
 - Update as Idle, execute everything remaining before stopping

Sometimes, these counts are `sync.WaitGroup`, sometimes these counts are atomic integers.
`sync.WaitGroup` is not always used because in order for it to work,
you (must call)[https://pkg.go.dev/sync#WaitGroup.Add] all the relevant `wg.Add(x)` before doing a `wg.Wait()`,
which isn't always the case as task keys can randomly arrive.

# Overview
There's three main states that a `ReconcilerManager` will be in:
 - Initialization
 - Task Watching
 - Termination

The pseudo code snippets differ from the actual code by:
 - omitting error checking
 - the specific details of exact data structures
 - implementations of things described in the previous section, namely:
   - status buffering
   - priority queue workerpool
   - timers
   - logging

## Initialization

The goal of initialization is to make sure that the reconciler is
up to date with the latest revision of etcd.

So, it reads the key spaces of each reconciler and does the callbacks for each reconciler:
 - For tasks that have a higher revision than last handled, it `Creates/Updates` them
   - This potentially includes a `Delete`, if the CreateRevision is higher than last handled
 - For tasks that have the same revision as last handled, it `Ensures` them
 - For tasks that have been handled, but are missing from etcd, it `Deletes` them.

These initialization callbacks are queued;
key watches will be handled at a higher priority than its initialization callback,
and the initialization callback will be skipped.

Note that if a delete key watch is observed, but the key has not been handled yet,
both the `Delete` callback and the initialization `Create` callback will be skipped.
This is intended to be used when your reconciler handles a large key space and ensuring everything on startup takes a while.

The code is approximately as follows:
```go
// this is psuedo code written in go style
// the actual code isn't structured liked this

func (rm *ReconcilerManager) Run() {
  for _, reconciler := range rm.Reconcilers {
    // read task and previous statuses
    task := rm.EtcdCli.ReadPrefix(r.Prefix)
    rm.statuses := rm.EtcdCli.ReadStatuses(r.Prefix)

    // we found a bunch of tasks, check if they're up to date
    for _, t := range task {

      f = func() {
        if reconciler.Parse(t.Key) {

          // the last handled revision is lower than the new create revision,
          // so it was deleted while we were not looking
          if rm.statuses[t] != nil && rm.statuses[t].Revision < t.CreateRevision {
            rm.statuses[t] = reconciler.Delete(t)
          }

          switch {
             // this can happen if we handled a newer task already
            case rm.statuses[t].Revision > t.Revision:
              // do nothing
            case rm.statuses[t].Revision == t.Revision:
              rm.statuses[t] = reconciler.Ensure(t)
            case rm.statuses[t].Revision < t.Revision:
              if t.Version == 0 || rm.statuses[t] == nil {
                rm.statuses[t] = reconciler.Create(t)
              } else {
                rm.statuses[t] = reconciler.Update(t)
              }
          }

          rm.statuses[t].WriteToEtcd()
        }
      }

      rm.pool.Submit(f)
    }

    // the set difference between the previous statuses of tasks we found
    // and the actual tasks we found
    deletedtasks = set(statuses - tasks)


    for _, t := range deletedtasks {
      f = func() {
        if reconciler.Parse(t.Key) {
          reconciler.Delete(t)
        }
      }

      rm.pool.Submit(f)
    }
  }

  // continues on...

}
```

## Task Watching

This is the main loop of the reconciler.
Here, we handle all maintainence tasks and key watches.

```go
// this is psuedo code written in go style
// the actual code isn't structured liked this

func (rm *ReconcilerManager) Run() {
  // continues on after init stuff

  // events are periodically sent to the event queue,
  for c := <- rm.eventqueue {
    switch c.eventType {
      case ReconcilerManagerEventStop:
        goto end

      case ReconcilerManagerEventEnsure:
        rm.handleEnsureEvent()

      case ReconcilerManagerEventPollCheck:
        rm.handlePollCheck()

      case ReconcilerManagerEventHealth:
        rm.handleHealthEvent()

      case ReconcilerManagerEventEtcdWatch:
        rm.pool.Submit(rm.handleKeyEvent(c.Task, c.Reconciler))
    }
  }

  end:
  // continues on...
}

func (rm *ReconcilerManager) handleKeyEvent(t *Task, r *Reconciler) {
  if t.Task.WasDeleted {
    rm.statuses[t] = r.Delete(t)
  }

  if t.Version == 0 || rm.statuses[t] == nil {
    rm.statuses[t] = reconciler.Create(t)
  } else {
    rm.statuses[t] = reconciler.Update(t)
  }

  rm.statuses[t].WriteToEtcd()
}
```

## Termination
Termination just waits for everything running to stop.
During termination, since we're not watching events anymore,
no more events can be queued.

Since a lot of things rely on golang channels,
a lot of code relies on the termination of the channels to indicate termination.
You have to be careful around how you close them to avoid channel deadlock/after close use.
