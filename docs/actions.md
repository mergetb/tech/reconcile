## Action Interface Semantics
To use the reconciler package, you must create a type that fulfills the `Actions` interface.
The `Actions` interface has the following methods:
```go
type Actions interface {
	Parse(string) bool
	Create([]byte, int64, *TaskData) *TaskMessage
	Update([]byte, []byte, int64, *TaskData) *TaskMessage
	Delete([]byte, int64, *TaskData) *TaskMessage
	Ensure([]byte, []byte, int64, *TaskData) *TaskMessage
}
```

Note that for the standard methods, the `*TaskData` argument is a pointer,
so changes to it will be read by the reconciler engine.

The `*TaskData` argument contains extra information that can be useful
for an `Actions` implementation:
```go
type TaskData struct {
	Status      *TaskStatus             // during a task handler, this will be the task status before the handler is called
	TaskValue   *storage.GenericEtcdKey // Current key/value/version/revision we are reconciling against
}
```

For `Create/Update/Ensure/Delete`, the reconcile engine interprets the returned `*TaskMessage` as the status result.
The reason why `Create/Update/Ensure/Delete` do not directly return a `TaskStatus` by default is because the struct itself
contains a lot of information that needs to be properly set and the vast majority of `Actions` implementations just need to
return success or failure.

`Create/Update/Ensure/Delete` all interpret the returned `*TaskMessage` in the following way:
  - nil: assumes that it was completed successfully
  - of level Error: assumes that it had an error
  - of level Warning/Info/Debug/Trace: assumes that it was completed successfully
  - of level Undefined: tells the reconcile engine to not interpret anything and instead use the value in `(*TaskData).Status` directly as the status

### Undefined *TaskMessage Return Value Semantics
This is behavior is for two reasons:
 1. Reconcilers that need to set the status as something other than the binary of success or failure can do so
 2. Reconcilers with something unimplemented (most commonly `Ensure`) can use this to tell the reconciler engine to use the previous status key as the "correct" status key
   - This works if the implementation does not touch `(*TaskData).Status` at all, since its default value is the previous status key.

If you want to manually set the status, it is highly advised to use the `(*TaskData).SetStatusClearMessages` method, as this also updates the status to be considered "seen."
Failure to do so can result in unexpected behavior during callbacks.

### Parse Semantics
When a potential key (based upon the set `Prefix` in `Task`) comes along, `Parse` is used to check if the `Actions` implementation wants to handle that key.
The key is passed as the string and the boolean return is whether the implementation wants to handle that key.

If you want, you can store data during `Parse` in the type; it will be available during future calls of `Create/Update/Ensure/Delete`, even when using multiple workers.

### Create/Update Semantics
Create/Update are called when all of the conditions are met:
 - One of the following:
   - A key is written to `etcd`.
   - During startup, `etcd` is read for all possible keys in the `Task`'s prefix space and keys are present.
   - During periodic ensure (if a previous returned `*TaskMessage` was `Undefined` and the status was not properly updated).
 - `Parse` returned true
 - One of the following:
   - The task key has not been previously seen.
   - The previously seen task key revision is less than the incoming task key revision.
 - Create is called when the version of the task key is 0 or the task key has not been previously seen.
 - Update is called when the version of the task key is greater than 0 and has been previously seen.

Note that before execution `Create/Update,` if there's a unhandled deleted, `Delete` will be called first.
An unhandled deleted is defined as the current task create revision being higher than the previously handled task revision.

When the returned `*TaskMessage` is not `Undefined`, the key will be considered to be seen, regardless of the actual outcome.
Then, `Create/Update` will only be called once per key write.

When the returned `*TaskMessage` is `Undefined`, whether the key is considered to be seen depends on whether `(*TaskData).Status` was changed.
If it was updated properly (generally through the `(*TaskData).SetStatusClearMessages` method), then it will be considered to be seen.

If it was not updated, then the key will be considered to be unseen.
This would be the desired behavior if you want to tell the reconciler engine that `Create/Update` is specifically unimplemented.
As a result of this, `Create/Update` may be called multiple times per key write if `Undefined` is returned.

Regardless of returned `*TaskMessage`, the task key will be registered for periodic ensure.

### Delete Semantics
Delete is called when all of the conditions are met:
 - One of the following:
   - A key is deleted from `etcd.`
   - During startup, `etcd` is read for all possible keys in the `Task`'s prefix space and a previously handled key is not present.
   - Before `Create/Update`, if the read task key's create revision is higher than the previously handled task revision.
   - During periodic ensure.
 - `Parse` returned true.
 - The task key has been previously seen.
 - The previously seen task key has not yet been successfully deleted.

The returned `*TaskMessage` matters here.
Until the returned value is interpreted as a success, during any of the potential triggers, the reconciler engine will try to delete the key.

The key will be unregistered from periodic ensure if and only if:
 - The returned value of `*TaskMessage` is interpreted to be a success.
 - The returned value of `*TaskMessage` is `Undefined`, regardless of `(*TaskData).Status`.

This has the potential to be most annoying during periodic ensures and startup.

### Ensure Semantics
Ensure is called when all of the conditions are met:
 - One of the following:
   - During startup, `etcd` is read for all possible keys in the `Task`'s prefix space and a key is present.
   - During periodic ensure.
 - `Parse` returned true.
 - The task key has been previously seen.
 - The previously seen task key's revision is identical to task key's revision in etcd.

Unlike `Create/Update/Delete`, `Ensure` will not always explicitly write the status into etcd.
This is because of wanting to avoid uneccessary `etcd` writes during periodic ensures, which can be very frequent.
If a reconciler is managing 100's of keys, then flooding etcd with writes every minute is not a good thing.

Statuses are written if any of the conditions are met:
  - the previous status is not a success
  - the current status is not a success
  - the returned `*TaskMessage` is warning, or error
  - the returned `*TaskMessage` is undefined and the previous status is different than the current status

The general case, in which the previous status is a success and the current status is a success,
is the case in which status messages are most often skipped.

The Warning case is a special case for Ensure.
Because Warning is still interpreted as a success, but is still always written,
it's useful to use to note the last time Ensure recovered from an error.

If future Ensures return a `*TaskMessage` of level info or higher,
then the previously written Warning message will not be overwritten.

### In Progress Status Updating
You might want to update the status before returning, so users can see what the reconcilers is currently doing on a task.
This is most useful for reconcilers that take a while, especially reconcilers that have to download something.

To do so, simply perform the following:
```go
// assumes that the *TaskData in the function argument was named td
// and the reconcile package was called rec
td.SetStatusClearMessages(
  rec.TaskStatus_Processing,
  rec.TaskMessageInfo("doing something long..."),
)
_, err = td.Status.Update(storage.EtcdClient)

if err != nil {
    log.Warnf("unable to set status to processing: %+v", err)
}
```

This can be done at any time, even after you've returned from the `Action` interface methods.
You can also use `td.SetStatusClearMessages` to manually set the status before returning an `Undefined` TaskMessage.

### Multiple messages
You might want to return multiple messages with different values instead of a single message.

Here's an example of doing this:
```go
// assumes that the *TaskData in the function argument was named td
// and the reconcile package was called rec
td.SetStatusClearMessages(
	rec.TaskStatus_Success,
	rec.TaskMessageWarning("subprocess a had a recovered error"),
)

// note that this function is different
td.SetStatusAppendMessages(
	rec.TaskStatus_Success,
	rec.TaskMessageWarning("subprocess b had a recovered error"),
)

return rec.TaskMessageUndefined()
```
