## Reconciler Manager Options

A reconciler manager represents a collection of reconcilers that share some common configuration and resources (like number of workers).
When creating a task manager, here are the options:

|                     Option |             Type | Default Value | Value Notes                                                 | Meaning                                                                                                                           | Notes                                                                    |
|---------------------------:|-----------------:|:-------------:|-------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------|
|                    Manager |           string |               | Required. Cannot have the `/` character in it.              | The base name of the reconciler.                                                                                                  | Typically, this is often what the reconciler's name is, like  `canopy` . |
|                    EtcdCli | *clientv3.Client |               | Required.                                                   | The etcd client to use.                                                                                                           |                                                                          |
|                Reconcilers |    []*Reconciler |               | At least 1 reconciler is required.                          | The list of reconcilers to run.                                                                                                   |                                                                          |
|            HealthFrequency |    time.Duration |   5 minutes   | A negative value means to never periodically check in.      | How often for a reconciler to perform a health check with etcd.                                                                   |                                                                          |
| ServerHeartbeatGracePeriod |    time.Duration |   30 seconds  | A negative value means to disable heartbeat checking.       | How long until a heartbeat is missed until we assume the server is dead and abort                                                 |                                                                          |
|                   LogLevel | TaskMessage_Type |    Warning    |                                                             | The reconciler engine will automatically log returned TaskMessages with a log level at least this high.                           | Messages with level undefined are never logged.                          |
|      DisableMessageLogging |             bool |     False     |                                                             | Disable automatic TaskMessage logging, if you want to do it yourself.                                                             |                                                                          |
|                    Workers |              int |       1       | A negative value will be set to NUM_CPU workers at runtime. | How many workers to run updates simultaneously. Tasks under the same TaskManager share a single worker pool.                      |                                                                          |
|              JustLogErrors |             bool |     False     |                                                             | When false, when the reconcile engine encounters an engine-level error, it logs the error with `Fatalf`. Otherwise, just logs it. | Use this if you want to caller to handle `Fatal` errors.                 |

## Reconciler Options
A reconciler represents a single reconciler operating on a specific key spaces.

|   |                   Option |          Type | Default Value | Value Notes                                    | Meaning                                                                                                                                 |
|---|-------------------------:|--------------:|:-------------:|------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------|
|   |                  Actions |       Actions |               | Required.                                      | The type that implements the action interface, so that the reconcile engine can use these callbacks.                                    |
|   |                     Name |        string |               | Required. Cannot have the `/` character in it. | The name of the specific task. This could either be the specific subtask or the host of the task.                                       |
|   |                     Desc |        string |               |                                                | The description of the task. This appears in status trees when multiple task keys are grouped under a single task.                      |
|   |                   Prefix |        string |               | Required.                                      | The key prefix space the task watches for, at a base level. (The key space is further refined with the `Actions.Parse` implementation.) |
|   |          EnsureFrequency | time.Duration |       0       | A value of 0 disables this.                    | How often to periodically check the task's keys. Usually, this means to call `Actions.Ensure` on every key at this frequency.           |
|   |       PollCheckFrequency | time.Duration |   15 Minutes  | A negative value disables this.                | How often to poll the task's keys to see if the last handled keys are actually up to date. If this fails, the reconcile engine aborts.  |
|   | PeriodicMessageFrequency | time.Duration |    1 Minute   | A negative value disables this.                | While `Actions.Create/Update/Ensure/Delete` are running,  you will get a warning message at this frequency about its current runtime.   |

## Notes about reconciler names
The full name of a reconciler is considered to be `${ReconcilerManager.Manager}/${Reconciler.Name}`.
Semantically, reconcilers with the exact same full name are considered to be the same reconciler.
This means that if you have different instances of a reconciler running across multiple machines (and the machines are logically distinct),
each reconciler must have a distinct name.
For example, `canopy/x0`, where `x0` is the host machine will work.

Currently, having the same multiples of the same reconciler running (aka horizontal scaling) is not supported.

## EnsureFrequency Notes
If `20 * periodicensuretime` > `EnsureFrequency`, then `EnsureFrequency` becomes a moving average of `20 * periodicensuretime`.
The reason for this is because you cannot really predict how long it will take to ensure all keys
(because it's dependent on the number of keys), this ensures that periodic ensures that 95% of the time is devoted to key handling, for QOS reasons.

In addition, if a task is received during a periodic ensure starts,
the task will handled with higher priority than its ensure, and its ensure will be skipped for that periodic ensure.

## Observing What Action The Reconciler Is Currently Doing
You may want to observe what action the reconciler is doing, especially for testing purposes.
With `Reconciler.WaitUntil` or `ReconcilerManager.WaitAllUntil` methods, they will return once any of the passed `ReconcilerOp` state transitions occur.
The `ReconcilerOps` are as follows:
|                 Option | Meaning                                                        |
|-----------------------:|----------------------------------------------------------------|
| ReconcilerOp_Undefined | Not used                                                       |
|      ReconcilerOp_Idle | The reconciler has finished all available work and is now idle |
|      ReconcilerOp_Init | The reconciler has begun to initialize                         |
|    ReconcilerOp_Create | The reconciler started an `Actions.Create` operation           |
|    ReconcilerOp_Update | The reconciler started an `Actions.Update` operation           |
|    ReconcilerOp_Delete | The reconciler started an `Actions.Delete` operation           |
|    ReconcilerOp_Ensure | The reconciler started an `Actions.Ensure` operation           |
|    ReconcilerOp_Health | The reconciler started a periodic `HealthCheck` operation      |
| ReconcilerOp_Pollcheck | The reconciler started a periodic `Pollcheck` operation        |

During testing, the most used one is waiting for `ReconcilerOp_Idle` immediately after calling `rm.Run()`.
This means that the reconciler has finishing initializing, so it's now it's handling keys received from etcd's watch feature.

Note that this currently only supports one waiter at a time.
You can, however, wait for multiple `ReconcilerOps`. When any of the targets are reached, it exits.
