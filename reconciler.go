package reconcile

import (
	"context"
	"fmt"
	"reflect"
	"sync"
	"sync/atomic"
	"time"

	"github.com/jinzhu/copier"
	"github.com/puzpuzpuz/xsync/v3"
	"github.com/yourbasic/radix"
	"go.etcd.io/etcd/client/v3"
	"google.golang.org/protobuf/types/known/timestamppb" // for .Now()

	"gitlab.com/mergetb/tech/reconcile/pkg/altsync"
	workerpool "gitlab.com/mergetb/tech/reconcile/pkg/pworkerpool"
	"gitlab.com/mergetb/tech/reconcile/pkg/qosticker"
	storage "gitlab.com/mergetb/tech/shared/storage/etcd"
)

type Actions interface {

	// Parse well get a candidate key string and the implementation
	// must decide if the key is one that it is interested in. If so,
	// the method should return true. And optionally store any needed information
	// about the key its own struct. Then it will reference that data
	// in Create/Update/Delete/Ensure calls. Parse() is always called first, so any stored
	// data will be available.
	Parse(string) bool

	// This key is newly created.
	Create([]byte, int64, *TaskData) *TaskMessage

	// This key had already existed.
	Update([]byte, []byte, int64, *TaskData) *TaskMessage

	// This key has been deleted.
	Delete([]byte, int64, *TaskData) *TaskMessage

	// Ensure that the key is still valid.
	// This is used when we want to do a handlePut, but the key is already reconciled (so on startup)
	// This *needs* to be idempotent to work properly.
	Ensure([]byte, []byte, int64, *TaskData) *TaskMessage
}

type Reconciler struct {

	// things that implement Task, need to supply these actions.
	Actions Actions

	Name   string // The name of this task
	Desc   string // Task description
	Prefix string // etcd key prefix to watch

	// How often to check the status the reconciler's task
	// A value of 0 means to not check it periodically.
	// The actual EnsureFrequency will depend on how long it takes to ensuring its key space, for QOS reasons
	EnsureFrequency time.Duration

	// How often to poll the reconciler's task to see if the latest reconciled tasks are the same as what's in Etcd
	// A value of < 0 means to not check it periodically
	// A value of 0 defaults to the package default
	PollCheckFrequency time.Duration

	// If Create/Update/Ensure/Delete take too long, a warning will be printed
	// A value of 0 will default to the package default `defaultPeriodicMessageFrequency`
	PeriodicMessageFrequency time.Duration

	// Set by the task engine.
	cancel context.CancelFunc // cancel the channel used to watch keys

	// task status related stuff
	ensuredTaskStatuses *xsync.MapOf[string, *TaskStatus]
	ensuredTaskValues   *xsync.MapOf[string, *storage.GenericEtcdKey]
	handledRevisions    *xsync.MapOf[string, revisionHistory]

	// runtime related stuff
	taskQueues   *xsync.MapOf[string, *taskQueue]
	poolWg       altsync.WaitGroup
	runningTasks atomic.Int64

	// task channel related stuff
	reconcilerChannelInit     sync.Once
	reconcilerChannel         *reconcilerChannel
	reconcilerIdleStatusMutex sync.Mutex

	// stats/health related values
	startTime      *timestamppb.Timestamp
	keysHandled    atomic.Int64
	lastKeyHandled atomic.Value // string
	whenLastKey    atomic.Value // timestamp

	// used to track what was recently deleted for poll check
}

type ReconcilerManagerEventType int

const (
	ReconcilerManagerEventNone ReconcilerManagerEventType = iota
	ReconcilerManagerEventStop
	ReconcilerManagerEventEtcdWatch
	ReconcilerManagerEventEnsure
	ReconcilerManagerEventPollCheck
	ReconcilerManagerEventHealth
)

type reconcilerHandlerEvent struct {
	eventType         ReconcilerManagerEventType
	etcdWatchResponse *clientv3.WatchResponse
	reconcilerTicker  *reconcilerTicker
	reconcilerOrigin  int
}

type reconcilerTicker struct {
	eventType        ReconcilerManagerEventType
	ticker           *qosticker.QOSTicker
	reconcilerToTick int
}

type reconcilerChannel struct {
	lastOp   *atomic.Int32
	taskChan chan ReconcilerOp
	stop     chan struct{}
	lock     sync.RWMutex
}

type taskQueue struct {
	workerpool.PriorityQueue[uint64]
	lock sync.Mutex
}

type revisionHistory struct {
	revision int64
	deleted  bool
}

func (r *Reconciler) Same(other *Reconciler) bool {
	if r == nil || other == nil {
		return false
	}

	return r.Name == other.Name && r.Prefix == other.Prefix
}

func (r *Reconciler) String() string {
	return fmt.Sprintf("[%s/%s]", r.Name, r.Prefix)
}

func (r *Reconciler) Stop() {
	r.cancel()
	r.poolWg.Wait()
	r.ensuredTaskValues.Range(func(key string, _ *storage.GenericEtcdKey) bool {
		r.ensuredTaskValues.Delete(key)
		return true
	})
	r.ensuredTaskStatuses.Range(func(key string, _ *TaskStatus) bool {
		r.ensuredTaskStatuses.Delete(key)
		return true
	})
	r.handledRevisions.Range(func(key string, _ revisionHistory) bool {
		r.handledRevisions.Delete(key)
		return true
	})
}

func (r *Reconciler) WaitUntil(targets ...ReconcilerOp) error {
	if r == nil {
		return fmt.Errorf("t is nil")
	}

	r.initTaskChannel()

	select {
	case <-r.reconcilerChannel.stop:
		// okay, the task channel is stopped so we can continue
	default:
		return fmt.Errorf("simultaneous WaitUntils not supported!")
	}

	r.initTaskChannelStartMessaging()
	defer close(r.reconcilerChannel.stop)

	op := ReconcilerOp(r.reconcilerChannel.lastOp.Load())
	for _, tar := range targets {
		if op == tar {
			return nil
		}
	}

	for op := range r.reconcilerChannel.taskChan {
		if len(targets) == 0 {
			return nil
		}

		for _, tar := range targets {
			if op == tar {
				return nil
			}
		}
	}

	return fmt.Errorf("t.TaskChannel.TaskChan closed")
}

func (r *Reconciler) registerTaskKey(k string, deleted bool, status *TaskStatus, value *storage.GenericEtcdKey) {
	r.ensuredTaskStatuses.Store(k, status)
	r.ensuredTaskValues.Store(k, value)
	r.handledRevisions.Store(k, revisionHistory{value.ModRevision, deleted})
}

// this also stores in the deletedHistory its revision
func (r *Reconciler) unregisterTaskKey(k string, rev int64) {
	r.ensuredTaskStatuses.Delete(k)
	r.ensuredTaskValues.Delete(k)
	r.handledRevisions.Store(k, revisionHistory{rev, true})
}

func (r *Reconciler) getEnsuredTasksKeys() []string {

	var keys []string

	r.ensuredTaskStatuses.Range(func(key string, _ *TaskStatus) bool {
		keys = append(keys, key)
		return true
	})
	radix.Sort(keys)

	return keys
}

func (r *Reconciler) incrementRunningTasks() {
	r.poolWg.Add(1)
	r.runningTasks.Add(1)
}

func (r *Reconciler) decrementRunningTasks() {
	r.poolWg.Done()
	x := r.runningTasks.Add(-1)

	if x == 0 {
		r.updateTaskChannel(ReconcilerOp_Idle)
	}
}

func (r *Reconciler) initTaskChannel() {

	r.reconcilerChannelInit.Do(func() {
		r.reconcilerChannel = &reconcilerChannel{
			lastOp:   new(atomic.Int32),
			taskChan: make(chan ReconcilerOp, 1),
			stop:     make(chan struct{}),
		}

		// close it immediately to show that it starts in the stopped state
		// the usage of this similar to context.Cancel()
		close(r.reconcilerChannel.stop)
	})

}

func (r *Reconciler) initTaskChannelStartMessaging() bool {

	r.reconcilerChannel.lock.Lock()
	defer r.reconcilerChannel.lock.Unlock()

	select {
	case <-r.reconcilerChannel.stop:
		r.reconcilerChannel.stop = make(chan struct{})
		return true
	default:
		return false
	}

}

func (r *Reconciler) updateTaskChannel(op ReconcilerOp) {
	if r == nil {
		return
	}

	r.initTaskChannel()
	r.reconcilerChannel.lastOp.Store(int32(op))

	r.reconcilerChannel.lock.RLock()
	defer r.reconcilerChannel.lock.RUnlock()

	select {
	case r.reconcilerChannel.taskChan <- op:
		// we sent it, so nothing to do
	case <-r.reconcilerChannel.stop:
		// drain channel because we are not sending anymore

		cont := r.reconcilerChannel.taskChan != nil
		for cont {
			select {
			case <-r.reconcilerChannel.taskChan:
			default:
				cont = false
			}
		}
	}
	return

}

func (r *Reconciler) bumpKeysHandled(k string) {
	r.keysHandled.Add(1)
	r.lastKeyHandled.Store(k)
	r.whenLastKey.Store(timestamppb.Now())
}

func (r *Reconciler) cloneActions() (Actions, error) {
	actions, ok := (reflect.New(reflect.ValueOf(r.Actions).Elem().Type()).Interface()).(Actions)
	if !ok {
		return nil, fmt.Errorf("actions struct in task was not able to be cloned")
	}

	copier.Copy(actions, r.Actions)
	return actions, nil
}
